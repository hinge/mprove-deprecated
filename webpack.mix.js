let mix = require('laravel-mix');
var path = require('path');
var LiveReloadPlugin = require('webpack-livereload-plugin');
const tailwindcss = require('tailwindcss');
require('laravel-mix-alias');

// require('laravel-elixir-spritesmith');

mix.alias({
    '@': '/resources/assets/js'
})

mix.sass('resources/assets/sass/styles.scss', 'public/css/styles.css')
  .copy('resources/assets/img', 'public/img')
    .copy('node_modules/sweetalert2/dist/sweetalert2.min.js', 'public/js/sweetalert.min.js')
  // .copy('resources/assets/img/**/*', 'public/images')
  // .copy('resources/assets/fonts/**/*', 'public/fonts')

  .js([
    'resources/assets/js/app.js',
    ], 'public/js/app.js')
  // .version()
  .webpackConfig({
       resolve: {
           modules: [
               path.resolve(__dirname, 'vendor/laravel/spark-aurelius/resources/assets/js'),
               'node_modules'
           ],
           alias: {
               'vue$': 'vue/dist/vue.js',
               'moment$': 'moment/moment.js'
           }
       },
       plugins: [
           new LiveReloadPlugin()
       ]
  })

    .options({
        processCssUrls: false,
        postCss: [ tailwindcss('tailwind.config.js') ],
    });
  // .spritesmith('resources/assets/img/sprite/**/*','public/img');


