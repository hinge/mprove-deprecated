@extends("master")



@section('content')
	<div class="myAccount">
		<div class="constrain">

			<h1 class="myAccount__title">Account Settings</h1>

			<spark-settings :user="user" :teams="teams" inline-template>
				<div>

					<div class="myAccount__sidebar spark-settings-tabs">
						<ul class="myAccount__sidebar-list" role="tablist">
							<li class="myAccount__sidebar-list-item">
								<a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Account Details</a>
							</li>
							<li class="myAccount__sidebar-list-item">
								<a href="#security" aria-controls="security" role="tab" data-toggle="tab">Change Password</a>
							</li>
						</ul>
					</div>

					<div class="myAccount__content tab-content">

						<div id="profile" role="tabpanel" class="myAccount__content-section tab-pane">
							<spark-update-contact-information :user="user" inline-template>
								<div>
									<h3 class="myAccount__content-title">Account details</h3>

						            <form role="form" class="myAccount__form">

										<!-- Success Message -->
							            <div class="alert alert-success" v-if="form.successful">
							                Your account details have been updated!
							            </div>

										<div class="myAccount__form-row">
											<!-- First Name -->
							                <div class="myAccount__form-row-column  myAccount__form-row-column--name">
												<label for="first_name" class="myAccount__form-label">Your first name</label>
												<div class="myAccount__form-input-wrapper">
								                    <input type="text" class="myAccount__form-input  myAccount__form-input--name" name="first_name" v-model="form.first_name">
								                    <span class="registerModal__inputText-icon registerModal__inputText-icon--person"></span>
								                </div>
						                        <span class="myAccount__form-errorMessage" v-show="form.errors.has('first_name')">
						                            @{{ form.errors.get('first_name') }}
						                        </span>
							                </div>

											<!-- Last Name -->
							                <div class="myAccount__form-row-column  myAccount__form-row-column--name">
												<label for="last_name" class="myAccount__form-label">Surname</label>
												<div class="myAccount__form-input-wrapper">
								                    <input type="text" class="myAccount__form-input  myAccount__form-input--name" name="last_name" v-model="form.last_name">
								                    <span class="registerModal__inputText-icon registerModal__inputText-icon--person"></span>
								                </div>
						                        <span class="myAccount__form-errorMessage" v-show="form.errors.has('last_name')">
						                            @{{ form.errors.get('last_name') }}
						                        </span>
							                </div>
										</div>

										<div class="myAccount__form-row">
							                <!-- E-Mail Address -->
							                <div class="myAccount__form-row-column  myAccount__form-row-column--email">
												<label for="last_name" class="myAccount__form-label">Email address</label>
												<div class="myAccount__form-input-wrapper">
							                        <input type="email" class="myAccount__form-input  myAccount__form-input--email" name="email" v-model="form.email">
							                        <span class="registerModal__inputText-icon registerModal__inputText-icon--email"></span>
							                    </div>
						                        <span class="myAccount__form-errorMessage" v-show="form.errors.has('email')">
						                            @{{ form.errors.get('email') }}
						                        </span>
							                </div>
										</div>
						            </form>

									<div class="myAccount__content-section-footer">
										<button type="submit" class="myAccount__form-submitButton"
												@click.prevent="update"
												:disabled="form.busy">
											Save profile details
										</button>
									</div>
								</div>
							</spark-update-contact-information>
						</div>

						<div id="security" role="tabpanel" class="myAccount__content-section tab-pane">
							<spark-update-password inline-template>
								<div>
									<h3 class="myAccount__content-title">Change your Password</h3>

						            <form role="form" class="myAccount__form">
										<!-- Success Message -->
							            <div class="alert alert-success" v-if="form.successful">
							                Your password has been updated!
							            </div>

										<div class="myAccount__form-row">
											<!-- Current Password -->
											<div class="myAccount__form-row-column  myAccount__form-row-column--currentPassword">
												<label for="current_password" class="myAccount__form-label">Current password</label>
												<div class="myAccount__form-input-wrapper">
													<input type="password" class="myAccount__form-input  myAccount__form-input--password" name="current_password" v-model="form.current_password" placeholder="Just to confirm it's you" />
													<span class="registerModal__inputText-icon registerModal__inputText-icon--password"></span>
												</div>
												<span class="myAccount__form-errorMessage" v-show="form.errors.has('current_password')">
													@{{ form.errors.get('current_password') }}
												</span>
											</div>
										</div>

										<div class="myAccount__form-row">
											<!-- New Password -->
											<div class="myAccount__form-row-column  myAccount__form-row-column--password">
												<label for="password" class="myAccount__form-label">New password</label>
												<div class="myAccount__form-input-wrapper">
													<input type="password" class="myAccount__form-input  myAccount__form-input--password" name="password" v-model="form.password" />
												<span class="registerModal__inputText-icon registerModal__inputText-icon--password"></span>
												</div>
												<span class="myAccount__form-errorMessage" v-show="form.errors.has('password')">
													@{{ form.errors.get('password') }}
												</span>
											</div>

											<!-- Confirm New Password -->
											<div class="myAccount__form-row-column  myAccount__form-row-column--password">
												<label for="password_confirmation" class="myAccount__form-label">Confirm new password</label>
												<div class="myAccount__form-input-wrapper">
													<input type="password" class="myAccount__form-input  myAccount__form-input--password" name="password_confirmation" v-model="form.password_confirmation" />
													<span class="registerModal__inputText-icon registerModal__inputText-icon--password"></span>
												</div>
												<span class="myAccount__form-errorMessage" v-show="form.errors.has('password_confirmation')">
													@{{ form.errors.get('password_confirmation') }}
												</span>
											</div>
										</div>

						            </form>

									<div class="myAccount__content-section-footer">
										<button type="submit" class="myAccount__form-submitButton"
				                                @click.prevent="update"
				                                :disabled="form.busy">
											Save new password
				                        </button>
									</div>
								</div>
							</spark-update-password>
						</div>

					</div>

				</div>
			</spark-settings>

		</div>
	</div>
@endsection
