@extends("master")

@section("title")
    Choose your new MPROvE password
@endsection

@section("stylesheets")

@endsection

@section("content")

    <div class="authentication">
        <div class="authentication__inner constrain">
            <div class="registerModal__column-signIn">
                @if (session('status'))
                    <div class="alert alert-success">
                    {{ session('status') }}
                    </div>
                @endif
                <h2 class="registerModal__column-signIn-heading registerModal__column-signIn-heading--leftAlign">Enter your email address<br />and choose your new password</h2>
                <form class="form-horizontal" role="form" method="POST" action="{{ url('/password/reset') }}">
                    {!! csrf_field() !!}
                    <input type="hidden" name="token" value="{{ $token }}">
                    <div class="registerModal__column-signIn-form-block">
                        <input
                            type="email" 
                            name="email" 
                            placeholder="Your email address"
                            autocomplete="off"
                            class="registerModal__inputText registerModal__inputText--emailLogin resetInputStyles {{ $errors->has('email') ? ' registerModal__inputText--error' : '' }}"
                            value="{{ old('email') }}"
                            autofocus
                        >
                        <span class="registerModal__inputText-icon registerModal__inputText-icon--email"></span>
                        <span class="registerModal__fieldErrorMessage">
                            @if ($errors->has('email'))
                                {{ $errors->first('email') }}
                            @endif
                        </span>
                    </div>
                    <div class="registerModal__column-signIn-form-block">
                        <input
                            type="password" 
                            name="password" 
                            placeholder="New Password"
                            autocomplete="off"
                            class="registerModal__inputText registerModal__inputText--passwordLogin resetInputStyles {{ $errors->has('password') ? ' registerModal__inputText--error' : '' }}"
                            value="{{ old('password') }}"
                        >
                        <span class="registerModal__inputText-icon registerModal__inputText-icon--password"></span>
                        <span class="registerModal__fieldErrorMessage">
                            @if ($errors->has('password'))
                                {{ $errors->first('password') }}
                            @endif
                        </span>
                    </div>
                    <div class="registerModal__column-signIn-form-block">
                        <input
                            type="password" 
                            name="password_confirmation" 
                            placeholder="Confirm New Password"
                            autocomplete="off"
                            class="registerModal__inputText registerModal__inputText--passwordLogin resetInputStyles {{ $errors->has('password_confirmation') ? ' registerModal__inputText--error' : '' }}"
                            value="{{ old('password_confirmation') }}"
                        >
                        <span class="registerModal__inputText-icon registerModal__inputText-icon--password"></span>
                        <span class="registerModal__fieldErrorMessage">
                            @if ($errors->has('password_confirmation'))
                                {{ $errors->first('password_confirmation') }}
                            @endif
                        </span>
                    </div>
                    <button type="submit" class="button authentication__submit">
                        <i class="fa fa-btn fa-refresh"></i> Reset Password
                    </button>
                </form>
            </div>
        </div>
    </div>

@endsection

@section("scripts")

@endsection

<?php /*
@extends('spark::layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Reset Password</div>

                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/password/reset') }}">
                        {!! csrf_field() !!}

                        <input type="hidden" name="token" value="{{ $token }}">

                        <!-- E-Mail Address -->
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input type="email" class="form-control" name="email" value="{{ $email or old('email') }}" autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        {{ $errors->first('email') }}
                                    </span>
                                @endif
                            </div>
                        </div>

                        <!-- Password -->
                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input type="password" class="form-control" name="password">

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        {{ $errors->first('password') }}
                                    </span>
                                @endif
                            </div>
                        </div>

                        <!-- Password Confirmation -->
                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">Confirm Password</label>
                            <div class="col-md-6">
                                <input type="password" class="form-control" name="password_confirmation">

                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        {{ $errors->first('password_confirmation') }}
                                    </span>
                                @endif
                            </div>
                        </div>

                        <!-- Reset Button -->
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-refresh"></i>Reset Password
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
*/ ?>