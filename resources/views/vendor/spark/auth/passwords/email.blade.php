@extends("master")

@section("title")
    Reset your MPROvE password
@endsection

@section("stylesheets")

@endsection

@section("content")

    <div class="authentication">
        <div class="authentication__innter constrain">
            <div class="registerModal__column-signIn">
                @if (session('status'))
                    <div class="alert alert-success">
                    {{ session('status') }}
                    </div>
                @endif
                <h2 class="registerModal__column-signIn-heading registerModal__column-signIn-heading--leftAlign">Enter your email to reset<br />your password</h2>
                <form class="form-horizontal" role="form" method="POST" action="{{ url('/password/email') }}">
                    {!! csrf_field() !!}
                    <div class="registerModal__column-signIn-form-block">
                        <input
                            type="email" 
                            name="email" 
                            placeholder="Your email address"
                            autocomplete="off"
                            class="registerModal__inputText registerModal__inputText--emailLogin resetInputStyles {{ $errors->has('email') ? ' registerModal__inputText--error' : '' }}"
                            value="{{ old('email') }}"
                            autofocus
                        >
                        <span class="registerModal__inputText-icon registerModal__inputText-icon--email"></span>
                        <span class="registerModal__fieldErrorMessage">
                            @if ($errors->has('email'))
                                {{ $errors->first('email') }}
                            @endif
                        </span>
                    </div>
                    <button type="submit" class="button authentication__submit">
                        <i class="fa fa-btn fa-paper-plane"></i> Send Password Reset Link
                    </button>
                </form>
            </div>
        </div>
    </div>

@endsection

@section("scripts")

@endsection