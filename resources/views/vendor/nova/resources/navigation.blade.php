@if (count(\Laravel\Nova\Nova::resourcesForNavigation(request())))

    @php $other = $navigation->get('Other') @endphp

    @if($navigation->get('Other')->contains('App\Nova\User'))
        @php
            $other->forget($other->search('App\Nova\User'));
        @endphp

        <div class="nova-users">
            <router-link :to="{
                name: 'index',
                params: {
                resourceName: '{{ App\Nova\User::uriKey() }}'
                }
                }" class="text-white text-justify no-underline dim block">

                <h3 class="flex items-center font-normal text-white mb-6 text-base no-underline">
                    <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="users" class="svg-inline--fa fa-users fa-w-20 sidebar-icon" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 512"><path fill="currentColor" d="M96 224c35.3 0 64-28.7 64-64s-28.7-64-64-64-64 28.7-64 64 28.7 64 64 64zm448 0c35.3 0 64-28.7 64-64s-28.7-64-64-64-64 28.7-64 64 28.7 64 64 64zm32 32h-64c-17.6 0-33.5 7.1-45.1 18.6 40.3 22.1 68.9 62 75.1 109.4h66c17.7 0 32-14.3 32-32v-32c0-35.3-28.7-64-64-64zm-256 0c61.9 0 112-50.1 112-112S381.9 32 320 32 208 82.1 208 144s50.1 112 112 112zm76.8 32h-8.3c-20.8 10-43.9 16-68.5 16s-47.6-6-68.5-16h-8.3C179.6 288 128 339.6 128 403.2V432c0 26.5 21.5 48 48 48h288c26.5 0 48-21.5 48-48v-28.8c0-63.6-51.6-115.2-115.2-115.2zm-223.7-13.4C161.5 263.1 145.6 256 128 256H64c-35.3 0-64 28.7-64 64v32c0 17.7 14.3 32 32 32h65.9c6.3-47.4 34.9-87.3 75.2-109.4z"></path></svg>
                    <span class="sidebar-label">
                            {{ App\Nova\User::label() }}
                    </span>
                </h3>
            </router-link>
        </div>
    @endif

    @if($navigation->get('Other')->contains('App\Nova\Venue'))

        @php
            $other->forget($other->search('App\Nova\Venue'));
        @endphp

        <div class="nova-venues">

            <router-link :to="{
                    name: 'index',
                    params: {
                    resourceName: '{{ App\Nova\Venue::uriKey() }}'
                    }
                    }" class="text-white text-justify no-underline dim block">

                <h3 class="flex items-center font-normal text-white mb-6 text-base no-underline">
                    <svg aria-hidden="true" focusable="false" data-prefix="far" data-icon="building" class="svg-inline--fa fa-building fa-w-14 sidebar-icon" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path fill="currentColor" d="M128 148v-40c0-6.6 5.4-12 12-12h40c6.6 0 12 5.4 12 12v40c0 6.6-5.4 12-12 12h-40c-6.6 0-12-5.4-12-12zm140 12h40c6.6 0 12-5.4 12-12v-40c0-6.6-5.4-12-12-12h-40c-6.6 0-12 5.4-12 12v40c0 6.6 5.4 12 12 12zm-128 96h40c6.6 0 12-5.4 12-12v-40c0-6.6-5.4-12-12-12h-40c-6.6 0-12 5.4-12 12v40c0 6.6 5.4 12 12 12zm128 0h40c6.6 0 12-5.4 12-12v-40c0-6.6-5.4-12-12-12h-40c-6.6 0-12 5.4-12 12v40c0 6.6 5.4 12 12 12zm-76 84v-40c0-6.6-5.4-12-12-12h-40c-6.6 0-12 5.4-12 12v40c0 6.6 5.4 12 12 12h40c6.6 0 12-5.4 12-12zm76 12h40c6.6 0 12-5.4 12-12v-40c0-6.6-5.4-12-12-12h-40c-6.6 0-12 5.4-12 12v40c0 6.6 5.4 12 12 12zm180 124v36H0v-36c0-6.6 5.4-12 12-12h19.5V24c0-13.3 10.7-24 24-24h337c13.3 0 24 10.7 24 24v440H436c6.6 0 12 5.4 12 12zM79.5 463H192v-67c0-6.6 5.4-12 12-12h40c6.6 0 12 5.4 12 12v67h112.5V49L80 48l-.5 415z"></path></svg>
                    <span class="sidebar-label">
                                        {{ App\Nova\Venue::label() }}
                                    </span>
                </h3>
            </router-link>

        </div>

    @endif

    @if($navigation->get('Other')->contains('App\Nova\Course'))

        @php
            $other->forget($other->search('App\Nova\Course'));
        @endphp

        <div class="nova-courses">
            <router-link :to="{
                name: 'index',
                params: {
                resourceName: '{{ App\Nova\Course::uriKey() }}'
                }
                }" class="text-white text-justify no-underline dim block">

                <h3 class="flex items-center font-normal text-white mb-6 text-base no-underline">
                    <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="calendar-day" class="svg-inline--fa fa-calendar-day fa-w-14 sidebar-icon" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path fill="currentColor" d="M0 464c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48V192H0v272zm64-192c0-8.8 7.2-16 16-16h96c8.8 0 16 7.2 16 16v96c0 8.8-7.2 16-16 16H80c-8.8 0-16-7.2-16-16v-96zM400 64h-48V16c0-8.8-7.2-16-16-16h-32c-8.8 0-16 7.2-16 16v48H160V16c0-8.8-7.2-16-16-16h-32c-8.8 0-16 7.2-16 16v48H48C21.5 64 0 85.5 0 112v48h448v-48c0-26.5-21.5-48-48-48z"></path></svg>
                    <span class="sidebar-label">
                        {{ App\Nova\Course::label() }}
                    </span>
                </h3>
            </router-link>
        </div>
    @endif

    @if($navigation->get('Other')->contains('App\Nova\MproveResource'))

        @php
            $other->forget($other->search('App\Nova\MproveResource'));
        @endphp

        <div class="nova-users">
            <router-link :to="{
                name: 'index',
                params: {
                resourceName: '{{ App\Nova\MproveResource::uriKey() }}'
                }
                }" class="text-white text-justify no-underline dim block">

                <h3 class="flex items-center font-normal text-white mb-6 text-base no-underline">
                    <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="box-open" class="svg-inline--fa fa-box-open fa-w-20 sidebar-icon" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 512"><path fill="currentColor" d="M425.7 256c-16.9 0-32.8-9-41.4-23.4L320 126l-64.2 106.6c-8.7 14.5-24.6 23.5-41.5 23.5-4.5 0-9-.6-13.3-1.9L64 215v178c0 14.7 10 27.5 24.2 31l216.2 54.1c10.2 2.5 20.9 2.5 31 0L551.8 424c14.2-3.6 24.2-16.4 24.2-31V215l-137 39.1c-4.3 1.3-8.8 1.9-13.3 1.9zm212.6-112.2L586.8 41c-3.1-6.2-9.8-9.8-16.7-8.9L320 64l91.7 152.1c3.8 6.3 11.4 9.3 18.5 7.3l197.9-56.5c9.9-2.9 14.7-13.9 10.2-23.1zM53.2 41L1.7 143.8c-4.6 9.2.3 20.2 10.1 23l197.9 56.5c7.1 2 14.7-1 18.5-7.3L320 64 69.8 32.1c-6.9-.8-13.5 2.7-16.6 8.9z"></path></svg>
                    <span class="sidebar-label">
                            {{ App\Nova\MproveResource::label() }}
                    </span>
                </h3>
            </router-link>
        </div>
    @endif

    @if($navigation->get('Other')->contains('App\Nova\TrainingCourse'))

        @php
            $other->forget($other->search('App\Nova\TrainingCourse'));
        @endphp

        <div class="nova-users">
            <router-link :to="{
                name: 'index',
                params: {
                resourceName: '{{ App\Nova\TrainingCourse::uriKey() }}'
                }
                }" class="text-white text-justify no-underline dim block">

                <h3 class="flex items-center font-normal text-white mb-6 text-base no-underline">
                    <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="book" class="svg-inline--fa fa-book fa-w-14 sidebar-icon" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path fill="currentColor" d="M448 360V24c0-13.3-10.7-24-24-24H96C43 0 0 43 0 96v320c0 53 43 96 96 96h328c13.3 0 24-10.7 24-24v-16c0-7.5-3.5-14.3-8.9-18.7-4.2-15.4-4.2-59.3 0-74.7 5.4-4.3 8.9-11.1 8.9-18.6zM128 134c0-3.3 2.7-6 6-6h212c3.3 0 6 2.7 6 6v20c0 3.3-2.7 6-6 6H134c-3.3 0-6-2.7-6-6v-20zm0 64c0-3.3 2.7-6 6-6h212c3.3 0 6 2.7 6 6v20c0 3.3-2.7 6-6 6H134c-3.3 0-6-2.7-6-6v-20zm253.4 250H96c-17.7 0-32-14.3-32-32 0-17.6 14.4-32 32-32h285.4c-1.9 17.1-1.9 46.9 0 64z"></path></svg>
                    <span class="sidebar-label">
                            {{ App\Nova\TrainingCourse::label() }}
                    </span>
                </h3>
            </router-link>

                <ul class="nova-venues clean-list mb-8 list-reset pl-6" >

                    @if($navigation->get('Other')->contains('App\Nova\Question'))

                        @php
                            $other->forget($other->search('App\Nova\Question'));
                        @endphp

                        <li>
                            <router-link :to="{
                                    name: 'index',
                                    params: {
                                    resourceName: '{{ App\Nova\Question::uriKey() }}'
                                    }
                                    }" class="text-white text-justify no-underline dim block">

                                <h3 class="flex items-center font-normal text-white mb-6 text-base no-underline">
                                    <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="question" class="svg-inline--fa fa-question fa-w-12 sidebar-icon" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512"><path fill="currentColor" d="M202.021 0C122.202 0 70.503 32.703 29.914 91.026c-7.363 10.58-5.093 25.086 5.178 32.874l43.138 32.709c10.373 7.865 25.132 6.026 33.253-4.148 25.049-31.381 43.63-49.449 82.757-49.449 30.764 0 68.816 19.799 68.816 49.631 0 22.552-18.617 34.134-48.993 51.164-35.423 19.86-82.299 44.576-82.299 106.405V320c0 13.255 10.745 24 24 24h72.471c13.255 0 24-10.745 24-24v-5.773c0-42.86 125.268-44.645 125.268-160.627C377.504 66.256 286.902 0 202.021 0zM192 373.459c-38.196 0-69.271 31.075-69.271 69.271 0 38.195 31.075 69.27 69.271 69.27s69.271-31.075 69.271-69.271-31.075-69.27-69.271-69.27z"></path></svg>
                                    <span class="sidebar-label">
                                    {{ App\Nova\Question::label() }}
                                </span>
                                </h3>
                            </router-link>
                        </li>
                    @endif

                    @if($navigation->get('Other')->contains('App\Nova\TrainingCourseReference'))

                        @php
                            $other->forget($other->search('App\Nova\TrainingCourseReference'));
                        @endphp

                        <li>
                            <router-link :to="{
                                    name: 'index',
                                    params: {
                                    resourceName: '{{ App\Nova\TrainingCourseReference::uriKey() }}'
                                    }
                                    }" class="text-white text-justify no-underline dim block">

                                <h3 class="flex items-center font-normal text-white mb-6 text-base no-underline">
                                    <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="link" class="svg-inline--fa fa-link fa-w-16 sidebar-icon" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path fill="currentColor" d="M326.612 185.391c59.747 59.809 58.927 155.698.36 214.59-.11.12-.24.25-.36.37l-67.2 67.2c-59.27 59.27-155.699 59.262-214.96 0-59.27-59.26-59.27-155.7 0-214.96l37.106-37.106c9.84-9.84 26.786-3.3 27.294 10.606.648 17.722 3.826 35.527 9.69 52.721 1.986 5.822.567 12.262-3.783 16.612l-13.087 13.087c-28.026 28.026-28.905 73.66-1.155 101.96 28.024 28.579 74.086 28.749 102.325.51l67.2-67.19c28.191-28.191 28.073-73.757 0-101.83-3.701-3.694-7.429-6.564-10.341-8.569a16.037 16.037 0 0 1-6.947-12.606c-.396-10.567 3.348-21.456 11.698-29.806l21.054-21.055c5.521-5.521 14.182-6.199 20.584-1.731a152.482 152.482 0 0 1 20.522 17.197zM467.547 44.449c-59.261-59.262-155.69-59.27-214.96 0l-67.2 67.2c-.12.12-.25.25-.36.37-58.566 58.892-59.387 154.781.36 214.59a152.454 152.454 0 0 0 20.521 17.196c6.402 4.468 15.064 3.789 20.584-1.731l21.054-21.055c8.35-8.35 12.094-19.239 11.698-29.806a16.037 16.037 0 0 0-6.947-12.606c-2.912-2.005-6.64-4.875-10.341-8.569-28.073-28.073-28.191-73.639 0-101.83l67.2-67.19c28.239-28.239 74.3-28.069 102.325.51 27.75 28.3 26.872 73.934-1.155 101.96l-13.087 13.087c-4.35 4.35-5.769 10.79-3.783 16.612 5.864 17.194 9.042 34.999 9.69 52.721.509 13.906 17.454 20.446 27.294 10.606l37.106-37.106c59.271-59.259 59.271-155.699.001-214.959z"></path></svg>
                                    <span class="sidebar-label">
                                        {{ App\Nova\TrainingCourseReference::label() }}
                                    </span>
                                </h3>
                            </router-link>
                        </li>
                    @endif

                </ul>
        </div>
    @endif

    @if($navigation->get('Other')->contains('App\Nova\Asset'))
        @php
            $other->forget($other->search('App\Nova\Asset'));
        @endphp

        <div class="nova-assets">
            <router-link :to="{
                    name: 'index',
                    params: {
                    resourceName: '{{ App\Nova\Asset::uriKey() }}'
                    }
                    }" class="text-white text-justify no-underline dim block">

                <h3 class="flex items-center font-normal text-white mb-6 text-base no-underline">
                    <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="box-open" class="svg-inline--fa fa-box-open fa-w-20 sidebar-icon" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 512"><path fill="currentColor" d="M425.7 256c-16.9 0-32.8-9-41.4-23.4L320 126l-64.2 106.6c-8.7 14.5-24.6 23.5-41.5 23.5-4.5 0-9-.6-13.3-1.9L64 215v178c0 14.7 10 27.5 24.2 31l216.2 54.1c10.2 2.5 20.9 2.5 31 0L551.8 424c14.2-3.6 24.2-16.4 24.2-31V215l-137 39.1c-4.3 1.3-8.8 1.9-13.3 1.9zm212.6-112.2L586.8 41c-3.1-6.2-9.8-9.8-16.7-8.9L320 64l91.7 152.1c3.8 6.3 11.4 9.3 18.5 7.3l197.9-56.5c9.9-2.9 14.7-13.9 10.2-23.1zM53.2 41L1.7 143.8c-4.6 9.2.3 20.2 10.1 23l197.9 56.5c7.1 2 14.7-1 18.5-7.3L320 64 69.8 32.1c-6.9-.8-13.5 2.7-16.6 8.9z"></path></svg>
                    <span class="sidebar-label">
                            {{ App\Nova\Asset::label() }}
                    </span>
                </h3>
            </router-link>
        </div>
    @endif

    @php
        $navigation->Other = $other;
    @endphp


{{--    @if($navigation->Other->count())--}}
{{--        <div class="nova-resources">--}}
{{--            <h3 class="flex items-center font-normal text-white mb-6 text-base no-underline">--}}
{{--                <svg class="sidebar-icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">--}}
{{--                    <path fill="var(--sidebar-icon)" d="M3 1h4c1.1045695 0 2 .8954305 2 2v4c0 1.1045695-.8954305 2-2 2H3c-1.1045695 0-2-.8954305-2-2V3c0-1.1045695.8954305-2 2-2zm0 2v4h4V3H3zm10-2h4c1.1045695 0 2 .8954305 2 2v4c0 1.1045695-.8954305 2-2 2h-4c-1.1045695 0-2-.8954305-2-2V3c0-1.1045695.8954305-2 2-2zm0 2v4h4V3h-4zM3 11h4c1.1045695 0 2 .8954305 2 2v4c0 1.1045695-.8954305 2-2 2H3c-1.1045695 0-2-.8954305-2-2v-4c0-1.1045695.8954305-2 2-2zm0 2v4h4v-4H3zm10-2h4c1.1045695 0 2 .8954305 2 2v4c0 1.1045695-.8954305 2-2 2h-4c-1.1045695 0-2-.8954305-2-2v-4c0-1.1045695.8954305-2 2-2zm0 2v4h4v-4h-4z"--}}
{{--                    />--}}
{{--                </svg>--}}
{{--                <span class="sidebar-label">{{ __('Resources') }}</span>--}}
{{--            </h3>--}}

{{--            @foreach($navigation as $group => $resources)--}}
{{--                @if (count($groups) > 1)--}}
{{--                    <h4 class="ml-8 mb-4 text-xs text-white-50% uppercase tracking-wide">{{ $group }}</h4>--}}
{{--                @endif--}}

{{--                <ul class="list-reset mb-8">--}}
{{--                    @foreach($resources as $resource)--}}
{{--                        <li class="leading-tight mb-4 ml-8 text-sm">--}}
{{--                            <router-link :to="{--}}
{{--                                    name: 'index',--}}
{{--                                    params: {--}}
{{--                                    resourceName: '{{ $resource::uriKey() }}'--}}
{{--                                    }--}}
{{--                                    }" class="text-white text-justify no-underline dim block">--}}
{{--                                {{ $resource::label() }}--}}
{{--                            </router-link>--}}
{{--                        </li>--}}
{{--                    @endforeach--}}
{{--                </ul>--}}
{{--            @endforeach--}}
{{--        </div>--}}
{{--    @endif--}}
@endif
