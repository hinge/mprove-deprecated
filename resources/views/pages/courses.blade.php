@extends("master")

@section("title")
	{{ $meta_title }}
@endsection

@section("content")
	<div class="content-wrapper">
		@include("blocks.pageHero", ['heroWide' => true])
		<courses></courses>
	</div>
@endsection