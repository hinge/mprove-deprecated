@extends("master")

@section("title")
	{{ $meta_title }}
@endsection

@section("content")
	<div class="content-wrapper">
		<course-details course_id="{{ $course_id }}" course_date="{{ $course_date }}" venue_slug="{{ $venue_slug }}"></course-details>
	</div>
@endsection