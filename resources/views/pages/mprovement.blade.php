@extends("master")

@section("title")
	{{ $meta_title }}
@endsection

@section("stylesheets")

@endsection

@section("content")
	<div class="content-wrapper">

		@include("blocks.pageHero", ['heroWide' => false])

		<div class="twoColumnGrid pull-page-hero-top">
			<div class="twoColumnGrid__inner constrain">
				<ul class="twoColumnGrid__items">
					<li class="twoColumnGrid__items-item">
						<div class="infoCard">
							<div class="infoCard__inner">
								<h2 class="infoCard__heading">Toolkits</h2>
								<p class="infoCard__text">The MPROvE team has developed toolkits, care bundles, and training packages in many areas involving the delivery of high quality neonatal care.</p>
								<a class="infoCard__button button" href="/quality-mprovement/toolkits">See the toolkits</a>
							</div>
						</div>
					</li><li class="twoColumnGrid__items-item">
						<div class="infoCard">
							<div class="infoCard__inner">
								<h2 class="infoCard__heading">Community Forum</h2>
								<p class="infoCard__text">The MPROvE community allows individuals to interact with the MPROvE team to discuss topics related to neonatology, share ideas and resources with each other.</p>
								<a class="infoCard__button button" href="/forums">Visit the forum</a>
							</div>
						</div>
					</li>
				</ul>
			</div>
		</div>

	</div>
@endsection

@section("scripts")

@endsection