@extends("master")

@section("title")
	{{ env('APP_NAME') }}
@endsection

@section("stylesheets")

@endsection

@section("content")

	<div class="content-wrapper">

		<div class="homeHero block-isWhite">


			<div class="homeHero__inner constrain">

				<div class="homeHero__textBlock">

					<h1 class="homeHero__textBlock-title">
						<span>MPROvE</span> MultiPROfessional neonatal Education
					</h1>
					<p class="homeHero__textBlock-text">Teams that train together improve Neonatal Care together.</p>
					<a href="/mprove-academy" class="homeHero__textBlock-button"><span>See upcoming courses</span><!----><i></i></a>

				</div><!-- END TEXT BLOCK
				--><div class="homeHero__videoBlock">

					<div class="homeHero__videoBlock-wrapper">
						<img class="sizing-image" src="/img/home-hero-image.jpg" alt="">
						<div class="homeHero__videoBlock-wrapper-coverImage" style="background-image: url('/img/home-hero-image.jpg')"></div>
						{{-- <img src="/img/play-svg.svg" alt="Play Button" class="homeHero__videoBlock-playButton"> --}}
					</div>

				</div>

			</div>

			<img src="/img/curve-svg.svg" alt="" class="homeAfterHeroBlock__curve-image">

		</div>
		<div class="clearfix"></div>


		<div class="fourColumnIconPlusTextGrid block-isWhite">


			<div class="fourColumnIconPlusTextGrid__inner constrain">

				<h2 class="fourColumnIconPlusTextGrid__heading">
					Learn with MPROvE. <span>Your way.</span>
				</h2>

				<ul class="fourColumnIconPlusTextGrid__cards">

					<li class="fourColumnIconPlusTextGrid__cards-card">

						<div class="fourColumnIconPlusTextGrid__cards-card-imageWrapper">
							<img src="/img/hat-svg.svg" alt="Mprove Academy Icon" class="fourColumnIconPlusTextGrid__cards-card-image">
						</div>
						<div class="fourColumnIconPlusTextGrid__cards-card-textWrapper">
							<h3 class="fourColumnIconPlusTextGrid__cards-card-title">MProve Academy</h3>
							<p class="fourColumnIconPlusTextGrid__cards-card-text">Theme based courses for Multidisciplinary Teams : Challenge Yourself</p>
						</div>
						<a href="/mprove-academy" class="fourColumnIconPlusTextGrid__cards-card-button"><span>Visit the academy</span><i></i></a>

					</li><!--
					--><li class="fourColumnIconPlusTextGrid__cards-card">

						<div class="fourColumnIconPlusTextGrid__cards-card-imageWrapper">
							<img src="/img/key-svg.svg" alt="Mprove Academy Icon" class="fourColumnIconPlusTextGrid__cards-card-image">
						</div>
						<div class="fourColumnIconPlusTextGrid__cards-card-textWrapper">
							<h3 class="fourColumnIconPlusTextGrid__cards-card-title">Quality MPROvEment</h3>
							<p class="fourColumnIconPlusTextGrid__cards-card-text">Toolkits & Care Bundles to Improve Neonatal Care</p>
						</div>
						<a href="/quality-mprovement/toolkits" class="fourColumnIconPlusTextGrid__cards-card-button"><span>See the toolkits</span><i></i></a>

					</li><!--
					--><li class="fourColumnIconPlusTextGrid__cards-card">

						<div class="fourColumnIconPlusTextGrid__cards-card-imageWrapper">
							<img src="/img/write-svg.svg" alt="Mprove Academy Icon" class="fourColumnIconPlusTextGrid__cards-card-image">
						</div>
						<div class="fourColumnIconPlusTextGrid__cards-card-textWrapper">
							<h3 class="fourColumnIconPlusTextGrid__cards-card-title">MPROvE Videos</h3>
							<p class="fourColumnIconPlusTextGrid__cards-card-text">Teaching neonatal skills through a visual and interactive approach</p>
						</div>
						<a href="/online-training-courses" class="fourColumnIconPlusTextGrid__cards-card-button"><span>See the courses</span><i></i></a>

					</li>
				</ul>

			</div>

		</div>

		{{-- @include("blocks.logoBanner") --}}


		<div class="testimonials">


			<div class="testimonials__inner constrain">

				<ul class="testimonials__items">

					<li class="testimonials__items-item">

						<div class="testimonials__items-item-imageWrapper">
							<img src="/img/tim-malpas.png" srcset="/img/tim-malpas@2x.png 2x," alt="" class="testimonials__items-item-image">
						</div>
						<div class="testimonials__items-item-textWrapper">
							<p class="testimonials__items-item-quote"> “Thanks so much for an inspiring course. I really enjoyed it and am looking forward to not being one of the 90% who don’t take it forward!" </p>
							<p class="testimonials__items-item-details">
								<span class="testimonials__items-item-details--name">Tim Malpas</span>
								<span class="testimonials__items-item-details--place">Consultant Paediatrician, Jersey</span>
							</p>
						</div>
					</li><!--
					--><li class="testimonials__items-item">

						<div class="testimonials__items-item-imageWrapper">
							<img src="/img/testimonial-1.jpg" srcset="/img/testimonial-1@2x.jpg 2x," alt="" class="testimonials__items-item-image">
						</div>
						<div class="testimonials__items-item-textWrapper">
							<p class="testimonials__items-item-quote"> “The course has led me to consider aspects of simulation that I didn't even realise I could. I now feel I will use simulation within my clinical practice in a far more integrated and useful way." </p>
							<p class="testimonials__items-item-details">
								<span class="testimonials__items-item-details--name">Dr Sushmitha Nataraja</span>
								<span class="testimonials__items-item-details--place">Neonatal Fellow, Southampton</span>
							</p>
						</div>

					</li><!--
					--><li class="testimonials__items-item">

						<div class="testimonials__items-item-imageWrapper">
							<img src="/img/testimonial-3.jpg" srcset="/img/testimonial-3@2x.jpg 2x," alt="" class="testimonials__items-item-image">
						</div>
						<div class="testimonials__items-item-textWrapper">
							<p class="testimonials__items-item-quote"> “Excellent course content which has enhanced my debriefing skills." </p>
							<p class="testimonials__items-item-details">
								<span class="testimonials__items-item-details--name">Dr Ashwin Saboo</span>
								<span class="testimonials__items-item-details--place">Neonatal Specialist Corniche Hospital, Abu Dhabi</span>
							</p>
						</div>

					</li>

				</ul>

			</div>

		</div>


		<div class="textPlusStackedImage">


			<div class="textPlusStackedImage__inner constrain">

				<div class="textPlusStackedImage__column">

					<h2 class="textPlusStackedImage__column-heading">Saving lives by developing skills</h2>
					<p class="textPlusStackedImage__column-text">The concept MPROvE relates to multidisciplinary education of any kind (i.e. multidisciplinary neonatal simulation, or TEL) being used to evaluate if we can improve the quality of care and or outcomes in neonates. It is covered by trademark.</p>
					<p class="textPlusStackedImage__column-text">Neonatal simualtion delivered as part of the MPROvE programme has been nominated for a BMJ award in 2014.</p>
					<a href="/about-mprove" class="textPlusStackedImage__column-link"><span>About MPROvE</span><i></i></a>

				</div><!--
				--><div class="textPlusStackedImage__column textPlusStackedImage__column--image">
					<div class="textPlusStackedImage__column-wrapper">
						<div class="textPlusStackedImage__column-imageWrapper textPlusStackedImage__column-imageWrapper--portrait">
							<img class="sizing-image" src="/img/saving-lives-1.jpg" alt="">
							<div class="textPlusStackedImage__column-imageWrapper-coverImage" style="background-image: url('/img/saving-lives-1@2x.jpg')"></div>
						</div>
						<div class="textPlusStackedImage__column-imageWrapper textPlusStackedImage__column-imageWrapper--landscape">
							<img class="sizing-image" src="/img/saving-lives-2.jpg" alt="">
							<div class="textPlusStackedImage__column-imageWrapper-coverImage" style="background-image: url('/img/saving-lives-2@2x.jpg')"></div>
						</div>
					</div>


				</div>

			</div>

		</div>

		@include("blocks.twoColumnCards")

	</div>
@endsection

@section("scripts")

@endsection
