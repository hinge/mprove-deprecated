@extends("master")

@section("title")
	{{ $meta_title }}
@endsection

@section("content")
	<div class="content-wrapper">
		<course-dates course_id="{{ $course_id }}"></course-dates>
	</div>
@endsection
