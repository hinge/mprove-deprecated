@extends("master")

@section("title")
	About MPRoVE
@endsection

@section("stylesheets")

@endsection

@section("content")
	<div class="content-wrapper">

		<section class="pageIntro">
			<div class="pageIntro__inner constrain">
				<h1 class="pageIntro__heading">MPROvE are working hard to bring first class neonatal training to those who need it</h1>
				<p class="pageIntro__subHeading">The MPROvE team strongly believe that teams that train together improve Neonatal Care together.</p>
			</div>
		</section>

		<section class="textPlusStackedImage no-padding">

			<!-- INNER START -->
			<div class="textPlusStackedImage__inner constrain">

				<div class="textPlusStackedImage__column">

					<h2 class="textPlusStackedImage__column-heading">Dr Alok Sharma</h2>
					<p class="textPlusStackedImage__column-text">Dr Alok Sharma is a Consultant Neonatologist at Princess Anne Hospital Southampton in the United Kingdom. He is lead for the Wessex-Oxford Neonatal Education Programme and has led development of the neonatal simulation programme at Princess Anne Hospital Southampton. He is one of the founding members of MPROvE and his principal areas of interest are cascading medical error and risk through in situ simulation, barriers to the uptake of simulation by multidisciplinary teams and identification of latent threat.</p>

				</div><!--
				--><div class="textPlusStackedImage__column textPlusStackedImage__column--image">
					<div class="textPlusStackedImage__column-wrapper">
						<div class="textPlusStackedImage__column-imageWrapper textPlusStackedImage__column-imageWrapper--portrait">
							<img class="sizing-image" src="/img/dr-alok-sharma.jpg" alt="">
							<div class="textPlusStackedImage__column-imageWrapper-coverImage" style="background-image: url('/img/dr-alok-sharma@2x.jpg')"></div>
						</div>
						<div class="textPlusStackedImage__column-imageWrapper textPlusStackedImage__column-imageWrapper--landscape">
							<img class="sizing-image" src="/img/profile-image-1.jpg" alt="">
							<div class="textPlusStackedImage__column-imageWrapper-coverImage" style="background-image: url('/img/profile-image-1@2x.jpg')"></div>
						</div>
					</div>


				</div>

			</div> <!-- END INNER -->

		</section>

		<section class="textPlusStackedImage textPlusStackedImage--reversed no-padding">

			<!-- INNER START -->
			<div class="textPlusStackedImage__inner constrain">

				<div class="textPlusStackedImage__column">

					<h2 class="textPlusStackedImage__column-heading">Dr Ranjit Gunda</h2>
					<p class="textPlusStackedImage__column-text">Dr Ranjit Gunda is a Consultant Neonatologist at Rainbow Children’s Hospital in India. He was lead for the Wessex-Oxford Neonatal Education Programme and faculty on the MPROvE Neonatal Instructor Programme and Simulated Neonatal airway workshop. Dr Gunda has trained in neonatal intensive care at Aberdeen, Southampton and Leicester. He has completed a research project on the parental perception towards therapeutic hypothermia. His research interest is the use of simulation as quality improvement methodology, in patient safety and risk.</p>

				</div><!--
				--><div class="textPlusStackedImage__column textPlusStackedImage__column--image">
					<div class="textPlusStackedImage__column-wrapper">
						<div class="textPlusStackedImage__column-imageWrapper textPlusStackedImage__column-imageWrapper--portrait">
							<img class="sizing-image" src="/img/dr-ranjit-gunda.jpg" alt="">
							<div class="textPlusStackedImage__column-imageWrapper-coverImage" style="background-image: url('/img/dr-ranjit-gunda@2x.jpg')"></div>
						</div>
						<div class="textPlusStackedImage__column-imageWrapper textPlusStackedImage__column-imageWrapper--landscape">
							<img class="sizing-image" src="/img/profile-image-2.jpg" alt="">
							<div class="textPlusStackedImage__column-imageWrapper-coverImage" style="background-image: url('/img/profile-image-2@2x.jpg')"></div>
						</div>
					</div>


				</div>

			</div> <!-- END INNER -->

		</section>

		<section class="textPlusStackedImage textPlusStackedImage no-padding">

			<!-- INNER START -->
			<div class="textPlusStackedImage__inner constrain">

				<div class="textPlusStackedImage__column">

					<h2 class="textPlusStackedImage__column-heading">Dr Jasim Shihab</h2>
					<p class="textPlusStackedImage__column-text">Dr Jasim Shihab is a Consultant Neonatologist  at Lancashire Woman and Newborn Centre, East Lancashire NHS Trust. Dr Shihab has trained and worked in neonatal intensive care in Bangalore, Chennai and Trivandrum  in  India and Medway, Aberdeen and London in the United Kingdom. He has keen interest in simulation. He was the trainee lead for simulation when working for the Royal London Neonatal Unit. He is currently the neonatal simulation lead for the East Lancashire NHS trust. Jasim has a special interest in moulage, manikin modification and difficult neonatal airway management.</p>

				</div><!--
				--><div class="textPlusStackedImage__column textPlusStackedImage__column--image">
					<div class="textPlusStackedImage__column-wrapper">
						<div class="textPlusStackedImage__column-imageWrapper textPlusStackedImage__column-imageWrapper--portrait">
							<img class="sizing-image" src="/img/dr-jasim-shihab.jpg" alt="">
							<div class="textPlusStackedImage__column-imageWrapper-coverImage" style="background-image: url('/img/dr-jasim-shihab@2x.jpg')"></div>
						</div>
						<div class="textPlusStackedImage__column-imageWrapper textPlusStackedImage__column-imageWrapper--landscape">
							<img class="sizing-image" src="/img/awaiting-image-squareish-no-text.png" alt="">
							<div class="textPlusStackedImage__column-imageWrapper-coverImage" style="background-image: url('/img/awaiting-image-squareish-no-text@2x.png')"></div>
						</div>
					</div>


				</div>

			</div> <!-- END INNER -->

		</section>

		<section class="textPlusStackedImage textPlusStackedImage--reversed no-padding">

			<!-- INNER START -->
			<div class="textPlusStackedImage__inner constrain">

				<div class="textPlusStackedImage__column">

					<h2 class="textPlusStackedImage__column-heading">Hinge</h2>
					<p class="textPlusStackedImage__column-text">We're a <a href="https://www.wearehinge.com">digital agency</a> based in Southampton and we love nothing more than getting involved with projects that make a real difference to the world we live in.</p>
					<p class="textPlusStackedImage__column-text">We don't see ourselves as an external company, rather that we're an invested partner in the online neonatal eduation platform and ultimately an extension of the MPROvE academy team.</p>
					<p class="textPlusStackedImage__column-text">We'll be helping to deliver the MPROvE educational resources mproveacademy.com in a consistent and reliable way.</p>

				</div><!--
				--><div class="textPlusStackedImage__column textPlusStackedImage__column--image">
					<div class="textPlusStackedImage__column-wrapper">
						<div class="textPlusStackedImage__column-imageWrapper textPlusStackedImage__column-imageWrapper--portrait">
							<img class="sizing-image" src="/img/hinge-digital-agency-southampton.jpg" alt="">
							<div class="textPlusStackedImage__column-imageWrapper-coverImage" style="background-image: url('/img/hinge-digital-agency-southampton@2x.jpg')"></div>
						</div>
						<div class="textPlusStackedImage__column-imageWrapper textPlusStackedImage__column-imageWrapper--landscape">
							<img class="sizing-image" src="/img/hinge-digital-agency-southampton-office.jpg" alt="">
							<div class="textPlusStackedImage__column-imageWrapper-coverImage" style="background-image: url('/img/hinge-digital-agency-southampton-office@2x.jpg')"></div>
						</div>
					</div>


				</div>

			</div> <!-- END INNER -->

		</section>

		@include("blocks.pageBreak")

		<div class="threeColumnImagePlusTitleText no-padding-top">
			<div class="threeColumnImagePlusTitleText__inner">
				<ul class="threeColumnImagePlusTitleText__items">
					<li class="threeColumnImagePlusTitleText__items-item">
						<a href="http://www.mproveacademy.org/partnerships">
							<div class="threeColumnImagePlusTitleText__items-item-image" style="background-image: url('/img/partnership@2x.png')"></div>
							<h2 class="threeColumnImagePlusTitleText__items-item-title">Partnerships</h2>
							<p class="threeColumnImagePlusTitleText__items-item-text">MPROvE has partnerships with simulation fraternity across the globe.</p>
						</a>
					</li><li class="threeColumnImagePlusTitleText__items-item">
						<a href="http://www.mproveacademy.org/voluntary-work">
							<div class="threeColumnImagePlusTitleText__items-item-image" style="background-image: url('/img/voluntary-work@2x.png')"></div>
							<h2 class="threeColumnImagePlusTitleText__items-item-title">Voluntary work</h2>
							<p class="threeColumnImagePlusTitleText__items-item-text">Helping train and set up simulation in many institutions worldwide.</p>
						</a>
					</li><li class="threeColumnImagePlusTitleText__items-item">
						<a href="http://www.mproveacademy.org/mprove-videos​">
							<div class="threeColumnImagePlusTitleText__items-item-image" style="background-image: url('/img/mprove-youtube@2x.png')"></div>
							<h2 class="threeColumnImagePlusTitleText__items-item-title">MPROvE YouTube Channel</h2>
							<p class="threeColumnImagePlusTitleText__items-item-text">We have videos for procedural skills training, escalation, human factors training and patient safety in neonatal care.</p>
						</a>
					</li><li class="threeColumnImagePlusTitleText__items-item">
						<a href="http://www.mproveacademy.org/webinars">
							<div class="threeColumnImagePlusTitleText__items-item-image" style="background-image: url('/img/webinar@2x.png')"></div>
							<h2 class="threeColumnImagePlusTitleText__items-item-title">Blog and Webinars</h2>
							<p class="threeColumnImagePlusTitleText__items-item-text">Interact with experts in neonatology, simulation and TEL.</p>
						</a>
					</li><li class="threeColumnImagePlusTitleText__items-item">
						<a href="http://www.mproveacademy.org/awards">
							<div class="threeColumnImagePlusTitleText__items-item-image" style="background-image: url('/img/awards@2x.png')"></div>
							<h2 class="threeColumnImagePlusTitleText__items-item-title">Awards</h2>
							<p class="threeColumnImagePlusTitleText__items-item-text">The MPROvE programme has received many awards and its research has been presented nationally and internationally.</p>
						</a>
					</li><li class="threeColumnImagePlusTitleText__items-item">
						<a href="http://www.mproveacademy.org/research">
							<div class="threeColumnImagePlusTitleText__items-item-image" style="background-image: url('/img/research@2x.png')"></div>
							<h2 class="threeColumnImagePlusTitleText__items-item-title">Research</h2>
							<p class="threeColumnImagePlusTitleText__items-item-text">MPROvE has done research in various areas of multi-professional education and simulation.</p>
						</a>
					</li>
				</ul>
			</div>
		</div>

		@include("blocks.twoColumnCards")
</div>
@endsection

@section("scripts")

@endsection