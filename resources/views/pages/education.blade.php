@extends("master")

@section("title")
	Education Resources
@endsection

@section("stylesheets")

@endsection

@section("content")
	<div class="content-wrapper">
		@include("blocks.pageHero")
		@include("blocks.moduleCards")
		@include("blocks.twoColumnCards")
	</div>
@endsection

@section("scripts")

@endsection