@extends("master")

@section("title")
    {{ $meta_title }}
@endsection

@section("stylesheets")

@endsection

@section("content")

    <training-course-show
            :training-course-id="{{$trainingCourseId}}"
            :banner-back="{{ json_encode($banner['back']) }}"
            :intro-title="'{{ $intro['title'] }}'"
            :intro-description="'{{ $intro['description'] }}'"
    ></training-course-show>

@endsection

@section("scripts")

@endsection
