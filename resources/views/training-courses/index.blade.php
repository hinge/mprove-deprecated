@extends("master")

@section("title")
 {{ $meta_title  }}
@endsection

@section("stylesheets")

@endsection

@section("content")

    <training-course-index
            :banner-title="'{{ $banner['title'] }}'"
            :banner-description="'{{ $banner['description'] }}'"
            :banner-back="{{ json_encode($banner['back']) }}"
            :intro-title="'{{ $intro['title'] }}'"
            :intro-description="'{{ $intro['description'] }}'"
    ></training-course-index>

@endsection

@section("scripts")

@endsection
