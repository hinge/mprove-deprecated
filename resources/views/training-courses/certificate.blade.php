<!DOCTYPE html>
<html>
    <head>
        @include("partials._head")
    </head>

    <body>
        <div class="certificate-background text-center">
            <div class="pt-32 text-center">

                <img src="/img/mprove-logo@2x.jpg" srcset="/img/mprove-logo.jpg 1x, /img/mprove-logo@2x.jpg 2x" alt="MPROvE Logo" class="inline-block mb-16 w-full max-w-xs pb-2 pt-12">
                <h1 class="text-blue text-center text-5xl font-avertaBlack mb-4">Certificate of Completion</h1>
                <h2 class="text-blue text-3xl mb-16">This acknowledges that on {{ $certificateData['completion_date'] }} </h2>


                <p class="text-center text-5xl mb-16 ">
                    <span class="border-bottom font-avertaSemiBold border-b-4 border-dashed pb-4 text-sky border-sky w-full max-w-xl inline-block">
                        {{ $certificateData['participant_name'] }}
                    </span>
                </p>

                <p class="text-blue text-3xl text-center mb-6">
                    <span class="max-w-5xl w-full inline-block w-full max-w-md">
                        Has successfully completed <span class="font-avertaSemiBold">{{ $certificateData['course_name'] }} </span> online training course and <span class="font-avertaSemiBold">scored {{ $certificateData['course_score'] }}%.</span>
                    </span>
                </p>
                
                <p class="text-blue text-2xl mb-20">2 Continual Professional Development hours are also awarded.</p>

                <img src="/img/alok_sharma_signature.svg" alt="" class="inline-block w-full max-w-xs mb-6 px-4">
                <p class="text-blue text-2xl">Dr Alok Sharma, Program Coordinator</p>
            </div>
        </div>
    </body>
</html>
