@extends("master")

@section("title")
    {{ $meta_title }}
@endsection

@section("stylesheets")

@endsection

@section("content")

    <training-course-questions
            :training-course-id="{{$trainingCourseId}}"
            :banner-back="{{ json_encode($banner['back']) }}"
            :intro-description="'{{ $intro['description'] }}'"
    ></training-course-questions>

@endsection

@section("scripts")

@endsection
