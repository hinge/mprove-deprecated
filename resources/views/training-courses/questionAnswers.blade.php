@extends("master")

@section("title")
    {{ $meta_title }}
@endsection

@section("stylesheets")

@endsection

@section("content")

    <training-course-question-answers
            :training-course-id="{{$trainingCourseId}}"
            :banner-back="{{ json_encode($banner['back']) }}"
            :intro-description="'{{ $intro['description'] }}'"
    ></training-course-question-answers>

@endsection

@section("scripts")

@endsection
