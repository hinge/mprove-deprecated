@extends("master")

@section("title")
    {{ $meta_title }}
@endsection

@section("stylesheets")

@endsection

@section("content")

    <training-course-questions-summary
            :training-course-id="{{$trainingCourseId}}"
            :banner-back="{{ json_encode($banner['back']) }}"
            :intro-description="'{{ $intro['description'] }}'"
    ></training-course-questions-summary>

@endsection

@section("scripts")

@endsection
