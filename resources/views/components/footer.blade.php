<!-- START FOOTER -->
<div class="footer">
	
	<!-- WRAPPER BORDER START -->
	<div class="footer__wrapper bg-blue">
			
		<!-- TOP START -->
		<div class="footer__top constrain">

			<!-- START TOP COLUMNS -->
			<div class="footer__top-columns">
				@php
					$footer_column_1 = $menus->firstWhere('slug', 'footer-column-1') ? $menus->firstWhere('slug', 'footer-column-1')['menuItems'] : null;
					$footer_column_2 = $menus->firstWhere('slug', 'footer-column-2') ? $menus->firstWhere('slug', 'footer-column-2')['menuItems'] : null;
					$footer_column_3 = $menus->firstWhere('slug', 'footer-column-3') ? $menus->firstWhere('slug', 'footer-column-3')['menuItems'] : null;
				@endphp

				@if ($footer_column_1)
					@if($footer_column_1->first()['enabled'] == 'true')
						<div class="footer__top-columns-column">
							@foreach($footer_column_1 as $nav_item)
								<h4 class="footer__top-columns-column-heading text-white">
									@if($nav_item['type'] == 'static-url')
										<a class="text-white" target="{{ $nav_item['target'] }}" href="{{ $nav_item['value'] }}">
											{{ $nav_item['name'] }}
										</a>
									@else
										{{ $nav_item['name'] }}
									@endif
								</h4>

								@if (count($nav_item['children']) > 0)
									<ul class="footer__top-columns-column-items">
										@foreach ($nav_item['children'] as $nav_item_child)
											@if($nav_item_child['enabled'] == 'true')
												<li class="footer__top-columns-column-items-item">

													@if($nav_item_child['type'] == 'static-url')
														<a class="text-white" target="{{ $nav_item_child['target'] }}" href="{{ $nav_item_child['value'] }}">
															{{ $nav_item_child['name'] }}
														</a>
													@else
														<a class="text-white" href="#">
															{{ $nav_item_child['name'] }}
														</a>
													@endif
												</li>
											@endif
										@endforeach
									</ul>
								@endif
							@endforeach
						</div>
					@endif
				@endif

				@if ($footer_column_2)
					@if($footer_column_2->first()['enabled'] == 'true')
						<div class="footer__top-columns-column">
							@foreach($footer_column_2 as $nav_item)
								<h4 class="footer__top-columns-column-heading text-white">
									@if($nav_item['type'] == 'static-url')
										<a class="text-white" target="{{ $nav_item['target'] }}" href="{{ $nav_item['value'] }}">
											{{ $nav_item['name'] }}
										</a>
									@else
										{{ $nav_item['name'] }}
									@endif
								</h4>

								@if (count($nav_item['children']) > 0)
									<ul class="footer__top-columns-column-items">
										@foreach ($nav_item['children'] as $nav_item_child)
											@if($nav_item_child['enabled'] == 'true')
												<li class="footer__top-columns-column-items-item">

													@if($nav_item_child['type'] == 'static-url')
														<a class="text-white" target="{{ $nav_item_child['target'] }}" href="{{ $nav_item_child['value'] }}">
															{{ $nav_item_child['name'] }}
														</a>
													@else
														<a class="text-white" href="#">
															{{ $nav_item_child['name'] }}
														</a>
													@endif
												</li>
											@endif
										@endforeach
									</ul>
								@endif
							@endforeach
						</div>
					@endif
				@endif
				
				<div class="footer__top-columns-column">
				</div>
				{{-- <h4 class="footer__top-columns-column-heading">MPROvE Videos</h4>
				<ul class="footer__top-columns-column-items">
					<li class="footer__top-columns-column-items-item">
						<a href="#" class="footer__top-columns-column-items-item-link">Register for free</a>
					</li>
				</ul> --}}

				@if ($footer_column_3)
					@if($footer_column_3->first()['enabled'] == 'true')
						<div class="footer__top-columns-column">
							@foreach($footer_column_3 as $nav_item)
								<h4 class="footer__top-columns-column-heading text-white">
									@if($nav_item['type'] == 'static-url')
										<a class="text-white" target="{{ $nav_item['target'] }}" href="{{ $nav_item['value'] }}">
											{{ $nav_item['name'] }}
										</a>
									@else
										{{ $nav_item['name'] }}
									@endif
								</h4>

								@if (count($nav_item['children']) > 0)
									<ul class="footer__top-columns-column-items">
										@foreach ($nav_item['children'] as $nav_item_child)
											@if($nav_item_child['enabled'] == 'true')
												<li class="footer__top-columns-column-items-item">

													@if($nav_item_child['type'] == 'static-url')
														<a class="text-white" target="{{ $nav_item_child['target'] }}" href="{{ $nav_item_child['value'] }}">
															{{ $nav_item_child['name'] }}
														</a>
													@else
														<a class="text-white" href="#">
															{{ $nav_item_child['name'] }}
														</a>
													@endif
												</li>
											@endif
										@endforeach
									</ul>
								@endif
							@endforeach
						</div>
					@endif
				@endif
				
			</div> <!-- END TOP COLUMNS -->

		</div> <!-- END TOP -->	

	</div> <!-- END WRAPPER BORDER -->

	<!-- BOTTOM START -->
	<div class="footer__bottom constrain">
		
		<!-- START BOTTOM COLUMNS -->
		<div class="footer__bottom-columns">
			
			<div class="footer__bottom-columns-column footer__bottom-columns-column--left">
				
				<p class="footer__bottom-columns-column-text">&copy; Copyright MPROvE {{ date('Y') }}. All rights reserved.</p>

			</div><!--
			--><div class="footer__bottom-columns-column footer__bottom-columns-column--right">
					
				<p class="footer__bottom-columns-column-text"><a href="https://hinge.agency">Site designed &amp; built by <span class="text-blue">hinge.</span></a></p>

			</div>

		</div> <!-- END BOTTOM COLUMNS -->

	</div> <!-- END BOTTOM -->

</div> <!-- END FOOTER -->
