<site-header :user="user" inline-template>
	<div>
		<header class="headerDesktop">

			<div class="headerDesktop__inner">
				<div class="headerDesktop__logo">
					<a href="/">
						<img src="/img/mprove-logo.jpg" srcset="/img/mprove-logo.jpg 1x, /img/mprove-logo@2x.jpg 2x" alt="MPROvE Logo" />
					</a>
				</div>

				<nav class="headerDesktop__leftNav">
					<ul class="headerDesktop__leftNav-items">

						@php
							$site_header_menu = $menus->firstWhere('slug', 'main-menu') ? $menus->firstWhere('slug', 'main-menu')['menuItems'] : null;
							$site_header_right_menu = $menus->firstWhere('slug', 'secondary-menu') ? $menus->firstWhere('slug', 'secondary-menu')['menuItems'] : null;
						@endphp

						@if ($site_header_menu)
							@foreach($site_header_menu as $nav_item)
								@if($nav_item['enabled'] == 'true')
									<li class="headerDesktop__leftNav-items-item {{ $nav_item['children']->count() ? 'has-children' : '' }}">
										@if($nav_item['type'] == 'static-url')
											<a target="{{ $nav_item['target'] }}" href="{{ $nav_item['value'] }}"
											   class="headerDesktop__leftNav-items-item-dropdown {{ Request::is(trim($nav_item['value'],'/').'*') ? 'headerDesktop__leftNav-items-item-dropdown--active' : '' }}"
											   >
												<span class="headerDesktop__leftNav-items-item-dropdown-text">{{ $nav_item['name'] }}</span>

												@if($nav_item['children']->count())
													<i class="headerDesktop__leftNav-items-item-dropdown-icon"></i>
												@endif
											</a>
										@else
											<a href="#" class="headerDesktop__leftNav-items-item-dropdown">
												<span class="headerDesktop__leftNav-items-item-dropdown-text">{{ $nav_item['name'] }}</span>
												@if($nav_item['children']->count())
													<i class="headerDesktop__leftNav-items-item-dropdown-icon"></i>
												@endif
											</a>
										@endif

										@if ($nav_item['children'] && $nav_item['children']->count())
											<div class="headerDesktop__leftNav-items-item-dropdown-menu">
												@foreach ($nav_item['children'] as $nav_item_child)
													@if($nav_item_child['enabled'] == 'true')
														@if($nav_item_child['type'] == 'static-url')
															<a target="{{ $nav_item_child['target'] }}" href="{{ $nav_item_child['value'] }}"
															   class="headerDesktop__leftNav-items-item-dropdown-menu-link {{ Request::is(trim($nav_item_child['value'],'/').'*') ? 'headerDesktop__leftNav-items-item-dropdown-menu-link--active' : '' }}">
															   {{ $nav_item_child['name'] }}
															</a>
														@else
															<a href="#"
															   class="headerDesktop__leftNav-items-item-dropdown-menu-link">
																{{ $nav_item_child['name'] }}
															</a>
														@endif
													@endif
													<div class="clearfix"></div>
												@endforeach
											</div>
										@endif
									</li>
								@endif
							@endforeach
						@endif
					</ul>

				</nav>

				<nav class="headerDesktop__rightNav">
					<ul class="headerDesktop__rightNav-items">
						@if ($site_header_right_menu)
							@foreach($site_header_right_menu as $nav_item)
								@if($nav_item['enabled'] == 'true')
									<li class="headerDesktop__rightNav-items-item">
										@if($nav_item['type'] == 'static-url')
											<a target="{{ $nav_item['target'] }}" href="{{ $nav_item['value'] }}"
											   class="headerDesktop__rightNav-items-item-link {{ Request::is(trim($nav_item['value'],'/').'*') ? 'headerDesktop__rightNav-items-item-link--active' : '' }}"
											   >
												{{ $nav_item['name'] }}
											</a>
										@else
											<a href="#" class="headerDesktop__leftNav-items-item-dropdown">
												<span class="headerDesktop__leftNav-items-item-dropdown-text">{{ $nav_item['name'] }}</span>
												<i class="headerDesktop__leftNav-items-item-dropdown-icon"></i>
											</a>
										@endif
									</li>
								@endif
							@endforeach
						@endif
						@if (Auth::user())
							<li class="headerDesktop__rightNav-items-itemDivider"></li>
							<li class="headerDesktop__rightNav-items-item">
								<a href="/settings"
								   class="headerDesktop__rightNav-items-item-link {{ Request::is('settings*') ? 'headerDesktop__rightNav-items-item-link--active' : '' }}"
								   >
									My Account
								</a>
							</li>
							<li class="headerDesktop__rightNav-items-item  headerDesktop__rightNav-items-item--logoutButton">
								<a href="/logout" class="headerDesktop__rightNav-items-item-link">Logout</a>
							</li>
						@endif
					</ul>
					@if (!Auth::user())
						<div class="headerDesktop__actions">
							<a href="#" class="headerDesktop__actions-button" id="desktopAction" v-on:click.prevent="showAuthModal()">
								<span class="headerDesktop__actions-button-text">Sign Up</span><!--
								--><i class="headerDesktop__actions-button-icon">&bull;</i><!--
								--><span class="headerDesktop__actions-button-text">Sign In</span>
							</a>
						</div>
					@endif
				</nav>
			</div>
		</header>

		<header class="headerMobile">
			<div class="headerMobile__inner">
				<div class="headerMobile__logo">
					<a href="/">
						<img src="/img/mprove-logo.jpg" srcset="/img/mprove-logo.jpg 1x, /img/mprove-logo@2x.jpg 2x" alt="MPROvE Logo" />
					</a>
				</div>
				<div class="headerMobile__actionBar">
					@if (!Auth::user())
						<a href="#" class="headerMobile__actionBar-button" id="mobileAction" v-on:click="showAuthModal()"></a>
					@endif
					<div class="headerMobile__actionBar-hamburger {{ (Auth::user() ? 'loggedInUser' : '') }}" v-bind:class="{'active' : responsiveNavOpen}" id="hamburger" v-on:click="toggleResponsiveNav()">
						<span class="headerMobile__actionBar-hamburger-line headerMobile__actionBar-hamburger-line--lineOne"></span>
						<span class="headerMobile__actionBar-hamburger-line headerMobile__actionBar-hamburger-line--lineTwo"></span>
						<span class="headerMobile__actionBar-hamburger-line headerMobile__actionBar-hamburger-line--lineThree"></span>
					</div>
				</div>
			</div>
		</header>

		<nav class="mobileSidebarNav" v-bind:class="{'showSidebar' : responsiveNavOpen}">

			<div class="mobileSidebarNav__scrollWrapper">
				<ul class="mobileSidebarNav__block">
					@if ($site_header_menu)
						@foreach($site_header_menu as $nav_item)
							@if($nav_item['enabled'] == 'true')
								<li class="mobileSidebarNav__block-item">
{{--									<i class="mobileSidebarNav__block-item-arrow"></i>--}}
									<a target="{{ $nav_item['type'] == 'static-url' ? $nav_item['target'] : '' }}" href="{{ $nav_item['type'] == 'static-url' ? $nav_item['value']: '#' }}" class="mobileSidebarNav__block-item-link">{{ $nav_item['name'] }}</a>
									@if (count($nav_item['children']) > 0)
										<ul class="mobileSidebarNav__block-dropdown">
											@foreach ($nav_item['children'] as $nav_item_child)
												<li class="mobileSidebarNav__block-dropdown-item">
													@if($nav_item_child['enabled'] == 'true')
														@if($nav_item_child['type'] == 'static-url')
															<a target="{{ $nav_item_child['target'] }}" href="{{ $nav_item_child['value'] }}" class="mobileSidebarNav__block-dropdown-item-link">{{ $nav_item_child['name'] }}</a>
														@else
															<a href="#" class="mobileSidebarNav__block-dropdown-item-link">{{ $nav_item_child['name'] }}</a>
														@endif
													@endif
												</li>
											@endforeach
										</ul>
									@endif
								</li>
							@endif
						@endforeach
					@endif
				</ul>

				<ul class="mobileSidebarNav__block">
					@if ($site_header_right_menu)
						@foreach($site_header_right_menu as $nav_item)
							@if($nav_item['enabled'] == 'true')
								<li class="mobileSidebarNav__block-item">
									@if($nav_item['type'] == 'static-url')
										<a target="{{ $nav_item['target'] }}" href="{{ $nav_item['value'] }}" class="mobileSidebarNav__block-item-link">{{ $nav_item['name'] }}</a>
									@else
										<a href="#" class="mobileSidebarNav__block-item-link">{{ $nav_item['name'] }}</a>
									@endif
								</li>
							@endif
						@endforeach
					@endif
					@if (Auth::user())
						<li class="mobileSidebarNav__block-item">
							<a href="/settings" class="mobileSidebarNav__block-item-link">My Account</a>
						</li>
						<li class="mobileSidebarNav__block-item">
							<a href="/logout" class="mobileSidebarNav__block-item-link">Logout</a>
						</li>
					@endif
				</ul>
			</div>

		</nav>
	</div>
</site-header>
