@extends("master")

@section("title")
	Education Resources
@endsection

@section("stylesheets")

@endsection

@section("content")

<div class="pageIntro">
	<div class="pageIntro__inner constrain">
		<h1 class="pageIntro__heading">Oops! Something went wrong somewhere.</h1>
		<p class="pageIntro__subHeading pageIntro__subHeading--wide pageIntro__subHeading--regularText">Sorry, something went wrong on our end. If you entered a web address please check it is correct then try again. Alternatively, use the button below to go to our homepage, or you can also visit our <a href="/mprove-academy">MPROvE Academy</a> or <a href="/quality-mprovement">Quality MPROvEment</a>.</p>
		<div class="pageIntro__actions">
			<a class="pageIntro__actions-action button button--iconRight button--large button--wide" href="/">
				MPROVE HOMEPAGE
				<svg width="14px" height="12px" viewBox="0 0 14 12" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
				    <g id="Designs---Iteration-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
				        <g id="Upcoming-Course-Page---Design" transform="translate(-829.000000, -527.000000)">
				            <g id="intro-&amp;-programme" transform="translate(270.000000, 159.000000)">
				                <g id="CTA-button" transform="translate(285.000000, 346.000000)">
				                    <g id="Group" transform="translate(43.000000, 17.000000)">
				                        <path class="fill" d="M239.474791,16.7163787 C239.27954,16.9053791 239.02304,17 238.766789,17 C238.511288,17 238.255787,16.9061033 238.060536,16.7180684 C237.669535,16.3415159 237.668785,15.7303422 238.058786,15.3528241 L241.590301,11.9655046 L232.000003,11.9655046 C231.447751,11.9655046 231,11.5331934 231,10.9999854 C231,10.4667773 231.447751,10.0344661 232.000003,10.0344661 L241.590301,10.0344661 L238.058786,6.64738794 C237.668785,6.26986991 237.669535,5.65845484 238.060536,5.28190233 C238.451538,4.90534982 239.08479,4.90607396 239.474791,5.28383337 L244.708062,10.3183288 C245.097313,10.6951226 245.097313,11.3048481 244.708062,11.6818833 L239.474791,16.7163787 Z" id="arrow-thin-right"></path>
				                    </g>
				                </g>
				            </g>
				        </g>
				    </g>
				</svg>
			</a>
		</div>
	</div>
</div>

@endsection

@section("scripts")

@endsection