<!DOCTYPE html>
<html>
<head>
	@include("partials._head")
	@yield('css')
</head>
<body>

	{{-- Main Body Content --}}
	<div id="spark-app" v-cloak>

		<section class="main">

			{{-- Header --}}
			@include("components.header", ['menus' => $menus])

			@yield("content")

			{{-- Footer --}}
			@include("components.footer", ['menus' => $menus])

			<auth-modal></auth-modal>

		</section>

	</div>

	{{-- Modals --}}
	@yield("modals")

	{{-- JavaScript --}}
	@include("partials._scripts")
	@yield('js')

</body>
</html>