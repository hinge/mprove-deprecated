<div class="pageIntro">
	<div class="pageIntro__inner constrain">
		<h1 class="pageIntro__heading">MPROvE are working hard to bring first class neonatal training to those who need it</h1>
		<p class="pageIntro__subHeading">Mauris ligula lorem, interdum vel ex a, faucibus consectetur metus. Etiam tempus eget nulla ac accumsan.</p>
	</div>
</div>