<!-- START LEARN BLOCK -->
<div class="fourColumnIconPlusTextGrid block-isWhite">
	
	<!-- INNER START -->
	<div class="fourColumnIconPlusTextGrid__inner constrain">
		
		<h2 class="fourColumnIconPlusTextGrid__heading">
			Learn with MPROvE. <span>Your way.</span>
		</h2>

		<!-- CARDS START --><ul class="fourColumnIconPlusTextGrid__cards">
			
			<li class="fourColumnIconPlusTextGrid__cards-card">
				
				<div class="fourColumnIconPlusTextGrid__cards-card-imageWrapper">
					<img src="/img/hat-svg.svg" alt="Mprove Academy Icon" class="fourColumnIconPlusTextGrid__cards-card-image">
				</div>
				<div class="fourColumnIconPlusTextGrid__cards-card-textWrapper">
					<h3 class="fourColumnIconPlusTextGrid__cards-card-title">MProve Academy</h3>
					<p class="fourColumnIconPlusTextGrid__cards-card-text">Theme based courses for Multidisciplinary Teams : Challenge Yourself</p>
				</div>
				<a href="/mprove-academy" class="fourColumnIconPlusTextGrid__cards-card-button"><span>Visit the academy</span><i></i></a>

			</li><!--
			--><li class="fourColumnIconPlusTextGrid__cards-card">
				
				<div class="fourColumnIconPlusTextGrid__cards-card-imageWrapper">
					<img src="/img/key-svg.svg" alt="Mprove Academy Icon" class="fourColumnIconPlusTextGrid__cards-card-image">
				</div>
				<div class="fourColumnIconPlusTextGrid__cards-card-textWrapper">
					<h3 class="fourColumnIconPlusTextGrid__cards-card-title">Quality MPROvEment</h3>
					<p class="fourColumnIconPlusTextGrid__cards-card-text">Toolkits & Care Bundles to Improve Neonatal Care</p>
				</div>
				<a href="/quality-mprovement/toolkits" class="fourColumnIconPlusTextGrid__cards-card-button"><span>See the toolkits</span><i></i></a>

			</li><!--
			--><li class="fourColumnIconPlusTextGrid__cards-card">
				
				<div class="fourColumnIconPlusTextGrid__cards-card-imageWrapper">
					<img src="/img/write-svg.svg" alt="Mprove Academy Icon" class="fourColumnIconPlusTextGrid__cards-card-image">
				</div>
				<div class="fourColumnIconPlusTextGrid__cards-card-textWrapper">
					<h3 class="fourColumnIconPlusTextGrid__cards-card-title">MPROvE Videos</h3>
					<p class="fourColumnIconPlusTextGrid__cards-card-text">Teaching neonatal skills through a visual and interactive approach</p>
				</div>
				<a href="/online-training-courses" class="fourColumnIconPlusTextGrid__cards-card-button"><span>See the courses</span><i></i></a>

			</li><!--
			--><li class="fourColumnIconPlusTextGrid__cards-card">
				
				<div class="fourColumnIconPlusTextGrid__cards-card-imageWrapper">
					<img src="/img/canvas-svg.svg" alt="Mprove Academy Icon" class="fourColumnIconPlusTextGrid__cards-card-image">
				</div>
				<div class="fourColumnIconPlusTextGrid__cards-card-textWrapper">
					<h3 class="fourColumnIconPlusTextGrid__cards-card-title">Conferences</h3>
					<p class="fourColumnIconPlusTextGrid__cards-card-text">Orci varius natoque penatibus et magnis dis parturient montes.</p>
				</div>
				<a class="fourColumnIconPlusTextGrid__cards-card-button"><span>Coming soon</span></a>

			</li>

		</ul> <!-- END CARDS -->

	</div> <!-- END INNER -->

</div> <!-- END LEARN BLOCK -->
