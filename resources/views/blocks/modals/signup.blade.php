{{-- SIGN UP/SIGN IN FORM --}}
<section class="registerModal">
	{{-- INNER --}}
	<div class="registerModal__inner">
		{{-- SIGN UP --}}
		<div class="registerModal__column-signUp">
			
			<img src="/img/signup-image.svg" class="registerModal__column-signUp-image">
			
			<h2 class="registerModal__column-signUp-heading">Create an MPROvE account</h2>
			<!-- START FORM --><div class="registerModal__column-signUp-form">
				
				<form method="POST">
					<div class="registerModal__column-signUp-form-block">
						
						<div class="registerModal__column-signUp-form-block-column">
							<input type="text" name="firstname" placeholder="First name" class="registerModal__inputText registerModal__inputText--firstName resetInputStyles">
						</div><!--
						--><div class="registerModal__column-signUp-form-block-column">
							<input type="text" name="surname" placeholder="Surname" class="registerModal__inputText registerModal__inputText--surname resetInputStyles">
						</div>

					</div>
					<div class="registerModal__column-signUp-form-block">
						
						<input type="email" name="email" placeholder="Your email address" class="registerModal__inputText registerModal__inputText--email resetInputStyles">

					</div>
					<div class="registerModal__column-signUp-form-block">
							
						<div class="registerModal__column-signUp-form-block-column">
							<input type="password" name="password" placeholder="Password" class="registerModal__inputText registerModal__inputText--password resetInputStyles">
						</div><!--
						--><div class="registerModal__column-signUp-form-block-column">
							<input type="password" name="password_confirm" placeholder="Repeat Password" class="registerModal__inputText registerModal__inputText--passwordConfirm resetInputStyles">
						</div>

					</div>
					<div class="registerModal__column-signIn-form-block">
						<input type="submit" name="signupSubmit" value="Create Account" class="registerModal__column-signUp-form-button resetInputStyles">
					</div>
				</form>

			</div> <!-- END FORM -->
		</div><!-- END SIGN UP
		SIGN IN--><div class="registerModal__column-signIn">

			<h2 class="registerModal__column-signIn-heading">Login</h2>
			<form method="POST">
				<div class="registerModal__column-signIn-form-block">
					<input type="email" name="emailLogin" placeholder="Your email address" class="registerModal__inputText registerModal__inputText--emailLogin resetInputStyles">
				</div>
				<div class="registerModal__column-signIn-form-block">
					<input type="password" name="passwordLogin" placeholder="Password" class="registerModal__inputText registerModal__inputText--passwordLogin resetInputStyles">
				</div>
				<div class="registerModal__column-signIn-form-block">
					<input type="submit" name="signinSubmit" value="Login" class="registerModal__column-signIn-button resetInputStyles">
				</div>
			</form>
			
			<div class="registerModal__column-signIn-form-block">
				<a href="#" class="registerModal__column-signIn-forgotPassword">Forgotten your password?</a>
			</div>
		</div> <!-- END SIGN IN -->

		<div class="registerModal__close">
			<span class="registerModal__close-cross registerModal__close-cross--left"></span>
			<span class="registerModal__close-cross registerModal__close-cross--right"></span>
		</div>

	</div> <!-- END INNER -->

</section> <!-- END SIGN UP/SIGN IN -->