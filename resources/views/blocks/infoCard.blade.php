<div class="infoCard">
	<div class="infoCard__inner">
		<h2 class="infoCard__heading">Neonatal Simulation Instructor Course</h2>
		<p class="infoCard__text">Cras tempus orci at augue convallis fermentum. Curabitur condimentum, eros in scelerisque viverra, ipsum lectus auctor erat, vel congue neque enim sagittis orci.</p>
		<a class="infoCard__button button" href="/mprove-academy/neonatal-simulation-instructor-course">See the courses</a>
	</div>
</div>