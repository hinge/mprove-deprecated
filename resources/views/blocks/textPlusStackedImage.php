<!-- START LOGO BANNER BLOCK -->
<div class="textPlusStackedImage">
	
	<!-- INNER START -->
	<div class="textPlusStackedImage__inner constrain">
		
		<div class="textPlusStackedImage__column">
			
			<h2 class="textPlusStackedImage__column-heading">Saving lives by developing skills</h2>
			<p class="textPlusStackedImage__column-text">The concept MPROvE relates to multidisciplinary education of any kind (i.e. multidisciplinary neonatal simulation, or TEL) being used to evaluate if we can improve the quality of care and or outcomes in neonates. It is covered by trademark.</p>
			<p class="textPlusStackedImage__column-text">Neonatal simualtion delivered as part of the MPROvE programme has been nominated for a BMJ award in 2014.</p>
			<a href="/about-mprove" class="textPlusStackedImage__column-link"><span>About MPROvE</span><i></i></a>

		</div><!--
		--><div class="textPlusStackedImage__column textPlusStackedImage__column--image">
			<div class="textPlusStackedImage__column-wrapper">
				<div class="textPlusStackedImage__column-imageWrapper textPlusStackedImage__column-imageWrapper--portrait">
					<img class="sizing-image" src="http://placehold.it/248x292" alt="">
					<div class="textPlusStackedImage__column-imageWrapper-coverImage" style="background-image: url('http://placehold.it/248x292')"></div>
				</div>
				<div class="textPlusStackedImage__column-imageWrapper textPlusStackedImage__column-imageWrapper--landscape">
					<img class="sizing-image" src="http://placehold.it/390x322" alt="">
					<div class="textPlusStackedImage__column-imageWrapper-coverImage" style="background-image: url('http://placehold.it/390x322')"></div>
				</div>	
			</div>
			

		</div>

	</div> <!-- END INNER -->

</div> <!-- END LOGO BANNER BLOCK -->