<!-- START LOGO BANNER BLOCK -->
<div class="testimonials">
	
	<!-- INNER START -->
	<div class="testimonials__inner constrain">
		
		<!-- BANNER START --><ul class="testimonials__items">
			
			<li class="testimonials__items-item">
				
				<div class="testimonials__items-item-imageWrapper">
					<img src="http://placehold.it/218x218" alt="Profile Image" class="testimonials__items-item-image">
				</div>
				<div class="testimonials__items-item-textWrapper">
					<p class="testimonials__items-item-quote"> “Quisque imperdiet ac ante ut rutrum. Nam nunc risus, dictum sit amet luctus sit amet, ullamcorper sit amet erat. Proin aliquam malesuada pellentesque." </p>
					<p class="testimonials__items-item-details">
						<span class="testimonials__items-item-details--name">Janny Smith</span>
						<span class="testimonials__items-item-details--place">Islington, London</span>
					</p>
				</div>
			</li><!--
			--><li class="testimonials__items-item">
				
				<div class="testimonials__items-item-imageWrapper">
					<img src="http://placehold.it/218x218" alt="Profile Image" class="testimonials__items-item-image">
				</div>
				<div class="testimonials__items-item-textWrapper">
					<p class="testimonials__items-item-quote"> “Quisque imperdiet ac ante ut rutrum. Nam nunc risus, dictum sit amet luctus sit amet, ullamcorper sit amet erat. Proin aliquam malesuada pellentesque." </p>
					<p class="testimonials__items-item-details">
						<span class="testimonials__items-item-details--name">Janny Smith</span>
						<span class="testimonials__items-item-details--place">Islington, London</span>
					</p>
				</div>

			</li><!--
			--><li class="testimonials__items-item">
				
				<div class="testimonials__items-item-imageWrapper">
					<img src="http://placehold.it/218x218" alt="Profile Image" class="testimonials__items-item-image">
				</div>
				<div class="testimonials__items-item-textWrapper">
					<p class="testimonials__items-item-quote"> “Quisque imperdiet ac ante ut rutrum. Nam nunc risus, dictum sit amet luctus sit amet, ullamcorper sit amet erat. Proin aliquam malesuada pellentesque." </p>
					<p class="testimonials__items-item-details">
						<span class="testimonials__items-item-details--name">Janny Smith</span>
						<span class="testimonials__items-item-details--place">Islington, London</span>
					</p>
				</div>

			</li>

		</ul> <!-- END BANNER -->

	</div> <!-- END INNER -->

</div> <!-- END LOGO BANNER BLOCK -->