<div class="moduleCards block-isWhite">
	<div class="moduleCards__inner constrain">
		<ul class="moduleCards__cards">
				
		@for ($i = 0; $i <= 6; $i++)<!--
			--><li class="moduleCards__cards-card">
    			<div class="moduleCards__cards-card-imageWrapper">
				<img src="/img/hat-svg.svg" alt="Mprove Academy Icon" class="moduleCards__cards-card-image">
				</div>
				<div class="moduleCards__cards-card-textWrapper">
					<h3 class="moduleCards__cards-card-title">Module Title<br>could be two lines</h3>
					<p class="moduleCards__cards-card-totalVideos"><span>14</span> videos</p>
					<p class="moduleCards__cards-card-text">Orci varius natoque penatibus et magnis dis parturient montes.</p>
				</div>
				<a href="" class="moduleCards__cards-card-button"><span>Explore</span><i></i></a>

			</li><!--
			-->@endfor
				
		</ul>
	</div>
</div>