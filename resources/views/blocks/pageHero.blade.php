<div class="pageHero">
	
	<div class="pageHero__inner constrain">

		@if (isset($pageHero['back']))		
			<a class="pageHero__back" href="./">
				<img src="/img/breadcrumb-back-arrow.png" srcset="/img/breadcrumb-back-arrow.png 1x, /img/breadcrumb-back-arrow@2x.png 2x" alt="" />
				<span>{{ $pageHero['back']['text'] }}</span>
			</a>
		@endif

		@if (isset($pageHero['heading']))
			<h1 class="pageHero__heading">{{ $pageHero['heading'] }}</h1>
		@endif

		@if (isset($pageHero['intro']))
			<p class="pageHero__text {{ (isset($heroWide) && $heroWide ? 'pageHero__text--wide' : '') }}">{{ $pageHero['intro'] }}</p>
		@endif

	</div> <!-- END INNER -->

	<img src="/img/curve-svg.svg" alt="" class="homeAfterHeroBlock__curve-image">

</div> <!-- END HERO -->