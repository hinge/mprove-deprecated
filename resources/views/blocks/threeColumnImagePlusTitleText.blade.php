<div class="threeColumnImagePlusTitleText no-padding-top">
	<div class="threeColumnImagePlusTitleText__inner">
		<ul class="threeColumnImagePlusTitleText__items">
			<li class="threeColumnImagePlusTitleText__items-item">
				<a href="#">
					<div class="threeColumnImagePlusTitleText__items-item-image" style="background-image: url('http://placehold.it/280x150')"></div>
					<h2 class="threeColumnImagePlusTitleText__items-item-title">Simulation</h2>
					<p class="threeColumnImagePlusTitleText__items-item-text">Quisque imperdiet ac ante ut rutrum. Nam nunc risus, dictum sit amet luctus sit amet, ullamcorper sit amet erat. Proin aliquam malesuada pellentesque.</p>
				</a>
			</li><li class="threeColumnImagePlusTitleText__items-item">
				<a href="#">
					<div class="threeColumnImagePlusTitleText__items-item-image" style="background-image: url('http://placehold.it/280x150')"></div>
					<h2 class="threeColumnImagePlusTitleText__items-item-title">OPEN Concept</h2>
					<p class="threeColumnImagePlusTitleText__items-item-text">Quisque imperdiet ac ante ut rutrum. Nam nunc risus, dictum sit amet luctus sit amet, ullamcorper sit amet erat. Proin aliquam malesuada pellentesque.</p>
				</a>
			</li><li class="threeColumnImagePlusTitleText__items-item">
				<a href="#">
					<div class="threeColumnImagePlusTitleText__items-item-image" style="background-image: url('http://placehold.it/280x150')"></div>
					<h2 class="threeColumnImagePlusTitleText__items-item-title">MPROvE Video</h2>
					<p class="threeColumnImagePlusTitleText__items-item-text">Quisque imperdiet ac ante ut rutrum. Nam nunc risus, dictum sit amet luctus sit amet, ullamcorper sit amet erat. Proin aliquam malesuada pellentesque.</p>
				</a>
			</li><li class="threeColumnImagePlusTitleText__items-item">
				<a href="#">
					<div class="threeColumnImagePlusTitleText__items-item-image" style="background-image: url('http://placehold.it/280x150')"></div>
					<h2 class="threeColumnImagePlusTitleText__items-item-title">Task Training Models</h2>
					<p class="threeColumnImagePlusTitleText__items-item-text">Quisque imperdiet ac ante ut rutrum. Nam nunc risus, dictum sit amet luctus sit amet, ullamcorper sit amet erat. Proin aliquam malesuada pellentesque.</p>
				</a>
			</li><li class="threeColumnImagePlusTitleText__items-item">
				<a href="#">
					<div class="threeColumnImagePlusTitleText__items-item-image" style="background-image: url('http://placehold.it/280x150')"></div>
					<h2 class="threeColumnImagePlusTitleText__items-item-title">Feedback</h2>
					<p class="threeColumnImagePlusTitleText__items-item-text">Quisque imperdiet ac ante ut rutrum. Nam nunc risus, dictum sit amet luctus sit amet, ullamcorper sit amet erat. Proin aliquam malesuada pellentesque.</p>
				</a>
			</li><li class="threeColumnImagePlusTitleText__items-item">
				<a href="#">
					<div class="threeColumnImagePlusTitleText__items-item-image" style="background-image: url('http://placehold.it/280x150')"></div>
					<h2 class="threeColumnImagePlusTitleText__items-item-title">Awards</h2>
					<p class="threeColumnImagePlusTitleText__items-item-text">Quisque imperdiet ac ante ut rutrum. Nam nunc risus, dictum sit amet luctus sit amet, ullamcorper sit amet erat. Proin aliquam malesuada pellentesque.</p>
				</a>
			</li>
		</ul>
	</div>
</div>