<div class="programmeIntro no-padding-bottom">
	<div class="programmeIntro__inner constrain">
		<a class="programmeIntro__back" href="#">
			<svg width="11px" height="10px" viewBox="0 0 11 10" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
			    <g id="Designs---Iteration-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
			        <g id="fill Upcoming-Course-Page---Design" transform="translate(-677.000000, -162.000000)">
			            <g id="intro-&amp;-programme" transform="translate(270.000000, 157.000000)">
			                <g id="Link" transform="translate(407.000000, 0.000000)">
			                    <path class="fill" d="M6.65876439,14.7636489 C6.50535317,14.9211493 6.30381679,15 6.10247684,15 C5.90172618,15 5.70097552,14.9217527 5.5475643,14.765057 C5.240349,14.4512632 5.23975972,13.9419518 5.5461893,13.6273534 L8.32095101,10.8045872 L0.78571687,10.8045872 C0.351804729,10.8045872 0,10.4443278 0,9.99998779 C0,9.55564778 0.351804729,9.19538841 0.78571687,9.19538841 L8.32095101,9.19538841 L5.5461893,6.37282329 C5.23975972,6.05822493 5.240349,5.54871237 5.5475643,5.23491861 C5.8547796,4.92112485 6.35233481,4.9217283 6.65876439,5.23652781 L10.7706198,9.43194063 C11.0764601,9.74593554 11.0764601,10.25404 10.7706198,10.5682361 L6.65876439,14.7636489 Z" id="arrow-thin-right" transform="translate(5.500000, 10.000000) scale(-1, 1) translate(-5.500000, -10.000000) "></path>
			                </g>
			            </g>
			        </g>
			    </g>
			</svg>
			Date List
		</a>
		<h1 class="programmeIntro__heading">Neonatal Simulation Instructor Course</h1>
		<div class="programmeIntro__details">
			<div class="programmeIntro__details-dates"><strong>Dates</strong> 21st April 2017</div>
			<div class="programmeIntro__details-price"><strong>Price</strong> £230 per person</div>
			<div class="programmeIntro__details-venue"><strong>Venue</strong> 6 Northlans Road Southampton, SO31 8HE</div>
		</div>
		<div class="programmeIntro__intro">
			<p class="programmeIntro__intro-text">The MPROvE neonatal instructor training course is run to train individuals as neonatal simulation instructors. It is also oriented towards providing training in administering and developing neonatal simulation programmes as well as accreditation training for the CHSE nationally. The course has been run at different centers nationally on an annual basis.</p>
		</div>
		<div class="programmeIntro__actions">
			<a class="programmeIntro__actions-action button button--iconRight" href="#">
				Register for a place
				<svg width="14px" height="12px" viewBox="0 0 14 12" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
				    <g id="Designs---Iteration-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
				        <g id="Upcoming-Course-Page---Design" transform="translate(-829.000000, -527.000000)">
				            <g id="intro-&amp;-programme" transform="translate(270.000000, 159.000000)">
				                <g id="CTA-button" transform="translate(285.000000, 346.000000)">
				                    <g id="Group" transform="translate(43.000000, 17.000000)">
				                        <path class="fill" d="M239.474791,16.7163787 C239.27954,16.9053791 239.02304,17 238.766789,17 C238.511288,17 238.255787,16.9061033 238.060536,16.7180684 C237.669535,16.3415159 237.668785,15.7303422 238.058786,15.3528241 L241.590301,11.9655046 L232.000003,11.9655046 C231.447751,11.9655046 231,11.5331934 231,10.9999854 C231,10.4667773 231.447751,10.0344661 232.000003,10.0344661 L241.590301,10.0344661 L238.058786,6.64738794 C237.668785,6.26986991 237.669535,5.65845484 238.060536,5.28190233 C238.451538,4.90534982 239.08479,4.90607396 239.474791,5.28383337 L244.708062,10.3183288 C245.097313,10.6951226 245.097313,11.3048481 244.708062,11.6818833 L239.474791,16.7163787 Z" id="arrow-thin-right"></path>
				                    </g>
				                </g>
				            </g>
				        </g>
				    </g>
				</svg>
			</a>
		</div>
	</div>
</div>
