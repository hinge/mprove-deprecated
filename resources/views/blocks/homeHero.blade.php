<!-- START HERO -->
<div class="homeHero block-isWhite">

	<!-- INNER -->	
	<div class="homeHero__inner constrain">
		
		<div class="homeHero__textBlock">
			
			<h1 class="homeHero__textBlock-title">
				Working together to improve <span>Neonatal Care</span>
			</h1>
			<p class="homeHero__textBlock-text">
				Teams that train together improve Neonatal Care together.
			</p>
			<a href="/mprove-academy" class="homeHero__textBlock-button"><span>See upcoming courses</span><!----><i></i></a>

		</div><!-- END TEXT BLOCK
		--><div class="homeHero__videoBlock">
				
			<div class="homeHero__videoBlock-wrapper">
				<img class="sizing-image" src="http://placehold.it/550x344" alt="">
				<div class="homeHero__videoBlock-wrapper-coverImage" style="background-image: url('http://placehold.it/550x344')"></div>
				<img src="/img/play-svg.svg" alt="Play Button" class="homeHero__videoBlock-playButton">
			</div>

		</div> <!-- END VIDEO BLOCK -->
		
	</div> <!-- END INNER -->

	<img src="/img/curve-svg.svg" alt="" class="homeAfterHeroBlock__curve-image">

</div> <!-- END HERO -->
<div class="clearfix"></div>
