var base = require('settings/profile/update-contact-information');

Vue.component('spark-update-contact-information', {
    mixins: [base],

	data() {
        return {
            form: $.extend(true, new SparkForm({
				name: '',
                first_name: '',
				last_name: '',
                email: ''
            }), Spark.forms.updateContactInformation)
        };
    },

	watch: {
		form: {
			handler(newVal, oldVal) {
	            this.form.name = this.form.first_name + " " + this.form.last_name;
	        },
	        deep: true
		}
	},

	mounted() {
		this.form.name = this.user.name;
        this.form.first_name = this.user.name.split(' ')[0];
		this.form.last_name = this.user.name.split(' ')[1];
        this.form.email = this.user.email;
    },
});
