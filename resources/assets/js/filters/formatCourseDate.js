Vue.filter('formatCourseDate', function(value) {
  if (value) {
    return moment(String(value)).format('Do MMMM YYYY');
  }
});

Vue.filter('formatCourseDateShort', function(value) {
    if (value) {
        return moment(String(value)).format('Do MMM YYYY');
    }
});