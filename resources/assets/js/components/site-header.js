Vue.component('site-header', {
    props: ['user'],
	data: function() {
		return {
			responsiveNavOpen: false
		}
	},
	methods: {
		showAuthModal: function() {
			Bus.$emit('showAuthModalEvent');
		},
		toggleResponsiveNav: function() {
			this.responsiveNavOpen = (this.responsiveNavOpen ? false : true);
			$('body .main').toggleClass('hasSidebar');
			$('body, html').toggleClass('stopScroll');
			// ($('.main').hasClass('hasSidebar') ? $('.main').removeClass('hasSidebar') : $('.main').addClass('hasSidebar'));
			// ($('body, html').hasClass('stopScroll') ? $('body, html').removeClass('stopScroll') : $('body, html').addClass('stopScroll'));
		},
		bindEvents(){
			var $this = this;

			/* Close Mobile Sidebar by clicking withing main content area */

			// $(".main").on('click', function() {
			// 	($(this).hasClass('hasSidebar') ? $(this).removeClass('hasSidebar') : '');
			// 	($('.mobileSidebarNav').hasClass('showSidebar') ? $('.mobileSidebarNav').removeClass('showSidebar') : '');
			// 	($('.headerMobile__actionBar-hamburger').hasClass('active') ? $('.headerMobile__actionBar-hamburger').removeClass('active') : '');
			// });

			// /* Open Sub Menu by clicking arrow */

			$(".mobileSidebarNav__block-item-arrow").on('click', function() {
				$(this).parent().hasClass('openSubMenu') ? $(this).parent().removeClass('openSubMenu') : $(this).parent().addClass('openSubMenu');
			});

			$(window).resize(function(){
				if ($(this).outerWidth() >= 1390 && $this.responsiveNavOpen){
					$this.toggleResponsiveNav();
				}
			});

			/* Open Signin/Signup Form */

		}
	},
	mounted() {
		var $this = this;
    	$this.bindEvents();
    }
});
