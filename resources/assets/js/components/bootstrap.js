require('./../spark-components/bootstrap');

require('./site-header');
require('./forums');

Vue.component('auth-modal', require('./auth-modal').default);
Vue.component('gated-course-content-modal', require('./gated-course-content-modal').default);
Vue.component('course-dates', require('./course-dates').default);
Vue.component('course-details', require('./course-details').default);
Vue.component('courses', require('./courses').default);
Vue.component('resources', require('./resources').default);
Vue.component('training-course-index', require('./TrainingCourses/Index').default);
Vue.component('training-course-show', require('./TrainingCourses/Show').default);
Vue.component('training-course-questions', require('./TrainingCourses/Questions').default);
Vue.component('training-course-question-answers', require('./TrainingCourses/QuestionAnswers').default);
Vue.component('training-course-questions-summary', require('./TrainingCourses/QuestionsSummary').default);

// This needs to be moved into a global JS file
var resizeFourColumnIconPlusTextGrid = function(){

	if ($('.fourColumnIconPlusTextGrid.auto-height').length > 0){
		setTimeout(function(){
			$('.fourColumnIconPlusTextGrid.auto-height').each(function(){
				var height = 0;
				$(this).find('.fourColumnIconPlusTextGrid__cards-card').removeAttr('style');
				$(this).find('.fourColumnIconPlusTextGrid__cards-card').each(function(){
					var $card = $(this);
					var cardHeight = $card.innerHeight();
					if (height <= cardHeight){
						height = cardHeight;
					}
				});		
				$(this).find('.fourColumnIconPlusTextGrid__cards-card').css({height : height + 'px'});
			});
		}, 1);
	}
}

resizeFourColumnIconPlusTextGrid();

$(window).resize(function(){
	resizeFourColumnIconPlusTextGrid();
});