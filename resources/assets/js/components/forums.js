Vue.component('forums', {
    props: ['user'],
	data: function() {
		return {
			'title' : ''
		}
	},
	methods: {
		bindEvents(){
			var $this = this;
			$('#login_required_btn, #login_or_register a').on('click', function(e){
				e.preventDefault();
				Bus.$emit('showAuthModalEvent');
			});
		}
	},
	mounted() {
		var $this = this;
		$this.bindEvents();
    }
});
