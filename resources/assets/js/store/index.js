import Vuex from 'vuex';
import Vue from 'vue';
import VueScrollTo from 'vue-scrollto';

Vue.use(Vuex);
Vue.use(VueScrollTo);

import trainingCourse from "@/store/modules/TrainingCourses/trainingCourse";
import userTrainingCourse from "@/store/modules/TrainingCourses/userTrainingCourse";
import userTrainingCourseMaterial from "@/store/modules/TrainingCourses/userTrainingCourseMaterial";
import userTrainingCourseQuestion from "@/store/modules/TrainingCourses/userTrainingCourseQuestion";
import userTrainingCourseAnsweredQuestion from "@/store/modules/TrainingCourses/userTrainingCourseAnsweredQuestion";

const debug = process.env.NODE_ENV !== 'production';

export default new Vuex.Store({
    strict: debug,
    modules: {
        trainingCourse,
        userTrainingCourse,
        userTrainingCourseMaterial,
        userTrainingCourseQuestion,
        userTrainingCourseAnsweredQuestion
    }
});
