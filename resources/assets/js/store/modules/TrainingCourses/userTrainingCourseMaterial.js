import userTrainingCourseMaterialApi from '@/services/userTrainingCourseMaterialApi';

const state = {
    setting_training_course_material_as_seen: false,
    revealed_training_course_material: null,
    collapsed_training_course_materials: [],
    hidden_training_course_materials: [],
    available_training_course_materials: []
};

const getters = {
    settingTrainingCourseMaterialAsSeen: () => state.setting_training_course_material_as_seen,
    revealedTrainingCourseMaterial: () => state.revealed_training_course_material,
    collapsedTrainingCourseMaterials: () => state.collapsed_training_course_materials,
    hiddenTrainingCourseMaterials: () => state.hidden_training_course_materials,
    availableTrainingCourseMaterials: () => state.available_training_course_materials,
};

const actions = {
    markTrainingCourseMaterialAsSeen({commit}, ids) {
        commit('setTrainingCourseMaterialAsSeen', true);
        return new Promise((resolve, reject) => {
            userTrainingCourseMaterialApi.setAsSeen(ids[0], ids[1])
                .then((data) => {
                    commit('setTrainingCourseMaterialAsSeen', false);
                    commit('setTrainingCourseMaterialAsRevealed', ids[1]);
                    return resolve(data);
                })
                .catch(error => {
                    commit('setTrainingCourseMaterialAsSeen', false);
                    return reject(error);
                });
        });
    },
    makeTrainingCourseMaterialAvailable({commit}, id) {
        commit('addToAvailableTrainingCourseMaterials', id);
    },
    revealTrainingCourseMaterial({commit}, id) {
        commit('setTrainingCourseMaterialAsRevealed', id);
    },
    collapseTrainingCourseMaterial({commit}, id) {
        commit('setTrainingCourseMaterialAsCollapsed', id);
    },
    collapseTrainingCourseMaterials({commit}, ids) {
        commit('setTrainingCourseMaterialsAsCollapsed', ids);
    },
    hideTrainingCourseMaterial({commit}, id) {
        commit('setTrainingCourseMaterialAsHidden', id);
    },
    purgeRevealedTrainingCourseMaterials({commit}) {
        commit('clearRevealedTrainingCourseMaterials');
    },
    purgeCollapsedTrainingCourseMaterials({commit}) {
        commit('clearCollapsedTrainingCourseMaterials');
    },
    purgeHiddenTrainingCourseMaterials({commit}) {
        commit('clearHiddenTrainingCourseMaterials');
    },
};

const mutations = {
    setTrainingCourseMaterialAsSeen(state, value) {
        state.setting_training_course_material_as_seen = value;
    },
    addToAvailableTrainingCourseMaterials(state, value) {
        if(!state.available_training_course_materials.includes(value)) {
            state.available_training_course_materials.push(value);
        }
    },
    setTrainingCourseMaterialAsRevealed(state, value) {
        state.revealed_training_course_material = value;
    },
    setTrainingCourseMaterialAsCollapsed(state, value) {
        if(!state.collapsed_training_course_materials.includes(value)) {
            state.collapsed_training_course_materials.push(value);
        }
    },
    setTrainingCourseMaterialsAsCollapsed(state, value) {
        state.collapsed_training_course_materials = value;
    },
    setTrainingCourseMaterialAsHidden(state, value) {
        if(!state.hidden_training_course_materials.includes(value)){
            state.hidden_training_course_materials.push(value);
        }
    },
    clearRevealedTrainingCourseMaterials(state) {
        state.revealed_training_course_materials = [];
    },
    clearCollapsedTrainingCourseMaterials(state) {
        state.collapsed_training_course_materials = [];
    },
    clearHiddenTrainingCourseMaterials(state) {
        state.hidden_training_course_materials = [];
    }
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};