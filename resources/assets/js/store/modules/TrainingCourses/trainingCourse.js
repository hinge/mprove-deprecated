import trainingCourseApi from '@/services/trainingCourseApi';

const state = {
    training_courses: [],
    fetching_training_courses: false
};

const getters = {
    trainingCourses: () => state.training_courses,
    totalNumberOfTrainingCourses: () => state.training_courses.length,
    fetchingTrainingCourses: () => state.fetching_training_courses,
};

const actions = {
    getAllTrainingCourses({commit}) {
        commit('setFetchingTrainingCourses', true);
        return new Promise((resolve, reject) => {
            trainingCourseApi.all()
                .then((data) => {
                    commit('setFetchingTrainingCourses', false);
                    commit('setTrainingCourses', data);
                    return resolve(data);
                })
                .catch(error => {
                    commit('setFetchingTrainingCourses', false);
                    return reject(error);
                });
        });
    }
};

const mutations = {
    setTrainingCourses(state, data) {
        state.training_courses = data;
    },
    setFetchingTrainingCourses(state, value) {
        state.fetching_training_courses = value;
    }
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
