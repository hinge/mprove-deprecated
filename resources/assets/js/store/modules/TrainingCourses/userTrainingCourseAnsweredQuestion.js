import userTrainingCourseAnsweredQuestionApi from '@/services/userTrainingCourseAnsweredQuestionApi';

const state = {
    training_course_answered_questions: [],
    fetching_training_course_questions: false
};

const getters = {
    getAnsweredQuestions: () => state.training_course_answered_questions,
    fetchingTrainingCourseQuestions: () => state.fetching_training_course_questions,
};

const actions = {
    getAllQuestions({commit}, trainingCourseId) {
        return new Promise((resolve, reject) => {
            commit('setFetchingTrainingCourseQuestions', true);
            userTrainingCourseAnsweredQuestionApi.all(trainingCourseId)
                .then((data) => {
                    commit('setFetchingTrainingCourseQuestions', false);
                    commit('setTrainingCourseQuestions', data);
                    return resolve(data);
                })
                .catch(error => {
                    commit('setFetchingTrainingCourseQuestions', false);
                    return reject(error);
                });
        });
    },
};

const mutations = {
    setTrainingCourseQuestions(state, data) {
        state.training_course_answered_questions = data;
    },
    setFetchingTrainingCourseQuestions(state, value) {
        state.fetching_training_course_questions = value;
    },

};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};