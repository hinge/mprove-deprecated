import userTrainingCourseQuestionApi from '@/services/userTrainingCourseQuestionApi';

const state = {
    fetching_training_course_questions: false,
    fetching_training_course_question: false,
    answering_training_course_question: false,
    training_course_questions: [],
    training_course_questions_with_answers: [],
    shown_training_course_question: {},
    selected_training_course_question_answer: {},
    skip_to_summary_and_submit_answer: false
};

const getters = {
    fetchingTrainingCourseQuestions: () => state.fetching_training_course_questions,
    fetchingTrainingCourseQuestion: () => state.fetching_training_course_question,
    trainingCourseQuestions: () => state.training_course_questions,
    trainingCourseQuestionsWithAnswers: () => state.training_course_questions_with_answers,
    shownTrainingCourseQuestion: () => state.shown_training_course_question,
    answeringTrainingCourseQuestion: () => state.answering_training_course_question,
    selectedTrainingCourseQuestionAnswer: () => state.selected_training_course_question_answer,
    getSkipToSummaryAndSubmitAnswer: () => state.skip_to_summary_and_submit_answer,
};

const actions = {
    getAllQuestions({commit}, trainingCourseId) {
        return new Promise((resolve, reject) => {
            commit('setFetchingTrainingCourseQuestions', true);
            userTrainingCourseQuestionApi.all(trainingCourseId)
                .then((data) => {
                    commit('setFetchingTrainingCourseQuestions', false);
                    commit('setTrainingCourseQuestions', data);
                    return resolve(data);
                })
                .catch(error => {
                    commit('setFetchingTrainingCourseQuestions', false);
                    return reject(error);
                });
        });
    },
    getQuestion({commit}, ids) {
        return new Promise((resolve, reject) => {
            commit('setFetchingTrainingCourseQuestion', true);
            userTrainingCourseQuestionApi.get(ids[0], ids[1])
                .then((data) => {
                    commit('setFetchingTrainingCourseQuestion', false);
                    commit('updateTrainingCourseQuestionsWithAnswers', data);

                    let answer = data.user_answers[0] ?? {};
                    commit('selectQuestionAnswer', answer);

                    return resolve(data);
                })
                .catch(error => {
                    commit('setFetchingTrainingCourseQuestion', false);
                    return reject(error);
                });
        });
    },
    showTrainingCourseQuestion({commit}, question) {
        commit('markTrainingCourseQuestionAsShown', question);
    },

    answerTrainingCourseQuestion({commit}, {trainingCourseId, questionId, formData}) {

        return new Promise((resolve, reject) => {
            commit('setAnsweringTrainingCourseQuestion', true);
            userTrainingCourseQuestionApi.answer(trainingCourseId, questionId, formData)
                .then((data) => {
                    commit('setAnsweringTrainingCourseQuestion', false);
                    return resolve(data);
                })
                .catch(error => {
                    commit('setAnsweringTrainingCourseQuestion', false);
                    return reject(error);
                });
        });
    },
    updateTrainingCourseQuestionAnswer({commit}, {trainingCourseId, questionId, answerId}) {
        return new Promise((resolve, reject) => {
            commit('setAnsweringTrainingCourseQuestion', true);
            userTrainingCourseQuestionApi.updateAnswer(trainingCourseId, questionId, answerId)
                .then((data) => {
                    commit('setAnsweringTrainingCourseQuestion', false);
                    return resolve(data);
                })
                .catch(error => {
                    commit('setAnsweringTrainingCourseQuestion', false);
                    return reject(error);
                });
        });
    },
    setSelectedQuestionAnswer({commit}, answer) {
        commit('selectQuestionAnswer', answer);
    },

    setSkipToSummaryAndSubmitAnswer({commit}, value) {
        commit('skipToSummaryAndSubmitAnswer', value);
    },
};

const mutations = {
    setFetchingTrainingCourseQuestions(state, value) {
        state.fetching_training_course_questions = value;
    },
    setFetchingTrainingCourseQuestion(state, value) {
        state.fetching_training_course_question = value;
    },
    setTrainingCourseQuestions(state, value) {
        state.training_course_questions = value;
    },
    updateTrainingCourseQuestionsWithAnswers(state, value) {
        state.training_course_questions_with_answers.push(value);
    },
    markTrainingCourseQuestionAsShown(state, question) {
        state.shown_training_course_question = question;
    },
    setAnsweringTrainingCourseQuestion(state, question) {
        state.answering_training_course_question = question;
    },
    selectQuestionAnswer(state, answer) {
        state.selected_training_course_question_answer = answer;
    },
    skipToSummaryAndSubmitAnswer(state, value) {
        state.skip_to_summary_and_submit_answer = value;
    }
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};