import userTrainingCourseApi from '@/services/userTrainingCourseApi';

const state = {
    user_training_courses: [],
    fetching_user_training_courses: false,
    available_user_training_courses: [],
    fetching_available_user_training_courses: false,
    user_training_course: [],
    fetching_user_training_course: false,
    user_training_course_materials: [],
    fetching_user_training_course_materials: false,
    user_training_course_is_startable: false,
    user_training_course_is_complete: false,
    user_training_course_is_submitting: false,
    user_training_course_pass_status: null,
    user_training_course_is_restarting: false,
    downloading_user_training_course_certificate: false,
    generating_user_training_course_certificate: false
};

const getters = {
    userTrainingCourses: () => state.user_training_courses,
    fetchingUserTrainingCourses: () => state.fetching_user_training_courses,
    retryingUserTrainingCourse: () => state.user_training_course_is_restarting,
    downloadingCertificate: () => state.downloading_user_training_course_certificate,
    generatingCertificate: () => state.generating_user_training_course_certificate,
    availableUserTrainingCourses: () => state.available_user_training_courses,
    fetchingAvailableUserTrainingCourses: () => state.fetching_available_user_training_courses,
    userTrainingCourse: () => state.user_training_course,
    fetchingUserTrainingCourse: () => state.fetching_user_training_course,
    userTrainingCourseMaterials: () => state.user_training_course_materials,
    fetchingUserTrainingCourseMaterials: () => state.fetching_user_training_course_materials,
    completeTrainingCourses: (state, getters) => {
        return getters.userTrainingCourses.filter( course => course.hasOwnProperty('results'))
    },
    incompleteTrainingCourses: (state, getters) => {
        return getters.userTrainingCourses.filter( course => !course.hasOwnProperty('results'))
    },
    courseCanBeStarted: () => state.user_training_course_is_startable,
    courseIsComplete: () => state.user_training_course_is_complete,
    coursePassStatus: () => state.user_training_course_pass_status,
    userTrainingCourseIsSubmitting: () => state.user_training_course_is_submitting
};

const actions = {
    getUserTrainingCourses({commit}) {
        commit('setFetchingUserTrainingCourses', true);
        return new Promise((resolve, reject) => {
            userTrainingCourseApi.all()
                .then((data) => {
                    commit('setFetchingUserTrainingCourses', false);
                    commit('setUserTrainingCourses', data);
                    return resolve(data);
                })
                .catch(error => {
                    commit('setFetchingUserTrainingCourses', false);
                    return reject(error);
                });
        });
    },
    getAvailableUserTrainingCourses({commit}) {
        commit('setFetchingAvailableUserTrainingCourses', true);
        return new Promise((resolve, reject) => {
            userTrainingCourseApi.available()
                .then((data) => {
                    commit('setFetchingAvailableUserTrainingCourses', false);
                    commit('setAvailableUserTrainingCourses', data);
                    return resolve(data);
                })
                .catch(error => {
                    commit('setFetchingAvailableUserTrainingCourses', false);
                    return reject(error);
                });
        });
    },
    getUserTrainingCourse({commit}, id) {
        commit('setFetchingUserTrainingCourse', true);
        return new Promise((resolve, reject) => {
            userTrainingCourseApi.get(id)
                .then((data) => {
                    commit('setFetchingUserTrainingCourse', false);
                    commit('setUserTrainingCourse', data);
                    return resolve(data);
                })
                .catch(error => {
                    commit('setFetchingUserTrainingCourse', false);
                    return reject(error);
                });
        });
    },
    getUserTrainingCourseMaterials({commit}, id) {
        commit('setFetchingUserTrainingCourseMaterials', true);
        return new Promise((resolve, reject) => {
            userTrainingCourseApi.materials(id)
                .then((data) => {
                    commit('setFetchingUserTrainingCourseMaterials', false);
                    commit('setUserTrainingCourseMaterials', data);
                    return resolve(data);
                })
                .catch(error => {
                    commit('setFetchingUserTrainingCourseMaterials', false);
                    return reject(error);
                });
        });
    },
    startTrainingCourse({commit}, id) {
        return new Promise((resolve, reject) => {
            userTrainingCourseApi.start(id)
                .then((data) => {
                    return resolve(data);
                })
                .catch(error => {
                    return reject(error);
                });
        });
    },
    retryTrainingCourse({commit}, id) {
        commit('setTrainingCourseRetryFlag', true);
        return new Promise((resolve, reject) => {
            userTrainingCourseApi.retry(id)
                .then((data) => {
                    commit('setTrainingCourseRetryFlag', false);
                    return resolve(data);
                })
                .catch(error => {
                    commit('setTrainingCourseRetryFlag', false);
                    return reject(error);
                });
        });
    },

    generateCertificate({commit}, id) {
        commit('setDownloadingCertificateFlag', true);

        return new Promise((resolve, reject) => {
            userTrainingCourseApi.generateCertificate(id)
                .then((data) => {
                    commit('setDownloadingCertificateFlag', false);
                    return resolve(data);
                })
                .catch(error => {
                    commit('setDownloadingCertificateFlag', false);
                    return reject(error);
                });
        });
    },
   downloadCertificate({commit}, id) {
        commit('setDownloadingCertificateFlag', true);

        return new Promise((resolve, reject) => {
            userTrainingCourseApi.downloadCertificate(id)
                .then((data) => {
                    commit('setDownloadingCertificateFlag', false);
                    return resolve(data);
                })
                .catch(error => {
                    commit('setDownloadingCertificateFlag', false);
                    return reject(error);
                });
        });
    },
    makeTheTrainingCourseStartable({commit}) {
        commit('markTheTrainingCourseAsStartable');
    },
    completeTheCourse({commit}) {
        commit('markTheTrainingCourseAsComplete');
    },
    submitTheCourse({commit}, id) {
        return new Promise((resolve, reject) => {
            commit('setTrainingCourseSubmitFlag', true);
            userTrainingCourseApi.submit(id)
                .then((data) => {
                    commit('setTrainingCourseSubmitFlag', false);
                    return resolve(data);
                })
                .catch(error => {
                    commit('setTrainingCourseSubmitFlag', false);
                    return reject(error);
                });
        });
    },
    provideCoursePassStatus({commit}, value) {
        commit('setCoursePassStatus', value);
    }
};

const mutations = {
    setUserTrainingCourses(state, data) {
        state.user_training_courses = data;
    },
    setFetchingUserTrainingCourses(state, value) {
        state.fetching_user_training_courses = value;
    },
    setAvailableUserTrainingCourses(state, data) {
        state.available_user_training_courses = data;
    },
    setFetchingAvailableUserTrainingCourses(state, value) {
        state.fetching_available_user_training_courses = value;
    },
    setUserTrainingCourse(state, data) {
        state.user_training_course = data;
    },
    setFetchingUserTrainingCourse(state, value) {
        state.fetching_user_training_course = value;
    },
    setUserTrainingCourseMaterials(state, data) {
        state.user_training_course_materials = data;
    },
    setFetchingUserTrainingCourseMaterials(state, value) {
        state.fetching_user_training_course_materials = value;
    },
    markTheTrainingCourseAsStartable(state) {
        state.user_training_course_is_startable = true;
    },
    markTheTrainingCourseAsComplete(state) {
        state.user_training_course_is_complete = true;
    },
    setCoursePassStatus(state, value) {
        state.user_training_course_pass_status = value;
    },
    setTrainingCourseSubmitFlag(state, value) {
        state.user_training_course_is_submitting = value;
    },
    setTrainingCourseRetryFlag(state, value) {
        state.user_training_course_is_restarting = value;
    },
    setGeneratingCertificateFlag(state, value) {
        state.generating_user_training_course_certificate = value;
    },
    setDownloadingCertificateFlag(state, value) {
        state.downloading_user_training_course_certificate = value;
    }
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};