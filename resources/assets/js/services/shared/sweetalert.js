import Swal from 'sweetalert2/dist/sweetalert2.js';
export default {
    baseOptions: function (title, body, icon = 'info') {
        return {
            icon: icon,
            title: title,
            text: body,
            background: '#f2f2f2',
            confirmButtonText: 'Continue',
            cancelButtonText: 'Cancel',
            reverseButtons: true,
            customClass: {
                confirmButton: 'btn bg-teal text-white',
                cancelButton: 'btn bg-red text-white'
            },
            buttonsStyling: false,
            heightAuto: false
        };
    },
    warning(title, body, additionalOptions) {
        let options = this.baseOptions(title, body, 'warning');
        options.showCancelButton = true;
        options = _.merge(options, additionalOptions);
        return new Promise((resolve, reject) => {
            Swal.fire(options).then(
                ({value: value}) => {
                    if (value) {
                        return resolve(value);
                    }
                }
            )
        });
    },
    success(title, body, additionalOptions) {
        let options = this.baseOptions(title, body, 'success');
        options.showConfirmButton = false;
        options.timer = 2000;
        options = _.merge(options, additionalOptions);
        return new Promise(resolve=>{
            Swal.fire(options).then(()=>{
                return resolve();
            });
        });
    },
    error(title, body, additionalOptions) {
        let options = this.baseOptions(title, body, 'error');
        options.showConfirmButton = true;
        options = _.merge(options, additionalOptions);
        return new Promise((resolve, reject) => {
            Swal.fire(options).then(
                ({value: value}) => {
                    if (value) {
                        return resolve(value);
                    }
                }
            )
        });
    },
}