import api from '@/services/api';

export default new class userTrainingCourseMaterialApi {
    setAsSeen(trainingCourseId, trainingCourseMaterialId) {
        return api.post('user-training-courses/' + trainingCourseId + '/materials/' + trainingCourseMaterialId + '/seen');
    }
}
