import api from '@/services/api';

export default new class userTrainingCourseAnsweredQuestionApi {
    all(id) {
        return api.get(`user-training-courses/${id}/answered-questions`);
    }
}
