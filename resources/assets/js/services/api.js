export default new class Api {
    /**
     * Helper method to make a 'GET' ajax call.
     *
     * @param {String} url
     * @param {Object} parameters
     * @returns {Promise<any>}
     */
    get(url, parameters = {}) {
        return this._call('get', url, parameters);
    }

    /**
     * Helper method to make a 'POST' ajax call.
     *
     * @param {String} url
     * @param {Object} data
     * @returns {Promise<any>}
     */
    post(url, data = {}) {
       return this._call('post', url, data);
    }

    /**
     * Helper method to make a 'PUT' ajax call.
     *
     * @param {String} url
     * @param {Object} data
     * @returns {Promise<any>}
     */
    put(url, data = {}) {
        return this._call('put', url, data);
    }

    /**
     * Helper method to make a 'PATCH' ajax call.
     *
     * @param {String} url
     * @param {Object} data
     * @returns {Promise<any>}
     */
    patch(url, data = {}) {
        return this._call('patch', url, data);
    }

    /**
     * Helper method to make a 'DELETE' ajax call.
     *
     * @param {String} url
     * @param {Object} data
     * @returns {Promise<any>}
     */
    delete(url, data = {}) {
        return this._call('delete', url, data);
    }

    /**
     * Make an api call.
     *
     * @param {string} method
     * @param {string} url
     * @param {Object} data
     * @returns {Promise<any>}
     */
    _call(method, url, data) {
        return new Promise((resolve, reject) => {
            axios[method](process.env.MIX_API_URL + url, data)
                .then(
                    ({data}) => {
                        return resolve(data);
                    },
                    ({response}) => {
                        return reject(response)
                    }
                );
        });
    }
};
