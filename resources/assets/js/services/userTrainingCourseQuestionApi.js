import api from '@/services/api';

export default new class userTrainingCourseQuestionApi {
    all(id) {
        return api.get(`user-training-courses/${id}/questions`);
    }
    get(trainingCourseId, questionId) {
        return api.get(`user-training-courses/${trainingCourseId}/questions/${questionId}`);
    }
    answer(trainingCourseId, questionId, formData) {
        return api.post(`user-training-courses/${trainingCourseId}/questions/${questionId}/answer`, formData);
    }
    updateAnswer(trainingCourseId, questionId, answerId) {
        return api.put(`user-training-courses/${trainingCourseId}/questions/${questionId}/answer/${answerId}`);
    }
}
