import api from '@/services/api';

export default new class userTrainingCourseApi {
    all() {
        return api.get('user-training-courses');
    }
    available() {
        return api.get('user-training-courses/available');
    }
    get(id) {
        return api.get('user-training-courses/' + id);
    }
    materials(id) {
        return api.get('user-training-courses/' + id + '/materials');
    }
    start(id) {
        return api.post('user-training-courses/' + id + '/start');
    }
    retry(id) {
        return api.delete('user-training-courses/' + id + '/retry');
    }
    submit(id) {
        return api.put('user-training-courses/' + id + '/complete');
    }
    generateCertificate(trainingCourseId) {
        return api.put(`user-training-courses/${trainingCourseId}/certificate-generate`);
    }
    downloadCertificate(trainingCourseId) {
        return api.get(`user-training-courses/${trainingCourseId}/certificate`);
    }
}
