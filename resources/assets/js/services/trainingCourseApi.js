import api from '@/services/api';

export default new class trainingCourseApi {

    all() {
        return api.get('training-courses');
    }
}
