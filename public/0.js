(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[0],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/components/TrainingCourses/Components/CourseCompleted/DescriptionFailed.vue?vue&type=script&lang=js&":
/*!******************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/assets/js/components/TrainingCourses/Components/CourseCompleted/DescriptionFailed.vue?vue&type=script&lang=js& ***!
  \******************************************************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  props: {
    courseName: {
      required: true,
      type: String
    },
    score: {
      required: true,
      type: Number
    },
    passRate: {
      required: true,
      type: Number
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/components/TrainingCourses/Components/CourseCompleted/DescriptionFailed.vue?vue&type=template&id=6a241649&":
/*!**********************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/assets/js/components/TrainingCourses/Components/CourseCompleted/DescriptionFailed.vue?vue&type=template&id=6a241649& ***!
  \**********************************************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("p", { staticClass: "text-xl mb-10" }, [
    _vm._v(
      "Unfortunately you did not pass the " +
        _vm._s(_vm.courseName) +
        " online training course this time.\n        "
    ),
    _c("span", { staticClass: "font-avertaBold text-blue" }, [
      _vm._v(" You scored " + _vm._s(_vm.score) + "%")
    ]),
    _vm._v("\n        but require a score of at least\n        "),
    _c("span", { staticClass: "font-avertaBold text-green" }, [
      _vm._v(" " + _vm._s(_vm.passRate) + "% to pass")
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/assets/js/components/TrainingCourses/Components/CourseCompleted/DescriptionFailed.vue":
/*!*********************************************************************************************************!*\
  !*** ./resources/assets/js/components/TrainingCourses/Components/CourseCompleted/DescriptionFailed.vue ***!
  \*********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _DescriptionFailed_vue_vue_type_template_id_6a241649___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./DescriptionFailed.vue?vue&type=template&id=6a241649& */ "./resources/assets/js/components/TrainingCourses/Components/CourseCompleted/DescriptionFailed.vue?vue&type=template&id=6a241649&");
/* harmony import */ var _DescriptionFailed_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./DescriptionFailed.vue?vue&type=script&lang=js& */ "./resources/assets/js/components/TrainingCourses/Components/CourseCompleted/DescriptionFailed.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _DescriptionFailed_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _DescriptionFailed_vue_vue_type_template_id_6a241649___WEBPACK_IMPORTED_MODULE_0__["render"],
  _DescriptionFailed_vue_vue_type_template_id_6a241649___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/assets/js/components/TrainingCourses/Components/CourseCompleted/DescriptionFailed.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/assets/js/components/TrainingCourses/Components/CourseCompleted/DescriptionFailed.vue?vue&type=script&lang=js&":
/*!**********************************************************************************************************************************!*\
  !*** ./resources/assets/js/components/TrainingCourses/Components/CourseCompleted/DescriptionFailed.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_DescriptionFailed_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./DescriptionFailed.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/components/TrainingCourses/Components/CourseCompleted/DescriptionFailed.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_DescriptionFailed_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/assets/js/components/TrainingCourses/Components/CourseCompleted/DescriptionFailed.vue?vue&type=template&id=6a241649&":
/*!****************************************************************************************************************************************!*\
  !*** ./resources/assets/js/components/TrainingCourses/Components/CourseCompleted/DescriptionFailed.vue?vue&type=template&id=6a241649& ***!
  \****************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_DescriptionFailed_vue_vue_type_template_id_6a241649___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./DescriptionFailed.vue?vue&type=template&id=6a241649& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/components/TrainingCourses/Components/CourseCompleted/DescriptionFailed.vue?vue&type=template&id=6a241649&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_DescriptionFailed_vue_vue_type_template_id_6a241649___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_DescriptionFailed_vue_vue_type_template_id_6a241649___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);