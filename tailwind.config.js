// tailwind.config.js
module.exports = {
    purge: {
        enabled: true,
        content: [
            './resources/**/*.html',
            './resources/**/*.vue',
            './resources/**/*.blade.php',
        ],
    },
    important: true,
    theme: {
        colors: {
            blue: '#1C487D',
            sky: '#72BBD3',
            teal: '#20c997',
            green: '#2CC09C',
            red: '#E21919',
            light_gray: '#e5e5e5',
            lighter_gray: '#fbfbfb'
        },
        fontFamily: {
            avertaBold: ['Averta Bold', 'sans-serif'],
            avertaSemiBold: ['Averta Semibold', 'sans-serif'],
            avertaExtraBold: ['Averta Extra Bold', 'sans-serif'],
            avertaBlack: ['Averta Black', 'sans-serif']
        }
    },
    variants: {},
    plugins: [
        require('@tailwindcss/typography'),
    ],
}
