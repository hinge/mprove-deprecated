<?php

Route::get('/', 'PageController@show');
Route::get('/about-mprove', 'PageController@showAbout');

Route::group(['prefix' => 'mprove-academy'], function(){
	Route::get('/', 'PageController@showAcademy');
	Route::get('/scenario-bank', 'PageController@showScenarioBank');
	Route::get('/moulage', 'PageController@showMoulage');
	Route::get('/{course}', 'PageController@showAcademyCourseDates');
	Route::get('/{course}/{venue}/{date}', 'PageController@showAcademyCourseDetails');
});

Route::group(['prefix' => 'quality-mprovement'], function(){
	Route::get('/', 'PageController@showMprovement');
	Route::get('toolkits', 'PageController@showToolkits');
});

Route::get('/education', 'PageController@showEducation');

//training courses

Route::get('/online-training-courses', 'TrainingCourses\TrainingCourseController@index');

Route::group([
    'prefix' => '/online-training-courses/'
], function () {
    Route::group([
        'middleware' => 'onlineTraining',
    ], function () {
        Route::get('{trainingCourseId}', 'TrainingCourses\TrainingCourseController@show');
    });

    Route::group([
        'middleware' => 'onlineTrainingQuestions',
    ], function () {
        Route::get('{trainingCourseId}/questions', 'TrainingCourses\TrainingCourseController@questions');

    });

    Route::group([
        'middleware' => 'onlineTrainingQuestionsSummary',
    ], function () {
        Route::get('{trainingCourseId}/questions/summary', 'TrainingCourses\TrainingCourseController@questionsSummary');
    });

    Route::group([
        'middleware' => 'onlineTrainingQuestionAnswers',
    ], function () {
        Route::get('{trainingCourseId}/answers', 'TrainingCourses\TrainingCourseController@questionAnswers');
    });

    Route::get('{trainingCourseId}/certificate-generate/{userId}', 'TrainingCourses\TrainingCourseController@certificateGenerate');
});







