<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register the API routes for your application as
| the routes are automatically authenticated using the API guard and
| loaded automatically by this application's RouteServiceProvider.
|
*/

Route::group([
    'middleware' => 'auth:api',
    'prefix' => '/v1/'
], function () {
    Route::apiResource('user-training-courses/available', 'API\V1\TrainingCourses\AvailableTrainingCourseController')->only(['index']);
    Route::apiResource('user-training-courses', 'API\V1\TrainingCourses\AuthenticatedTrainingCourseController')->only(['index', 'show']);
    Route::group(['prefix' => 'user-training-courses/{id}'], function() {
        Route::apiResource('materials', 'API\V1\TrainingCourseMaterials\TrainingCourseMaterialController')->only(['index']);
        Route::apiResource('materials', 'API\V1\TrainingCourseMaterials\TrainingCourseMaterialController')->only(['index']);
        Route::put('certificate-generate', 'API\V1\TrainingCourses\TrainingCourseCertificateController@store')->name('trainingCourseCertificateGenerate');
        Route::get('certificate', 'API\V1\TrainingCourses\TrainingCourseCertificateController@show')->name('trainingCourseCertificate');
        Route::get('status', 'API\V1\TrainingCourses\TrainingCourseStatusController@show');
        Route::get('answered-questions', 'API\V1\TrainingCourses\AnsweredQuestionController@index');
        Route::apiResource('questions', 'API\V1\Questions\QuestionController');
        Route::apiResource('questions/{questionId}/answer', 'API\V1\TrainingCourses\QuestionAnswerController');
        Route::apiResource('materials/{trainingCourseMaterialId}/seen', 'API\V1\TrainingCourseMaterials\TrainingCourseMaterialSeenController')->only(['store']);
        Route::apiResource('start', 'API\V1\TrainingCourses\UserTrainingCourseController')->only(['store']);
        Route::put('complete', 'API\V1\TrainingCourses\UserTrainingCourseController@update');
        Route::delete('retry', 'API\V1\TrainingCourses\UserTrainingCourseController@destroy');
    });
});

Route::group([
	'middleware' => ['guest'],
	'prefix' => '/v1/'
], function() {
	Route::post('register', 'API\V1\RegisterController@store');
	Route::resource('courses', 'API\V1\CourseController');
	Route::resource('venues', 'API\V1\VenueController');
	Route::resource('resources/categories', 'API\V1\ResourceCategoryController');
	Route::resource('resources', 'API\V1\ResourceController');
	Route::get('courses/{startDate}/{endDate}/feedback', 'API\V1\CourseFeedbackController@index');
    Route::apiResource('training-courses', 'API\V1\TrainingCourses\UnauthenticatedTrainingCourseController')->only(['index']);
});

