<?php

namespace Tests\Unit;

use App\Question;
use App\QuestionAnswer;
use App\TrainingCourse;
use App\TrainingCourseQuestion;
use App\User;
use Illuminate\Database\Eloquent\Collection;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class QuestionTest extends TestCase
{
    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();
    }

    /** @test */
    public function test_questions_are_ordered_by_the_order_number()
    {
        $this->login();

        $trainingCourse = factory(TrainingCourse::class)->create();

        $questions = [];
        array_push($questions, ['instance' => $this->makeQuestion('Q4')->first(), 'order' => 4]);
        array_push($questions, ['instance' => $this->makeQuestion('Q1')->first(), 'order' => 1]);
        array_push($questions, ['instance' => $this->makeQuestion('Q3')->first(), 'order' => 3]);
        array_push($questions, ['instance' => $this->makeQuestion('Q2')->first(), 'order' => 2]);

        collect($questions)->map(fn(array $question, $key) => $trainingCourse->questions()->attach($question['instance'], ['order'=> $question['order']]));

        $response = $this->json('GET',"/api/v1/user-training-courses/{$trainingCourse->id}/questions");
        $response->assertStatus(200);
        $response = $response->decodeResponseJson();

        $this->assertCount(4, $response);
        $this->assertTrue($response[0]['title'] === 'Q1');
        $this->assertTrue($response[1]['title'] === 'Q2');
        $this->assertTrue($response[2]['title'] === 'Q3');
        $this->assertTrue($response[3]['title'] === 'Q4');
    }

    /** @test */
    public function test_it_has_the_correct_data_structure()
    {
        $this->login();

        $trainingCourse = factory(TrainingCourse::class)->create();

        $questions = factory(Question::class, 15)->create();

        foreach($questions as $key => $question) {
            $trainingCourse->questions()->attach($question, ['order'=> $key]);
        }

        $response = $this->get('/api/v1/user-training-courses/1/questions');

        $response->assertStatus(200)
            ->assertJsonStructure([
                '*' => [
                    'id',
                    'title',
                ]
            ]);
    }

    /**
     * @return \App\User
     */
    protected function login(): User
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        return $user;
    }

    /**
     * @param string $title
     * @param int|null $count
     * @return Collection
     */
    protected function makeQuestion(string $title, ?int $count = 1): Collection
    {
       return factory(Question::class, $count)->create([
           'title' => $title
       ]);
    }
}
