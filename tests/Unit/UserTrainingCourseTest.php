<?php

namespace Tests\Unit;

use App\Question;
use App\QuestionAnswer;
use App\TrainingCourse;
use App\TrainingCourseMaterial;
use App\User;
use App\UserTrainingCourse;
use App\UserTrainingCourseMaterialSeen;
use App\UserTrainingCourseQuestionAnswer;
use Illuminate\Database\Eloquent\Collection;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;

class UserTrainingCourseTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;

    public function setUp(): void
    {
        parent::setUp();
    }

    /**
     *
     * @return void
     */
    public function test_store_returns_200_and_user_can_pass_a_course(): void
    {
        $user = $this->login();

        $trainingCourse = $this->makeTrainingCourse(true);

        $questions = $this->makeQuestions($trainingCourse);

        $correctUserAnswers = $this->makePassedUserQuestionAnswers($user, $questions);

        $response = $this->post('/api/v1/user-training-courses/' . $trainingCourse->id . '/complete');

        $response->assertStatus(200);

        $this->assertDatabaseHas('user_training_courses', [
            'user_id' => $user->id,
            'training_course_id' => $trainingCourse->id,
            'passed' => 1
        ]);
    }


    /**
     *
     * @return void
     */
    public function test_store_returns_200_and_user_can_fail_a_course(): void
    {
        $user = $this->login();

        $trainingCourse = $this->makeTrainingCourse(true);

        $questions = $this->makeQuestions($trainingCourse);

        $incorrectUserAnswers = $this->makeFailedUserQuestionAnswers($user, $questions);

        $response = $this->post('/api/v1/user-training-courses/' . $trainingCourse->id . '/complete');

        $response->assertStatus(200);

        $this->assertDatabaseHas('user_training_courses', [
            'user_id' => $user->id,
            'training_course_id' => $trainingCourse->id,
            'passed' => 0
        ]);
    }

    /**
     *
     * @return void
     */
    public function test_store_returns_302_when_course_is_submitted_more_than_once(): void
    {
        $user = $this->login();

        $trainingCourse = $this->makeTrainingCourse(true);

        $questions = $this->makeQuestions($trainingCourse);

        $correctUserAnswers = $this->makePassedUserQuestionAnswers($user, $questions);

        $response = $this->post('/api/v1/user-training-courses/' . $trainingCourse->id . '/complete');

        $response2 = $this->post('/api/v1/user-training-courses/' . $trainingCourse->id . '/complete');

        $response2->assertStatus(302);
    }

    /**
     *
     * @return void
     */
    public function test_store_returns_403_when_an_unpublished_course_is_submitted(): void
    {
        $user = $this->login();

        $trainingCourse = $this->makeTrainingCourse(false);

        $questions = $this->makeQuestions($trainingCourse);

        $response = $this->post('/api/v1/user-training-courses/' . $trainingCourse->id . '/complete');

        $response->assertStatus(403);
    }

    /**
     *
     * @return void
     */
    public function test_delete_returns_404_when_not_attempted_course_is_retried(): void
    {
        $user = $this->login();

        $trainingCourse = $this->makeTrainingCourse(true);

        $response = $this->delete('/api/v1/user-training-courses/' . $trainingCourse->id . '/retry');

        $response->assertStatus(404);
    }

    /**
     *
     * @return void
     */
    public function test_delete_does_not_allow_retrying_unpublished_courses(): void
    {
        $user = $this->login();

        $trainingCourse = $this->makeTrainingCourse(false);

        $response = $this->delete('/api/v1/user-training-courses/' . $trainingCourse->id . '/retry');

        $response->assertStatus(403);
    }

    /**
     *
     * @return void
     */
    public function test_delete_removes_material_seens_question_answers_and_nullifies_user_training_course(): void
    {
        $user = $this->login();

        $trainingCourse = $this->makeTrainingCourse(true);

        $userTrainingCourse = factory(UserTrainingCourse::class)->create([
            'training_course_id' => $trainingCourse->id,
            'user_id' => $user->id,
            'passed' => 1,
            'completed' => 1,
            'score' => 90
        ]);

        $questions = $this->makeQuestions($trainingCourse);

        $correctUserAnswers = $this->makePassedUserQuestionAnswers($user, $questions);

        $trainingCourseMaterial = factory(TrainingCourseMaterial::class)->create([
            'training_course_id' => $trainingCourse->id
        ]);

        $trainingCourseMaterialSeen = factory(UserTrainingCourseMaterialSeen::class)->create([
            'user_id' => $user->id,
            'training_course_material_id' => $trainingCourseMaterial->id,
        ]);

        $response = $this->delete('/api/v1/user-training-courses/' . $trainingCourse->id . '/retry');

        $response->assertStatus(200);

        $this->assertDatabaseHas('user_training_courses', [
            'training_course_id' => $trainingCourse->id,
            'user_id' => $user->id,
            'passed' => 0,
            'completed' => 0,
            'score' => 0
        ]);

        $this->assertDatabaseMissing('user_training_course_material_seens', [
            'user_id' => $user->id,
            'training_course_material_id' => $trainingCourseMaterial->id,
        ]);

        $this->assertDatabaseMissing('user_training_course_question_answers', [
            'user_id' => $user->id
        ]);
    }

    /**
     * @param TrainingCourse $trainingCourse
     * @return Collection
     */
    protected function makeQuestions(TrainingCourse $trainingCourse): Collection
    {
        $questions = $this->makeQuestionsWithAnswers(4);

        foreach ($questions as $question) {
            $trainingCourse->questions()->attach($question, ['order' => rand(1, 4)]);
        }

        return $questions;
    }

    /**
     * @param int|null $count
     * @return Collection
     */
    protected function makeQuestionsWithAnswers(?int $count = 1): Collection
    {
        $questions = factory(Question::class, $count)->create();

        foreach ($questions as $question) {
            $question->answers()->saveMany($this->makeAnswers());
        }

        return $questions;
    }

    /**
     * @return Collection
     */
    protected function makeAnswers(): Collection
    {
        return factory(QuestionAnswer::class)->createMany([
            ['correct' => false],
            ['correct' => false],
            ['correct' => false],
            ['correct' => true]
        ]);
    }

    /**
     * @param User $user
     * @param Collection $questions
     * @return Collection
     */
    protected function makePassedUserQuestionAnswers(User $user, Collection $questions): Collection
    {
        //create answers where at least 75% are correct since only answers D are correct in this test
        return factory(UserTrainingCourseQuestionAnswer::class)->createMany([
            [
                'user_id' => $user->id,
                'training_course_question_id' => $questions[0]->id,
                'question_answer_id' => $questions[0]->answers[0]->id
            ],
            [
                'user_id' => $user->id,
                'training_course_question_id' => $questions[1]->id,
                'question_answer_id' => $questions[1]->answers[3]->id
            ],
            [
                'user_id' => $user->id,
                'training_course_question_id' => $questions[2]->id,
                'question_answer_id' => $questions[2]->answers[3]->id
            ],
            [
                'user_id' => $user->id,
                'training_course_question_id' => $questions[3]->id,
                'question_answer_id' => $questions[3]->answers[3]->id
            ],
        ]);
    }

    /**
     * @param User $user
     * @param Collection $questions
     * @return Collection
     */
    protected function makeFailedUserQuestionAnswers(User $user, Collection $questions): Collection
    {
        //create answers where less than 75% are correct since only answers D are correct in this test
        return factory(UserTrainingCourseQuestionAnswer::class)->createMany([
            [
                'user_id' => $user->id,
                'training_course_question_id' => $questions[0]->id,
                'question_answer_id' => $questions[0]->answers[0]->id
            ],
            [
                'user_id' => $user->id,
                'training_course_question_id' => $questions[1]->id,
                'question_answer_id' => $questions[1]->answers[0]->id
            ],
            [
                'user_id' => $user->id,
                'training_course_question_id' => $questions[2]->id,
                'question_answer_id' => $questions[2]->answers[3]->id
            ],
            [
                'user_id' => $user->id,
                'training_course_question_id' => $questions[3]->id,
                'question_answer_id' => $questions[3]->answers[3]->id
            ],
        ]);
    }

    /**
     * @param bool $published
     * @return TrainingCourse
     */
    protected function makeTrainingCourse(bool $published): TrainingCourse
    {
        return factory(TrainingCourse::class)->create([
            'pass_rate' => 75,
            'published' => $published,
            'draft' => false,
        ]);
    }

    /**
     * @return \App\User
     */
    protected function login(): User
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);
        return $user;
    }
}