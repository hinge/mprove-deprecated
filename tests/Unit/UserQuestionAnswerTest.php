<?php

namespace Tests\Unit;

use App\Question;
use App\QuestionAnswer;
use App\TrainingCourse;
use App\TrainingCourseQuestion;
use App\User;
use App\UserTrainingCourseQuestionAnswer;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserQuestionAnswerTest extends TestCase
{

    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();
    }

    /**
     * Test that submitting an answer returns a 200
     *
     * @return void
     */
    public function test_store_returns_200(): void
    {
        $this->login();

        $trainingCourse = factory(TrainingCourse::class)->create();

        $question = factory(Question::class)->create();

        $answer = factory(QuestionAnswer::class)->create([
            'question_id' => $question->id,
        ]);

        $trainingCourseQuestion = factory(TrainingCourseQuestion::class)->create([
            'training_course_id' => $trainingCourse->id,
            'question_id' => $question->id,
            'order' => rand(1,4),
        ]);

        $response = $this->post('/api/v1/user-training-courses/1/questions/1/answer', [
            'answer_id' => $answer->id,
        ]);

        $response->assertStatus(200);
    }

    /**
     * Test that updating an answer returns 200
     *
     * @return void
     */
    public function test_update_returns_200_and_it_indeed_updates_the_record(): void
    {
        $user = $this->login();

        $trainingCourse = factory(TrainingCourse::class)->create();

        $question = factory(Question::class)->create();

        $trainingCourseQuestion = factory(TrainingCourseQuestion::class)->create([
            'training_course_id' => $trainingCourse->id,
            'question_id' => $question->id,
            'order' => rand(1,4),
        ]);

        $firstAnswer = factory(QuestionAnswer::class)->create([
            'question_id' => $question->id,
        ]);

        $secondAnswer = factory(QuestionAnswer::class)->create([
            'question_id' => $question->id,
        ]);

        $userTrainingCourseQuestionAnswer = factory(UserTrainingCourseQuestionAnswer::class)->create([
            'user_id' => $user->id,
            'training_course_question_id' => $trainingCourseQuestion->id,
            'question_answer_id' => $firstAnswer->id
        ]);

        $response = $this->put('/api/v1/user-training-courses/1/questions/1/answer/' . $secondAnswer->id);

        $response->assertStatus(200);

        $this->assertDatabaseMissing('user_training_course_question_answers', [
            'user_id' => $user->id,
            'training_course_question_id' => $trainingCourseQuestion->id,
            'question_answer_id' => $firstAnswer->id
        ]);

        $this->assertDatabaseHas('user_training_course_question_answers', [
            'user_id' => $user->id,
            'training_course_question_id' => $trainingCourseQuestion->id,
            'question_answer_id' => $secondAnswer->id
        ]);
    }

    /**
     * @return \App\User
     */
    protected function login(): User
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        return $user;
    }
}
