<?php

namespace Tests\Unit;

use App\Question;
use App\QuestionAnswer;
use App\TrainingCourse;
use App\TrainingCourseMaterial;
use App\User;
use App\UserTrainingCourseMaterialSeen;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserTrainingCourseMaterialSeenTest extends TestCase
{
    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();
    }

    /**
     *
     * @return void
     */

    public function test_store_returns_200_and_it_indeed_creates_the_record(): void
    {
        $user = $this->login();

        $firstTrainingCourse = factory(TrainingCourse::class)->create();

        $secondTrainingCourse = factory(TrainingCourse::class)->create();

        $firstTrainingCourseMaterial = factory(TrainingCourseMaterial::class)->create([
                'training_course_id' => $firstTrainingCourse->id,
            ]
        );

        $secondTrainingCourseMaterial = factory(TrainingCourseMaterial::class)->create([
                'training_course_id' => $secondTrainingCourse->id,
            ]
        );

        //check that the endpoint returns 200 if matching training course and material are submitted to be "seen"
        $response = $this->post('/api/v1/user-training-courses/' . $firstTrainingCourse->id . '/materials/' . $firstTrainingCourseMaterial->id . '/seen');

        $response->assertStatus(200);

        //check that the record has been created
        $this->assertDatabaseHas('user_training_course_material_seens', [
            'user_id' => $user->id,
            'training_course_material_id' => $firstTrainingCourseMaterial->id
        ]);

        //check that the endpoint returns 404 if non-matching training course and material are submitted to be "seen"
        $response = $this->post('/api/v1/user-training-courses/' . $firstTrainingCourse->id . '/materials/' . $secondTrainingCourseMaterial->id . '/seen');

        $response->assertStatus(404);

        //check that the record has not been created
        $this->assertDatabaseMissing('user_training_course_material_seens', [
            'user_id' => $user->id,
            'training_course_material_id' => $secondTrainingCourseMaterial->id,
        ]);
    }

    /**
     * @return \App\User
     */
    protected function login(): User
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);
        return $user;
    }
}