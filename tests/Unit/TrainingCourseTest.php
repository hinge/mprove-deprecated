<?php

namespace Tests\Unit;

use App\TrainingCourse;
use App\TrainingCourseQuestion;
use App\TrainingCourseMaterial;
use App\UserTrainingCourse;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;

class TrainingCourseTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;

    public function setUp(): void
    {
        parent::setUp();
    }

    /**
     * Test for the returned data structure of /api/v1/user-training-courses endpoint
     *
     * @test
     */
    public function it_returns_training_courses()
    {
        $this->login();

        factory(TrainingCourse::class, 20)->create();

        $response = $this->json('GET','/api/v1/user-training-courses');

        $response->assertStatus(200)
            ->assertJsonStructure([
                '*' => [
                    'id',
                    'name',
                    'description',
                    'draft'
                ]
            ]);
    }

    /**
     * @test
     */
    public function it_returns_training_courses_in_alphabetical_order()
    {
        $this->login();

        $this->makeTrainingCourse('D course');
        $this->makeTrainingCourse('A course');
        $this->makeTrainingCourse('C course');
        $this->makeTrainingCourse('B course');

        $response = $this->json('GET', '/api/v1/user-training-courses');
        $response->assertStatus(200);
        $response = $response->decodeResponseJson();

        $this->assertTrue($response[0]['name'] === 'A course');
        $this->assertTrue($response[1]['name'] === 'B course');
        $this->assertTrue($response[2]['name'] === 'C course');
    }

    /**
     * @test
     */
    public function it_returns_published_training_courses_only()
    {
        $this->login();

        $this->makeTrainingCourse('A course');
        $this->makeTrainingCourse('B course');
        $this->makeTrainingCourse('C course', false);

        $response = $this->json('GET','/api/v1/user-training-courses');
        $response->assertStatus(200);
        $response = $response->decodeResponseJson();

        $this->assertCount(2, $response);
    }

    /**
     * @test
     */
    public function it_has_correct_number_of_related_questions()
    {
        $this->login();

        $trainingCourse = $this->makeTrainingCourse('A course')->first();
        $questionsCount = 10;

        factory(TrainingCourseQuestion::class, $questionsCount)->create([
            'training_course_id' => $trainingCourse->id
        ]);

        $response = $this->json('GET','/api/v1/user-training-courses');
        $response->assertStatus(200);
        $response = $response->decodeResponseJson();

        $totalCount = (int) $response[0]['count']['questions'];
        $this->assertTrue($questionsCount === $totalCount);
    }

    /**
     * @test
     */
    public function it_has_correct_number_of_related_materials()
    {
        $this->login();

        $trainingCourse = $this->makeTrainingCourse('A course')->first();
        $materialsCount = 10;

        factory(TrainingCourseMaterial::class, $materialsCount)->create([
            'training_course_id' => $trainingCourse->id
        ]);

        $response = $this->json('GET','/api/v1/user-training-courses');
        $response->assertStatus(200);
        $response = $response->decodeResponseJson();

        $totalCount = (int) $response[0]['count']['materials'];
        $this->assertTrue($materialsCount === $totalCount);
    }

    /**
     * @return void
     */
    public function test_course_results_only_show_when_course_is_completed(): void
    {
        $user = $this->login();

        $firstTrainingCourse = $this->makeTrainingCourse('A course')->first();
        factory(UserTrainingCourse::class, 10)->create([
            'user_id' => $user->id,
            'training_course_id' => $firstTrainingCourse->id,
            'completed' => true
        ]);

        $secondTrainingCourse = $this->makeTrainingCourse('B course')->first();
        factory(UserTrainingCourse::class, 10)->create([
            'user_id' => $user->id,
            'training_course_id' => $secondTrainingCourse->id,
            'completed' => false
        ]);

        $response = $this->json('GET','/api/v1/user-training-courses');
        $response->assertStatus(200);
        $response = $response->decodeResponseJson();

        $this->assertCount(2, $response);
        $this->assertArrayHasKey('results', $response[0]);
        $this->assertArrayNotHasKey('results', $response[1]);
    }

    /**
     * @test
     */
    public function test_it_returns_list_of_seen_material_ids_for_training_courses()
    {
        $user = $this->login();

        $trainingCourse = $this->makeTrainingCourse('A course')->first();

        $materialsCount = 10;
        $trainingCourseMaterials = factory(TrainingCourseMaterial::class, $materialsCount)->create([
            'training_course_id' => $trainingCourse->id,
        ]);

        $trainingCourseMaterials->map(fn(TrainingCourseMaterial $material) => $user->trainingCourseMaterials()->attach($material->id, ['completed' => true]));

        $response = $this->json('GET','/api/v1/user-training-courses');
        $response->assertStatus(200);
        $response = $response->decodeResponseJson();

        $diff = array_diff(
            $trainingCourseMaterials->pluck('id')->toArray(),
            $response[0]['seen']['materials']
        );

        $this->assertTrue(!count($diff));
    }

    /**
     * @return void
     */
    public function test_it_returns_list_of_seen_material_ids_for_individual_training_course_endpoint(): void
    {
        //create one user, later used for actingAs()
        $user = factory(User::class)->create();

        //create one training course
        $trainingCourse = factory(TrainingCourse::class)->create([
            'published' => true
        ]);

        $this->actingAs($user);

        //create a bunch of training course materials for the above training course
        $trainingCourseMaterials = factory(TrainingCourseMaterial::class, 10)->create([
            'training_course_id' => $trainingCourse->id,
        ]);

        $completedMaterialIds = [];

        foreach($trainingCourseMaterials as $trainingCourseMaterial) {
            $completed = rand(0,1);

            $user->trainingCourseMaterials()->attach($trainingCourseMaterial, ['completed' => $completed]);

            if($completed == 1) {
                $completedMaterialIds[] = $trainingCourseMaterial->id;
            }
        }

        $response = $this->get('/api/v1/user-training-courses/1')->getContent();

        $response = $this->get('/api/v1/user-training-courses/1');
        $response->assertStatus(200);
        $response = $response->getContent();

        //check if there are any id differences in the list of completed materials and the list returned by the endpoint
        $this->assertTrue(collect(json_decode($response)->seen->materials)->diff(collect($completedMaterialIds))->count() == 0, 'List of een material ids returned does not match the ones in the database.');
    }

    /**
     * @return void
     */
    public function test_individual_course_results_only_show_when_course_is_completed(): void
    {
        $user = factory(\App\User::class)->create();
        $this->actingAs($user);

        $userTrainingCourse = factory(UserTrainingCourse::class)->create();
        $userTrainingCourseIds = $userTrainingCourse->pluck('completed', 'training_course_id');

        $response = $this->get('/api/v1/user-training-courses/1');
        $response->assertStatus(200);
        $response = $response->getContent();

        $correctResults = true;
        $trainingCourse = json_decode($response);

        //if the results are not present and the course is completed
        if(!isset($trainingCourse->results) && $userTrainingCourseIds[$trainingCourse->id]) {
            $correctResults = false;
        }
        //if the results are present and the course isn't completed
        if(isset($trainingCourse->results) && !$userTrainingCourseIds[$trainingCourse->id]) {
            $correctResults = false;
        }

        $this->assertTrue($correctResults, 'The presence or lack of results does not correspond with the course completed status.');
    }

    /**
     * @return \App\User
     */
    protected function login(): User
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        return $user;
    }

    /**
     * @param string|null $name
     * @param bool|null $published
     * @param int|null $count
     * @return Collection
     */
    protected function makeTrainingCourse(?string $name = null, ?bool $published = true, ?int $count = 1): Collection
    {
        return factory(TrainingCourse::class, $count)->create([
            'name' => $name ?: $this->faker->text(20),
            'published' => $published,
            'draft' => !$published,
        ]);
    }
}
