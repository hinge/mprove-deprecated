<?php

namespace Tests\Unit;

use App\TrainingCourse;
use App\TrainingCourseMaterial;
use App\TrainingCourseReference;
use App\User;
use Illuminate\Database\Eloquent\Collection;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class TrainingCourseMaterialTest extends TestCase
{

    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();
    }

    /** @test */
    public function test_it_has_the_correct_data_structure()
    {
        $this->login();

        $trainingCourse = factory(TrainingCourse::class)->create();
        factory(TrainingCourseMaterial::class, 20)->create([
            'training_course_id' => $trainingCourse->id
        ]);

        $response = $this->json('GET',"api/v1/user-training-courses/{$trainingCourse->id}/materials");

        $response->assertStatus(200)
            ->assertJsonStructure([
                '*' => [
                    'id',
                    'title',
                    'summary',
                    'training_course_id',
                    'order',
                    'references'
                ]
            ]);
    }

    /** @test */
    public function test_it_returns_references_with_materials()
    {
        $this->login();

        $trainingCourse = factory(TrainingCourse::class)->create();
        $trainingCourseMaterial = factory(TrainingCourseMaterial::class)->create([
            'training_course_id' => $trainingCourse->id
        ]);

        $trainingCourseReferences = factory(TrainingCourseReference::class, 5)->create();

        $trainingCourseMaterial->references()->attach($trainingCourseReferences);

        $response = $this->json('GET',"api/v1/user-training-courses/{$trainingCourse->id}/materials");

        $response->assertStatus(200)
            ->assertJsonCount(5, '*.references.*');
    }

    /** @test */
    public function test_training_course_materials_are_ordered_by_order_number()
    {
        $this->login();

        $trainingCourse = factory(TrainingCourse::class)->create(['published' => true]);

        //remove materials attached to the training course in factory
        $trainingCourse->materials()->delete();

        $this->makeTrainingCourseMaterial($trainingCourse->id, 2);
        $this->makeTrainingCourseMaterial($trainingCourse->id, 1);
        $this->makeTrainingCourseMaterial($trainingCourse->id, 3);

        $response = $this->json('GET',"api/v1/user-training-courses/{$trainingCourse->id}/materials");

        $response->assertStatus(200);
        $response = $response->decodeResponseJson();

        $this->assertTrue((int) $response[0]['order'] === 1);
        $this->assertTrue((int) $response[1]['order'] === 2);
        $this->assertTrue((int) $response[2]['order'] === 3);
    }

    /**
     * @return \App\User
     */
    protected function login(): User
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        return $user;
    }

    /**
     * @param int $trainingCourseId
     * @param int $order
     * @param int $count
     * @return Collection
     */
    protected function makeTrainingCourseMaterial(int $trainingCourseId, int $order, $count = 1): Collection
    {
        return factory(TrainingCourseMaterial::class, $count)->create([
            'training_course_id' => $trainingCourseId,
            'order' => $order
        ]);
    }

}
