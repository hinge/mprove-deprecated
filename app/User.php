<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Laravel\Spark\User as SparkUser;
 
class User extends SparkUser
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'authy_id',
        'country_code',
        'phone',
        'card_brand',
        'card_last_four',
        'card_country',
        'billing_address',
        'billing_address_line_2',
        'billing_city',
        'billing_zip',
        'billing_country',
        'extra_billing_information',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'trial_ends_at' => 'datetime',
        'uses_two_factor_auth' => 'boolean',
    ];

    /**
     * Whether the user has an admin role.
     *
     * @return bool
     */
    public function isAdmin(): bool
    {
        return $this->role_id == 1;
    }

    /**
     * Whether the user is allowed to impersonate other users.
     *
     * @return bool
     */
    public function canImpersonate(): bool
    {
        return $this->isAdmin();
    }

    /**
     *
     * @return BelongsToMany
     */
    public function trainingCourseMaterials(): BelongsToMany
    {
        return $this->belongsToMany(
            TrainingCourseMaterial::class,
            'training_course_material_users',
            'user_id',
            'training_course_material_id'
        )->withPivot('completed');
    }
}
