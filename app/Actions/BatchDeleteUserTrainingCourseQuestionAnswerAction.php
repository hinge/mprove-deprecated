<?php

namespace App\Actions;

use App\UserTrainingCourse;
use App\UserTrainingCourseQuestionAnswer;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;
use Throwable;

class BatchDeleteUserTrainingCourseQuestionAnswerAction
{
    /**
     * @param Collection $userTrainingCourseQuestionAnswers
     */

    public function execute(Collection $userTrainingCourseQuestionAnswers): void
    {
        DB::transaction(fn() => $this->delete($userTrainingCourseQuestionAnswers));
    }

    /**
     * @param Collection $userTrainingCourseQuestionAnswers
     * @return void
     * @throws Throwable
     */

    protected function delete(Collection $userTrainingCourseQuestionAnswers): void
    {
        abort_if(!$userTrainingCourseQuestionAnswers->count(), 404, 'No UserTrainingCourseQuestionAnswers records found');
        $userTrainingCourseQuestionAnswers = UserTrainingCourseQuestionAnswer::whereIn('id', $userTrainingCourseQuestionAnswers->pluck('id'));
        throw_if(!$userTrainingCourseQuestionAnswers->delete(), 'User training course question answers could not be deleted.');
    }

}