<?php

namespace App\Actions;

use App\UserTrainingCourseQuestionAnswer;
use Illuminate\Support\Facades\DB;
use Throwable;

class UpdateUserQuestionAnswerAction
{
    /**
     * @param UserTrainingCourseQuestionAnswer $userTrainingCourseQuestionAnswer
     * @param int $answerId|null
     * @return UserTrainingCourseQuestionAnswer
     */
    public function execute(UserTrainingCourseQuestionAnswer $userTrainingCourseQuestionAnswer, ?int $answerId): UserTrainingCourseQuestionAnswer
    {
        return DB::transaction(fn() => $this->update($userTrainingCourseQuestionAnswer, $answerId));
    }

    /**
     * @param UserTrainingCourseQuestionAnswer $userTrainingCourseQuestionAnswer
     * @param int $answerId|null
     * @throws Throwable
     * @return UserTrainingCourseQuestionAnswer
     */
    protected function update(UserTrainingCourseQuestionAnswer $userTrainingCourseQuestionAnswer, ?int $answerId): UserTrainingCourseQuestionAnswer
    {

        $userTrainingCourseQuestionAnswer->question_answer_id = $answerId;
        $userTrainingCourseQuestionAnswer->save();

        throw_if(!$userTrainingCourseQuestionAnswer, 'Answer has not been created');

        return $userTrainingCourseQuestionAnswer;
    }
}