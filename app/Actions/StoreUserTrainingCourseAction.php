<?php

namespace App\Actions;

use App\UserTrainingCourse;
use Illuminate\Support\Facades\DB;
use Throwable;

class StoreUserTrainingCourseAction
{
    /**
     * @param int $userId
     * @param int $trainingCourseId
     * @param array $completionInformation
     * @return UserTrainingCourse
     */

    public function execute(int $userId, int $trainingCourseId, array $completionInformation): UserTrainingCourse
    {
        return DB::transaction(fn() => $this->store($userId, $trainingCourseId, $completionInformation['score'], $completionInformation['passed'], $completionInformation['completed']));
    }

    /**
     * @param int $userId
     * @param int $trainingCourseId
     * @param int $score
     * @param bool $passed
     * @param bool $completed
     * @return UserTrainingCourse
     * @throws Throwable
     */

    protected function store(int $userId, int $trainingCourseId, int $score, bool $passed, bool $completed): UserTrainingCourse
    {
        $userTrainingCourse = new UserTrainingCourse();
        $userTrainingCourse->user_id = $userId;
        $userTrainingCourse->training_course_id = $trainingCourseId;
        $userTrainingCourse->score = $score;
        $userTrainingCourse->passed = $passed;
        $userTrainingCourse->completed = $completed;
        $userTrainingCourse->save();

        throw_if(!$userTrainingCourse, 'UserTrainingCourse record has not been created');

        return $userTrainingCourse;
    }

}