<?php

namespace App\Actions;

use App\UserTrainingCourseMaterialSeen;
use Illuminate\Support\Facades\DB;
use Throwable;

class StoreUserTrainingCourseMaterialSeenAction
{
    /**
     * @param int $userId
     * @param int $trainingCourseMaterialId
     * @return UserTrainingCourseMaterialSeen
     */

    public function execute(int $userId, int $trainingCourseMaterialId): UserTrainingCourseMaterialSeen
    {
        return DB::transaction(fn() => $this->store($userId, $trainingCourseMaterialId));
    }

    /**
     * @param int $userId
     * @param int $trainingCourseMaterialId
     * @return UserTrainingCourseMaterialSeen
     * @throws Throwable
     */

    protected function store(int $userId, int $trainingCourseMaterialId): UserTrainingCourseMaterialSeen
    {
        $userTrainingCourseMaterialSeen = new UserTrainingCourseMaterialSeen();
        $userTrainingCourseMaterialSeen->user_id = $userId;
        $userTrainingCourseMaterialSeen->training_course_material_id = $trainingCourseMaterialId;
        $userTrainingCourseMaterialSeen->save();

        throw_if(!$userTrainingCourseMaterialSeen, 'UserTrainingCourseMaterial record has not been created');

        return $userTrainingCourseMaterialSeen;
    }

}