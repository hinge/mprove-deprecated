<?php

namespace App\Actions;

use App\UserTrainingCourse;
use Illuminate\Support\Facades\DB;
use Throwable;

class UpdateUserTrainingCourseAction
{
    /**
     * @param int $userId
     * @param UserTrainingCourse $userTrainingCourse
     * @param int $trainingCourseId
     * @param int $score
     * @param bool $passed
     * @param bool $completed
     * @return UserTrainingCourse
     */

    public function execute(int $userId, UserTrainingCourse $userTrainingCourse, int $score, bool $passed, bool $completed): UserTrainingCourse
    {
        return DB::transaction(fn() => $this->update($userId, $userTrainingCourse, $score, $passed, $completed));
    }

    /**
     * @param int $userId
     * @param UserTrainingCourse $userTrainingCourse
     * @param int $score
     * @param bool $passed
     * @param bool $completed
     * @return UserTrainingCourse
     * @throws Throwable
     */

    protected function update(int $userId, UserTrainingCourse $userTrainingCourse, int $score, bool $passed, bool $completed): UserTrainingCourse
    {
        $userTrainingCourse->user_id = $userId;
        $userTrainingCourse->score = $score;
        $userTrainingCourse->passed = $passed;
        $userTrainingCourse->completed = $completed;
        $userTrainingCourse->save();

        throw_if(!$userTrainingCourse, 'UserTrainingCourse record has not been updated');

        return $userTrainingCourse;
    }

}