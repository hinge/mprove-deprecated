<?php

namespace App\Actions;

use App\UserTrainingCourseQuestionAnswer;
use Illuminate\Support\Facades\DB;
use Throwable;

class StoreUserQuestionAnswerAction
{
    /**
     * @param int $userId
     * @param int $questionId
     * @param int $answerId|null
     * @return UserTrainingCourseQuestionAnswer
     */
    public function execute(int $userId, int $questionId, ?int $answerId): UserTrainingCourseQuestionAnswer
    {
        return DB::transaction(fn() => $this->store($userId, $questionId, $answerId));
    }

    /**
     * @param int $userId
     * @param int $questionId
     * @param int $answerId|null
     * @throws Throwable
     * @return UserTrainingCourseQuestionAnswer
     */
    protected function store(int $userId, int $questionId, ?int $answerId): UserTrainingCourseQuestionAnswer
    {
        $userTrainingCourseQuestionAnswer = new UserTrainingCourseQuestionAnswer();
        $userTrainingCourseQuestionAnswer->user_id = $userId;
        $userTrainingCourseQuestionAnswer->training_course_question_id = $questionId;
        $userTrainingCourseQuestionAnswer->question_answer_id = $answerId;
        $userTrainingCourseQuestionAnswer->save();

        throw_if(!$userTrainingCourseQuestionAnswer, 'Answer has not been created');

        return $userTrainingCourseQuestionAnswer;
    }
}