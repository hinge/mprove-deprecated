<?php

namespace App\Actions;

use App\UserTrainingCourse;
use App\UserTrainingCourseMaterialSeen;
use App\UserTrainingCourseQuestionAnswer;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;
use Throwable;

class BatchDeleteUserTrainingCourseMaterialSeenAction
{
    /**
     * @param Collection $userTrainingCourseMaterialSeens
     */

    public function execute(Collection $userTrainingCourseMaterialSeens): void
    {
        DB::transaction(fn() => $this->delete($userTrainingCourseMaterialSeens));
    }

    /**
     * @param Collection $userTrainingCourseMaterialSeens
     * @return void
     */
    protected function delete(Collection $userTrainingCourseMaterialSeens): void
    {
        abort_if(!$userTrainingCourseMaterialSeens->count(), 404, 'No UserTrainingCourseMaterialSeen records found');
        $userTrainingCourseMaterialSeens = UserTrainingCourseMaterialSeen::whereIn('id', $userTrainingCourseMaterialSeens->pluck('id'));
        throw_if(!$userTrainingCourseMaterialSeens->delete(), 'User training course material seens could not be deleted.');
    }

}