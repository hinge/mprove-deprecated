<?php

namespace App\Actions;

use App\TrainingCourse;
use App\User;
use App\UserTrainingCourse;
use Illuminate\Support\Facades\Storage;
use Spatie\Browsershot\Browsershot;
use Spatie\Browsershot\Exceptions\CouldNotTakeBrowsershot;
use Illuminate\Support\Facades\File;

class GenerateUserTrainingCourseCertificateAction
{
    /**
     * @param TrainingCourse $trainingCourse
     * @param UserTrainingCourse $userTrainingCourse
     * @param User $user
     * @return bool
     * @throws CouldNotTakeBrowsershot
     */
    public function execute(TrainingCourse $trainingCourse, UserTrainingCourse $userTrainingCourse, User $user): bool
    {
        if($trainingCourse && $user && $userTrainingCourse) {
            if($this->canGenerateCertificate($userTrainingCourse)) {
                $fileName = $user->name . ' - ' . $trainingCourse->name . ' Certification';

                if(!$trainingCourse || !$user) {
                    return false;
                }

                $url = env('APP_URL') . "/online-training-courses/$trainingCourse->id/certificate-generate/$user->id";

                //create an image by taking a screenshot of the browser window
                Browsershot::url($url)->windowSize(1570, 1200)->landscape()->save($fileName . '.jpg');

                $imageContent = file_get_contents($fileName . '.jpg');

                $image = base64_encode($imageContent);

                $html = "<img src='data:image/png;base64, $image'/>";

                $path = "users/$user->id/certificates/";

                $date = new \DateTime();

                $S3fileName = $fileName . ' - ' . $date->getTimestamp() . '.pdf';

                //embed the image into a pdf
                Browsershot::html($html)->windowSize(1754, 1240)->landscape()->save($S3fileName);

                if(Storage::disk('s3')->put($path . $S3fileName, file_get_contents($S3fileName))) {
                    $userTrainingCourse->update(['certificate_url' => $path . $S3fileName]);
                }

                File::delete($S3fileName, $fileName . '.jpg');

                if(Storage::exists($path . $S3fileName)) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * @param $userTrainingCourse
     * @return bool
     */

    protected function canGenerateCertificate($userTrainingCourse): bool
    {
        return $userTrainingCourse && $userTrainingCourse->completed && $userTrainingCourse->passed && !$userTrainingCourse->certificate_url;
    }
}