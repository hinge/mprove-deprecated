<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

class ResourceCategory extends Model
{
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
	protected $hidden = ['resource_type_id'];

    /**
     *
     * @return BelongsToMany
     */
    public function resources(): BelongsToMany
    {
        return $this->belongsToMany(Resource::class);
    }

    /**
     *
     * @return BelongsTo
     */
    public function type(): BelongsTo
    {
    	return $this->belongsTo('App\ResourceType', 'resource_type_id', 'id');
    }
}
