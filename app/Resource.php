<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Resource extends Model
{
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['type_id'];

    /**
     *
     * @return BelongsTo
     */
    public function type(): BelongsTo
    {
      return $this->belongsTo('App\ResourceType', 'type_id', 'id');
    }

    /**
     * The file link path.
     *
     * @return string
     */
    public function getFilelinkAttribute(): string
    {
    	return env('S3_PATH') . 'resources/' . $this->link;
    }

    /**
     * Comma separated list of categories.
     *
     * @return string
     */
    public function getCategoriesListAttribute(): string
    {
      return join(', ', $this->categories->pluck('name')->toArray());
    }

    /**
     *
     * @return BelongsToMany
     */
    public function categories(): BelongsToMany
    {
        return $this->belongsToMany('App\ResourceCategory');
    }
}