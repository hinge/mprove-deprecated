<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class CourseFeedback extends Model
{
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */

	protected $hidden = ['id', 'course_id'];

    /**
     * Defines table name.
     *
     * @var string
     */
    protected $table = 'course_feedbacks';

    /**
     *
     * @return BelongsTo
     */
    public function course(): BelongsTo
    {
        return $this->belongsTo(Course::class);
    }

    /**
     *
     * @return BelongsTo
     */
    public function courseDate(): BelongsTo
    {
        return $this->belongsTo(CourseDate::class);
    }

    /**
     *
     * @return BelongsTo
     */
    public function venue(): BelongsTo
    {
        return $this->belongsTo(Venue::class);
    }
}
