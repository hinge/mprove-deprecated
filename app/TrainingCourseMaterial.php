<?php

namespace App;

use App\Services\Resolvers\Contracts\TextIframeResolverInterface;
use App\Services\Resolvers\Contracts\TextReferenceResolverInterface;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;

/**
 * Class TrainingCourseMaterial
 * @package App
 * @property int $id
 * @property int $training_course_id
 * @property string $title
 * @property string $summary
 * @property string $content
 * @property string $referenced_content
 * @property int $order
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property Course $course
 * @property TrainingCourseReference[]|Collection $references
 * @property User[]|Collection $users
 */
class TrainingCourseMaterial extends Model
{
    protected $fillable = [
        'id',
        'title',
        'content',
        'summary',
        'order',
        'updated_at',
        'created_at'
    ];

    /**
     *
     * @return BelongsTo
     */
    public function course(): BelongsTo
    {
        return $this->belongsTo(TrainingCourse::class, 'training_course_id');
    }

    /**
     *
     * @return BelongsToMany
     */
    public function references(): BelongsToMany
    {
        return $this->belongsToMany(
            TrainingCourseReference::class,
            'training_course_material_references',
            'training_course_material_id',
            'training_course_reference_id'
        )->withPivot('unique_reference_number');
    }

    /**
     *
     * @return BelongsToMany
     */
    public function users(): BelongsToMany
    {
        return $this->belongsToMany(
            User::class,
            'user_training_course_material_seens',
            'training_course_material_id',
            'user_id'
        )->withPivot('completed');
    }

    /**
     * @param $query
     * @return void
     */
    public function scopeComplete($query): void
    {
        $query->join('user_training_course_material_seens', 'user_training_course_material_seens.training_course_material_id', '=', 'training_course_materials.id')
            ->where('user_training_course_material_seens.user_id', '=', Auth::user()->id);
    }

    /**
     * @return string
     */
    public function getReferencedContentAttribute(): string
    {
        return resolve(TextReferenceResolverInterface::class)->resolve(
            resolve(TextIframeResolverInterface::class)->resolve($this->content),
            $this->references);
    }
}
