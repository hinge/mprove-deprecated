<?php

namespace App;

use \Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;


/**
 * Class CourseDate
 * @package App
 * @property string start_date
 * @property string end_date
 */
class CourseDate extends Model
{

    /**
     *
     * @var array
     */
	protected $hidden = ['course_id', 'venue_id'];

    /**
     *
     * @var array
     */
    protected $appends = ['slug', 'has_ended'];

    /**
     *
     * @var array
     */
    protected $dates = [
        'start_date',
        'end_date'
    ];

    /**
     *
     * @return string
     */
	public function getPriceAttribute($value): string
    {
        return number_format($value/100, 2, '.', '');
    }

    /**
     *
     * @return string
     */
    public function getSlugAttribute(): string
    {
        return Carbon::parse($this->start_date)->format('d-m-Y');
    }

    /**
     *
     * @return bool
     */
    public function getHasEndedAttribute(): bool
    {
        return $this->end_date < Carbon::now();
    }

    /**
     *
     * @return BelongsTo
     */
    public function venue(): BelongsTo
    {
        return $this->belongsTo('App\Venue');
    }

    /**
     *
     * @return BelongsTo
     */
    public function course(): BelongsTo
    {
        return $this->belongsTo('App\Course');
    }

    /**
     *
     * @return HasMany
     */
    public function prices(): HasMany
    {
        return $this->hasMany(CoursePrices::class);
    }
}
