<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Resource;
use Illuminate\Database\Eloquent\Relations\HasMany;

class ResourceType extends Model
{
    public $timestamps = false;

    /**
     *
     * @return HasMany
     */
    public function resources(): HasMany
    {
      return $this->hasMany('App\Resource');
    }
}
