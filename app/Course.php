<?php

namespace App;

use \Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;

class Course extends Model
{

    /**
     *
     * @return HasManyThrough
     */
    public function dates(): HasManyThrough
    {
        return $this->hasManyThrough(
        	'App\CourseDate',
        	'App\Course',
        	'id',
        	'course_id'
        )->orderBy('start_date', 'asc');
    }

    /**
     *
     * @return HasManyThrough
     */
    public function upcoming_dates(): HasManyThrough
    {
        return $this->hasManyThrough(
        	'App\CourseDate',
        	'App\Course',
        	'id',
        	'course_id'
        )->where([
        	['course_dates.end_date', '>', Carbon::now()]
        ])
        ->orderBy('start_date', 'asc');
    }

    /**
     *
     * @return HasManyThrough
     */
    public function previous_dates(): HasManyThrough
    {
        return $this->hasManyThrough(
        	'App\CourseDate',
        	'App\Course',
        	'id',
        	'course_id'
        )->where([
        	['course_dates.end_date', '<', Carbon::now()]
        ])
        ->orderBy('start_date', 'asc');
    }

    /**
     *
     * @return HasMany
     */
    public function overview(): HasMany
    {
        return $this->hasMany('App\CourseOverview');
    }

    /**
     *
     * @return BelongsToMany
     */
    public function details(): BelongsToMany
    {
        return $this->belongsToMany('App\CourseDetail');
    }

    /**
     *
     * @return HasMany
     */
    public function feedbacks(): HasMany
    {
        return $this->hasMany(CourseFeedback::class);
    }

}
