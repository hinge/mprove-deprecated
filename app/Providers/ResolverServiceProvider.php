<?php
namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ResolverServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind(
            'App\Services\Resolvers\Contracts\TextReferenceResolverInterface',
            'App\Services\Resolvers\TextReferenceResolver'
        );

        $this->app->bind(
            'App\Services\Resolvers\Contracts\TextIframeResolverInterface',
            'App\Services\Resolvers\TextIframeResolver'
        );

        $this->app->bind(
            'App\Services\Resolvers\Contracts\UserTrainingCourseCertificateResolverInterface',
            'App\Services\Resolvers\UserTrainingCourseCertificateResolver'
        );
    }
}
