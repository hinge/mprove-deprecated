<?php
namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind(
            'App\Repositories\Contracts\TrainingCourseRepositoryInterface',
            'App\Repositories\TrainingCourseRepository'
        );

        $this->app->bind(
            'App\Repositories\Contracts\TrainingCourseMaterialRepositoryInterface',
            'App\Repositories\TrainingCourseMaterialRepository'
        );

        $this->app->bind(
            'App\Repositories\Contracts\QuestionRepositoryInterface',
            'App\Repositories\QuestionRepository'
        );

        $this->app->bind(
            'App\Repositories\Contracts\QuestionAnswerRepositoryInterface',
            'App\Repositories\QuestionAnswerRepository'
        );

        $this->app->bind(
            'App\Repositories\Contracts\TrainingCourseQuestionRepositoryInterface',
            'App\Repositories\TrainingCourseQuestionRepository'
        );
    }
}
