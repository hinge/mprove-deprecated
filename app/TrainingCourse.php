<?php

namespace App;

use App\Services\Resolvers\MaterialVideoCountResolver;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Facades\Auth;

class TrainingCourse extends Model
{

    protected $fillable = [
        'name',
        'description',
        'published',
        'draft',
        'pass_rate',
    ];

    /**
     *
     * @return HasMany
     */
    public function materials(): HasMany
    {
        return $this->hasMany(TrainingCourseMaterial::class);
    }

    /**
     *
     * @param $query
     * @return Builder
     *
     */
    public function scopeByOrderNumber($query): Builder
    {
        return $query->orderBy('order_number');
    }

    /**
     *
     * @return BelongsToMany
     */
    public function questions(): BelongsToMany
    {
        return $this->belongsToMany(
            Question::class,
            'training_course_questions',
            'training_course_id',
            'question_id'
        )->withPivot('order');
    }

    /**
     * @return HasMany
     */
    public function trainingCourseQuestions(): HasMany
    {
        return $this->hasMany(TrainingCourseQuestion::class);
    }

    /**
     * @return HasMany
     */
    public function completedMaterials(): HasMany
    {
        return $this->hasMany(TrainingCourseMaterial::class)->complete();
    }

    /**
     * @return BelongsToMany
     */
    public function answeredQuestions(): BelongsToMany
    {
        return $this->belongsToMany(
            Question::class,
            'training_course_questions',
            'training_course_id',
            'question_id'
        )->complete();
    }

    /**
     * @return HasMany
     */
    public function completeInformation(): HasMany
    {
        $userId = Auth::user()->id;

        return $this->hasMany(UserTrainingCourse::class)->where('user_id', '=', $userId);
    }

    public function getVideoCountAttribute()
    {
        return (new MaterialVideoCountResolver)->resolve($this->materials);
    }
}
