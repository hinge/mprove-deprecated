<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class OnlineTrainingCourseQuestionsSummary
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $trainingCourse = null;

        $trainingCourseId = $request->route('trainingCourseId');

        if($trainingCourseId && Auth::user()) {
            $trainingCourse = \App\TrainingCourse::where('id', $trainingCourseId)->with('completeInformation', 'completedMaterials', 'materials')->first();

            if($trainingCourse) {
                if( $trainingCourse->trainingCourseQuestions->count() === $trainingCourse->answeredQuestions->count() ) {
                    return $next($request);
                }
            }
        }

        return redirect('/online-training-courses');
    }
}
