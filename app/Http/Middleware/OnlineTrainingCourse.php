<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class OnlineTrainingCourse
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $trainingCourse = null;

        $trainingCourseId =$request->route('trainingCourseId');

        if($trainingCourseId && Auth::user()) {
            $trainingCourse = \App\TrainingCourse::where('id', $trainingCourseId)->with('completeInformation')->first();

            if($trainingCourse) {
                $completeInformation = $trainingCourse->completeInformation->first();
                if(!$completeInformation || !$completeInformation->completed || $completeInformation->passed) {
                    return $next($request);
                }
            }
        }

        return redirect('/online-training-courses');

    }
}
