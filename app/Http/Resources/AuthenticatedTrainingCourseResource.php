<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;

class AuthenticatedTrainingCourseResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $resource = [
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->description,
            'draft' => $this->draft,
            'published' => $this->published,
            'pass_rate' => $this->pass_rate,
            'video_count' => $this->video_count,
            'count' => [
                'questions' => $this->questions_count,
                'materials' => $this->materials_count
            ],
        ];

        return $this->withConditional($resource);
    }

    /**
     * Loads additional resource keys conditionally
     *
     * @param array $resource
     * @return array
     */
    protected function withConditional(array $resource): array
    {
        $resource = $this->seenMaterials($resource);
        $resource = $this->answeredQuestions($resource);
        $resource = $this->completeInformation($resource);

        return $resource;
    }

    /**
     * @param array $resource
     * @return array
     */
    protected function seenMaterials(array $resource): array
    {
        $materials = $this->whenLoaded('completedMaterials')->pluck('training_course_material_id')->unique();
        $resource['seen'] = [
            'materials' => $materials->count()
                ? $materials->toArray()
                : []
        ];

        return $resource;
    }

    /**
     * @param array $resource
     * @return array
     */
    protected function answeredQuestions(array $resource): array
    {

        $questions = $this->whenLoaded('answeredQuestions')->pluck('id')->unique();

        $resource['questions'] = [
            'answered' => $questions->count()
                ? $questions->toArray()
                : []
        ];

        return $resource;
    }

    /**
     * @param array $resource
     * @return array
     */
    protected function completeInformation(array $resource): array
    {
        $completeInformation = $this->whenLoaded('completeInformation')->first();

        if (!$completeInformation) {
            return $resource;
        }

        if (!$completeInformation->completed) {
            return $resource;
        }

        $resource['results'] = [
            'score' => $completeInformation->score,
            'passed' => $completeInformation->passed,
            'certificate' =>route('trainingCourseCertificate', ['id'=> $this->id])
        ];

        return $resource;
    }
}
