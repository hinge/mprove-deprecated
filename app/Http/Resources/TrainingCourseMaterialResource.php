<?php

namespace App\Http\Resources;

use App\TrainingCourseMaterial;
use Illuminate\Http\Resources\Json\JsonResource;


class TrainingCourseMaterialResource extends JsonResource
{
    /** @var TrainingCourseMaterial */
    public $resource;

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->resource->id,
            'title' => $this->resource->title,
            'summary' => $this->resource->summary,
            'content' => $this->resource->referenced_content,
            'training_course_id' => $this->resource->training_course_id,
            'order' => $this->resource->order,
            'references' => TrainingCourseReferenceResource::collection($this->whenLoaded('references')),
        ];
    }
}
