<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TrainingCourseReferenceResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        return [
            'id' => $this->id,
            'training_course_reference_type_id' => $this->training_course_reference_type_id,
            'training_course_reference_type' => $this->whenLoaded('type')->name,
            'title' => $this->title,
            'url' => $this->url,
            'file' => $this->file,
            'unique_reference_number' => $this->pivot->unique_reference_number,
        ];
    }
}
