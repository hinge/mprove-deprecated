<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;

class UnauthenticatedTrainingCourseResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $resource = [
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->description,
            'draft' => $this->draft,
            'published' => $this->published,
            'count' => [
                'questions' => $this->questions_count,
                'materials' => $this->materials_count
            ],
        ];

        return $resource;
    }

}
