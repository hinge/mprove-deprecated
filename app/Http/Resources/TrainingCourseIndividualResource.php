<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class TrainingCourseIndividualResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $seen = [];
        $materials = $this->whenLoaded('completedMaterials')->pluck('training_course_material_id');
        $completeInformation = $this->whenLoaded('completeInformation')->first();

        if($materials->count()) {
            $seen['materials'] = $materials;
        }

        $return = [
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->description,
            'draft' => $this->draft,
            'seen' => $seen,
            'video_count' => $this->video_count,
            'materials' => TrainingCourseMaterialResource::collection($this->whenLoaded('materials'))
        ];

        if($completeInformation && $completeInformation->completed) {
            $return['results'] = [
                'pass_rate' => $this->pass_rate,
                'score' => $completeInformation->score,
                'passed' => $completeInformation->passed,
                'certificate' =>route('trainingCourseCertificate', ['id'=> $this->id])
            ];
        }

        return $return;
    }
}
