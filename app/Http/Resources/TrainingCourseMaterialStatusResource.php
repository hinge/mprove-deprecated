<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class TrainingCourseMaterialStatusResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $trainingCourseMaterialIds = $this->whenLoaded('materials')->pluck('id');
        $trainingCourseCompletedMaterialIds = $this->whenLoaded('completedMaterials')->pluck('id');
        $rainingCourseMaterialsComplete = $trainingCourseMaterialIds->diff($trainingCourseCompletedMaterialIds)->count() == 0;

        return [
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->description,
            'draft' => $this->draft,
            'completedAllMaterials' => $rainingCourseMaterialsComplete,
            'completeMaterials' => TrainingCourseMaterialCompletedResource::collection($this->whenLoaded('completedMaterials'))
        ];
    }
}
