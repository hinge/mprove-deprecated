<?php namespace App\Http\Controllers\API\V1;

	use App\User;
	use App\Http\Controllers\Controller;
	use Illuminate\Support\Facades\Hash;
	use App\Http\Requests\StoreUserRequest;

	class RegisterController extends controller
    {

        /**
         * @param StoreUserRequest $request
         * @return User
         */
		public function store(StoreUserRequest $request): User
		{
		 	$user = new User();
			$user->name = $request->first_name . " " . $request->last_name;
			$user->email = $request->email;
			$user->password = Hash::make($request->password);
			$user->save();

			return $user;
		}

	}
