<?php

namespace App\Http\Controllers\API\V1\TrainingCourses;

use App\Http\Controllers\Controller;
use App\Http\Resources\QuestionResource;
use App\Repositories\Contracts\QuestionRepositoryInterface;
use App\Repositories\Contracts\TrainingCourseRepositoryInterface;
use App\Repositories\QuestionRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class AnsweredQuestionController extends Controller
{

    protected TrainingCourseRepositoryInterface $trainingCourseRepository;
    protected QuestionRepositoryInterface $questionRepository;

    /**
     * QuestionController constructor.
     *
     * @param TrainingCourseRepositoryInterface $trainingCourseRepository
     * @param QuestionRepositoryInterface $questionRepository
     */
    public function __construct(
        TrainingCourseRepositoryInterface $trainingCourseRepository,
        QuestionRepositoryInterface $questionRepository
    ) {
        $this->trainingCourseRepository = $trainingCourseRepository;
        $this->questionRepository = $questionRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param int $trainingCourseId
     * @return JsonResponse
     */
    public function index(int $trainingCourseId): JsonResponse
    {
        $trainingCourse = $this->trainingCourseRepository->find($trainingCourseId);
        abort_if(!$trainingCourse, 404);

        return response()->json(QuestionResource::collection($this->questionRepository->getByTrainingCourseId($trainingCourseId, ['answers', 'userAnswers'], ['answers' => ['order', 'asc']])));
    }
}
