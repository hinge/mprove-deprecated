<?php

namespace App\Http\Controllers\API\V1\TrainingCourses;

use App\Http\Resources\TrainingCourseMaterialStatusResource;
use App\Repositories\Contracts\TrainingCourseRepositoryInterface;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class TrainingCourseStatusController extends Controller
{
    protected TrainingCourseRepositoryInterface $trainingCourseRepository;
    /**
     * TrainingCourseController constructor.
     * @param TrainingCourseRepositoryInterface $trainingCourseRepository
     */
    public function __construct(TrainingCourseRepositoryInterface $trainingCourseRepository)
    {
        $this->trainingCourseRepository = $trainingCourseRepository;
    }

    /**
     *
     * @param int $id
     * @return JsonResponse
     */
    public function show(int $id): JsonResponse
    {
        return response()->json(TrainingCourseMaterialStatusResource::make($this->trainingCourseRepository->find($id, ['completedMaterials', 'materials'], ['completedMaterials' => ['order', 'asc']])));
    }
}
