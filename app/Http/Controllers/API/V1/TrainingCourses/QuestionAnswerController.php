<?php

namespace App\Http\Controllers\API\V1\TrainingCourses;

use App\Actions\StoreUserQuestionAnswerAction;
use App\Actions\UpdateUserQuestionAnswerAction;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreUserQuestionAnswerRequest;
use App\Repositories\Contracts\QuestionAnswerRepositoryInterface;
use App\Repositories\Contracts\QuestionRepositoryInterface;
use App\Repositories\Contracts\TrainingCourseQuestionRepositoryInterface;
use App\Repositories\Contracts\TrainingCourseRepositoryInterface;
use App\UserTrainingCourseQuestionAnswer;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class QuestionAnswerController extends Controller
{
    protected TrainingCourseRepositoryInterface $trainingCourseRepository;
    protected QuestionRepositoryInterface $questionRepository;
    protected QuestionAnswerRepositoryInterface $questionAnswerRepository;
    protected TrainingCourseQuestionRepositoryInterface $trainingCourseQuestionRepository;

    public function __construct(
        TrainingCourseRepositoryInterface $trainingCourseRepository,
        QuestionRepositoryInterface $questionRepository,
        QuestionAnswerRepositoryInterface $questionAnswerRepository,
        TrainingCourseQuestionRepositoryInterface $trainingCourseQuestionRepository
    ) {
        $this->trainingCourseRepository = $trainingCourseRepository;
        $this->questionRepository = $questionRepository;
        $this->questionAnswerRepository = $questionAnswerRepository;
        $this->trainingCourseQuestionRepository = $trainingCourseQuestionRepository;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param StoreUserQuestionAnswerRequest $request
     * @param StoreUserQuestionAnswerAction $storeUserQuestionAnswerAction
     * @param int $trainingCourseId
     * @param int $questionId
     * @return Response
     */
    public function store(StoreUserQuestionAnswerRequest $request, StoreUserQuestionAnswerAction $storeUserQuestionAnswerAction, int $trainingCourseId, int $questionId): Response
    {

        $questionAnswerId = request()->input('question_answer_id');

        //returns array of training course, question and answer using the ids passed through
        $relevantRecords = $this->retrieveRelevantRecords($trainingCourseId, $questionId, $questionAnswerId);

        $userId = Auth::user()->id;

        $userTrainingCourseQuestionAnswer = $this->checkQuestionAnswerRelationship($userId, $relevantRecords);

        abort_if($userTrainingCourseQuestionAnswer, '302', 'The user has already answered this question');

        $questionAnswerId = $questionAnswerId ? $relevantRecords['questionAnswer']->id : $questionAnswerId;

        $action = $storeUserQuestionAnswerAction->execute($userId, $relevantRecords['trainingCourseQuestion']->id, $questionAnswerId);

        return response('Answer submitted', 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateUserQuestionAnswerAction $updateUserQuestionAnswerAction
     * @param int $trainingCourseId
     * @param int $questionId
     * @param int $questionAnswerId
     * @return Response
     */
    public function update(UpdateUserQuestionAnswerAction $updateUserQuestionAnswerAction, int $trainingCourseId, int $questionId, int $questionAnswerId): Response
    {
        //returns array of training course, question and answer using the ids passed through
        $relevantRecords = $this->retrieveRelevantRecords($trainingCourseId, $questionId, $questionAnswerId);

        $userId = Auth::user()->id;

        $userTrainingCourseQuestionAnswer = $this->checkQuestionAnswerRelationship($userId, $relevantRecords);

        throw_unless($userTrainingCourseQuestionAnswer,
            \Exception::class,
            'User has not yet submitted an answer to this question'
        );

        $questionAnswerId = $questionAnswerId !== 0 ? $relevantRecords['questionAnswer']->id : null;

        $action = $updateUserQuestionAnswerAction->execute($userTrainingCourseQuestionAnswer, $questionAnswerId);

        return response('Answer updated', 200);
    }

    /**
     * @param int $trainingCourseId
     * @param int $questionId
     * @param int $questionAnswerId|null
     * @return array $data
     */
    private function retrieveRelevantRecords(int $trainingCourseId, int $questionId, ?int $questionAnswerId): array
    {
        $trainingCourse = $this->trainingCourseRepository->find($trainingCourseId, [], null);
        abort_if(!$trainingCourse, 404, 'Training course not found');

        $question = $this->questionRepository->find($questionId, [], null);
        abort_if(!$question, 404, 'Question not found');

        $trainingCourseQuestion = $this->trainingCourseQuestionRepository->getByQuestionAndTrainingCourse($question->id, $trainingCourse->id);
        abort_if(!$trainingCourseQuestion, 404, 'TrainingCourseQuestion record not found');

        $answer = null;

        if($questionAnswerId && $questionAnswerId !== 0) {
            $answer = $this->questionAnswerRepository->find($questionAnswerId);
            abort_if(!$answer, 404, 'Answer not found');
        }

        return [
            'trainingCourse' => $trainingCourse,
            'question' => $question,
            'trainingCourseQuestion' => $trainingCourseQuestion,
            'questionAnswer' => $answer,
        ];
    }

    /**
     * @param $userId
     * @param $relevantRecords
     * @return UserTrainingCourseQuestionAnswer
     */
    private function checkQuestionAnswerRelationship($userId, $relevantRecords): ?UserTrainingCourseQuestionAnswer
    {
        $userTrainingCourseQuestionAnswer = UserTrainingCourseQuestionAnswer::where('user_id', '=', $userId)->where('training_course_question_id', '=', $relevantRecords['trainingCourseQuestion']->id)->first();

        if($relevantRecords['questionAnswer']) {
            abort_if($relevantRecords['questionAnswer']->question_id != $relevantRecords['question']->id,
                404,
                'Answer with ID provided does not belong to this question'
            );
        }

        return $userTrainingCourseQuestionAnswer;
    }
}
