<?php

namespace App\Http\Controllers\API\V1\TrainingCourses;

use App\Actions\GenerateUserTrainingCourseCertificateAction;
use App\Http\Controllers\Controller;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Spatie\Browsershot\Browsershot;
use Symfony\Component\HttpFoundation\StreamedResponse;

class TrainingCourseCertificateController extends Controller
{

    /**
     * Get project file signed url - download
     * @param int $trainingCourseId
     * @return string
     */
    public function show(int $trainingCourseId): string
    {
        $user = Auth::user();
        $trainingCourse = \App\TrainingCourse::find($trainingCourseId);
        $userTrainingCourse = \App\UserTrainingCourse::where('user_id', $user->id)->where('training_course_id', $trainingCourse->id)->first();
        $explodedFilePath = explode('/', $userTrainingCourse->certificate_url);
        $fileName = end($explodedFilePath);

        abort_if(!Storage::exists($userTrainingCourse->certificate_url), 404);

        return Storage::temporaryUrl(
            $userTrainingCourse->certificate_url,
            now()->addMinutes(5),
            [
                'ResponseContentDisposition' => 'attachment; filename="'.$fileName.'"'
            ]
        );
    }
    /**
     * @param int $trainingCourseId
     * @return Response
     * @throws \Spatie\Browsershot\Exceptions\CouldNotTakeBrowsershot
     */

    public function store(int $trainingCourseId): Response
    {
        $user = Auth::user();
        $trainingCourse = \App\TrainingCourse::find($trainingCourseId);
        $userTrainingCourse = \App\UserTrainingCourse::where('user_id', $user->id)->where('training_course_id', $trainingCourse->id)->first();

        if($userTrainingCourse) {
            if($userTrainingCourse->certificate_url) {
                return response('Found', 302);
            }

            if ((new GenerateUserTrainingCourseCertificateAction)->execute($trainingCourse, $userTrainingCourse, $user)) {
                return response('OK', 200);
            }
        }

        return response('Not found', 404);
    }
}