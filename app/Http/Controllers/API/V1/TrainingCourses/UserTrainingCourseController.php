<?php

namespace App\Http\Controllers\API\V1\TrainingCourses;
use App\Actions\StoreUserTrainingCourseAction;
use App\Actions\UpdateUserTrainingCourseAction;
use App\Actions\BatchDeleteUserTrainingCourseQuestionAnswerAction;
use App\Actions\BatchDeleteUserTrainingCourseMaterialSeenAction;
use App\Http\Controllers\Controller;

use App\Repositories\Contracts\TrainingCourseRepositoryInterface;
use App\Services\Resolvers\UserTrainingCourseCompletionResolver;
use App\UserTrainingCourse;
use App\UserTrainingCourseMaterialSeen;
use App\UserTrainingCourseQuestionAnswer;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class UserTrainingCourseController extends Controller
{
    protected TrainingCourseRepositoryInterface $trainingCourseRepository;

    public function __construct (
        TrainingCourseRepositoryInterface $trainingCourseRepository
    )
    {
        $this->trainingCourseRepository = $trainingCourseRepository;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param StoreUserTrainingCourseAction $storeUserTrainingCourseAction
     * @param UserTrainingCourseCompletionResolver $userTrainingCourseCompletionResolver
     * @param int $trainingCourseId
     * @return Response
     */

    public function store(Request $request, StoreUserTrainingCourseAction $storeUserTrainingCourseAction, UserTrainingCourseCompletionResolver $userTrainingCourseCompletionResolver, int $trainingCourseId): Response
    {
        $userId = Auth::user()->id;

        $trainingCourse = $this->retrieveRelevantRecords($trainingCourseId)['trainingCourse'];

        abort_if(!$trainingCourse->published || $trainingCourse->draft, 403, 'Training Course is not published or is currently being drafted');

        $userTrainingCourse = UserTrainingCourse::where('user_id', '=', $userId)->where('training_course_id', '=', $trainingCourse->id)->first();

        abort_if($userTrainingCourse, 302, 'The user has already started the course');

        $completionInformation = [
            'score' => 0,
            'passed' => 0,
            'completed' => 0
        ];

        $action = $storeUserTrainingCourseAction->execute($userId, $trainingCourse->id, $completionInformation);

        return response('UserTrainingCourse record created', 200);
    }

    public function update(Request $request, UpdateUserTrainingCourseAction $updateUserTrainingCourseAction, UserTrainingCourseCompletionResolver $userTrainingCourseCompletionResolver, int $trainingCourseId): Response
    {
        $userId = Auth::user()->id;

        $trainingCourse = $this->retrieveRelevantRecords($trainingCourseId)['trainingCourse'];

        abort_if(!$trainingCourse->published || $trainingCourse->draft, 403, 'Training Course is not published or is currently being drafted');

        $completionInformation = $userTrainingCourseCompletionResolver->resolve($trainingCourse);

        $userTrainingCourse = UserTrainingCourse::where('user_id', '=', $userId)->where('training_course_id', '=', $trainingCourse->id)->first();

        abort_unless($userTrainingCourse, 404, 'The user has not started the course');

        $action = $updateUserTrainingCourseAction->execute($userId, $userTrainingCourse, $completionInformation['score'], $completionInformation['passed'], $completionInformation['completed']);

        return response('UserTrainingCourse record updated', 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param UpdateUserTrainingCourseAction $updateUserTrainingCourseAction
     * @param BatchDeleteUserTrainingCourseQuestionAnswerAction $batchDeleteUserTrainingCourseQuestionAnswerAction
     * @param BatchDeleteUserTrainingCourseMaterialSeenAction $batchDeleteUserTrainingCourseMaterialSeenAction
     * @param int $trainingCourseId
     * @return Response
     */

    public function destroy(
        Request $request,
        UpdateUserTrainingCourseAction $updateUserTrainingCourseAction,
        BatchDeleteUserTrainingCourseQuestionAnswerAction $batchDeleteUserTrainingCourseQuestionAnswerAction,
        BatchDeleteUserTrainingCourseMaterialSeenAction $batchDeleteUserTrainingCourseMaterialSeenAction,
        int $trainingCourseId
    ): Response

    {
        $userId = Auth::user()->id;

        $trainingCourse = $this->retrieveRelevantRecords($trainingCourseId)['trainingCourse'];

        $userTrainingCourse = UserTrainingCourse::where('user_id', '=', $userId)->where('training_course_id', '=', $trainingCourse->id)->first();

        abort_if(!$trainingCourse->published || $trainingCourse->draft, 403, 'Training Course is not published or is currently being drafted');
        abort_if(!$userTrainingCourse, 404, 'There is not a UserTrainingCourse record present to be retried');

        //erase score, passed, completed for the given user and given course
        $updateUserTrainingCourseAction->execute($userId, $userTrainingCourse, 0, 0, 0);

        //delete records of materials seen by the user
        $userTrainingCourseMaterialSeens = UserTrainingCourseMaterialSeen::where('user_id', '=', $userId)->whereIn('training_course_material_id', $trainingCourse->materials->pluck('id'))->get();

        if($userTrainingCourseMaterialSeens->count()) {
            $batchDeleteUserTrainingCourseMaterialSeenAction->execute($userTrainingCourseMaterialSeens);
        }

        //delete records of questions answered by the user for this course
        $userTrainingCourseQuestionAnswers = UserTrainingCourseQuestionAnswer::where('user_id', '=', $userId)->whereIn('training_course_question_id', $trainingCourse->trainingCourseQuestions->pluck('id'))->get();

        if($userTrainingCourseQuestionAnswers->count()) {
            $batchDeleteUserTrainingCourseQuestionAnswerAction->execute($userTrainingCourseQuestionAnswers);
        }

        return response('The course data has been cleared for this user', 200);
    }

    /**
     * @param int $trainingCourseId
     * @return array
     */

    private function retrieveRelevantRecords(int $trainingCourseId): array
    {
        $trainingCourse = $this->trainingCourseRepository->find($trainingCourseId, ['questions.userAnswers'], null);
        abort_if(!$trainingCourse, 404, 'Training course not found');

        return [
            'trainingCourse' => $trainingCourse,
        ];
    }
}
