<?php

namespace App\Http\Controllers\API\V1\TrainingCourses;

use App\Repositories\Contracts\TrainingCourseRepositoryInterface;
use Illuminate\Http\JsonResponse;

use App\Http\Controllers\Controller;
use App\Http\Resources\AuthenticatedTrainingCourseResource;
use Illuminate\Support\Facades\Auth;

class AvailableTrainingCourseController extends Controller
{

    protected TrainingCourseRepositoryInterface $trainingCourseRepository;

    /**
     * TrainingCourseController constructor.
     *
     * @param TrainingCourseRepositoryInterface $trainingCourseRepository
     */
    public function __construct(TrainingCourseRepositoryInterface $trainingCourseRepository)
    {
        $this->trainingCourseRepository = $trainingCourseRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        return response()->json(
            AuthenticatedTrainingCourseResource::collection($this->trainingCourseRepository->publishedAvailable(
                ['completedMaterials', 'answeredQuestions', 'materials', 'completeInformation'], ['completedMaterials' => ['order', 'asc']], Auth::user()->id)
            )
        );
    }
}
