<?php

namespace App\Http\Controllers\API\V1\TrainingCourses;

use App\Repositories\Contracts\TrainingCourseRepositoryInterface;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use \Illuminate\Http\Response;

use App\Http\Controllers\Controller;
use App\Http\Resources\AuthenticatedTrainingCourseResource;
use App\Http\Resources\TrainingCourseIndividualResource;
use Illuminate\Support\Facades\Auth;

class AuthenticatedTrainingCourseController extends Controller
{

    protected TrainingCourseRepositoryInterface $trainingCourseRepository;

    /**
     * TrainingCourseController constructor.
     *
     * @param TrainingCourseRepositoryInterface $trainingCourseRepository
     */
    public function __construct(TrainingCourseRepositoryInterface $trainingCourseRepository)
    {
        $this->trainingCourseRepository = $trainingCourseRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        return response()->json(
            AuthenticatedTrainingCourseResource::collection($this->trainingCourseRepository->publishedReleased(
                ['completedMaterials', 'answeredQuestions', 'materials', 'completeInformation'],
                ['completedMaterials' => ['order', 'asc']],
                Auth::user()->id
            ))
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return JsonResponse
     */
    public function show($id): JsonResponse
    {
        $trainingCourse = $this->trainingCourseRepository->find(
            $id,
            ['completedMaterials', 'answeredQuestions', 'completeInformation'],
            ['completedMaterials' => ['order', 'asc']]
        );

        abort_if(!$trainingCourse, 422);

        return response()->json(new AuthenticatedTrainingCourseResource($trainingCourse));
    }
}
