<?php

namespace App\Http\Controllers\API\V1\TrainingCourses;

use App\Repositories\Contracts\TrainingCourseRepositoryInterface;
use Illuminate\Http\JsonResponse;

use App\Http\Controllers\Controller;
use App\Http\Resources\UnauthenticatedTrainingCourseResource;

class UnauthenticatedTrainingCourseController extends Controller
{

    protected TrainingCourseRepositoryInterface $trainingCourseRepository;

    /**
     * TrainingCourseController constructor.
     *
     * @param TrainingCourseRepositoryInterface $trainingCourseRepository
     */
    public function __construct(TrainingCourseRepositoryInterface $trainingCourseRepository)
    {
        $this->trainingCourseRepository = $trainingCourseRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        return response()->json(
            UnauthenticatedTrainingCourseResource::collection($this->trainingCourseRepository->published(
                ['materials'], null)
            )
        );
    }
}
