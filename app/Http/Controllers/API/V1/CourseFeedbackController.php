<?php

namespace App\Http\Controllers\API\V1;


use App\CourseDate;
use App\CourseFeedback;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;

class CourseFeedbackController extends Controller
{
    /**
     * @param string $startDate
     * @param string $endDate
     * @return Response
     */
	public function index(string $startDate, string $endDate): Response
	{
		$courseDate = CourseDate::query()
			->where('start_date', $startDate)
			->where('end_date', $endDate)
			->first();

		if ($courseDate) {

			$feedback = CourseFeedback::query()
				->where('course_date_id', $courseDate->id)->get();

			return response($feedback);
		}

		return response([], 200);
	}
}
