<?php

namespace App\Http\Controllers\API\V1;

use App\Course;
use Illuminate\Http\Request;
use \Illuminate\Http\JsonResponse;
use \Illuminate\Http\Response;
use App\Http\Controllers\Controller;

class CourseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        $courses = Course::with([
            'overview',
            'details',
            'dates.venue',
            'upcoming_dates.venue',
            'previous_dates.venue'
        ])->get();

        return response()->json($courses);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return JsonResponse
     */
    public function show(int $id): JsonResponse
    {
        $course = Course::with([
            'overview',
            'details',
            'feedbacks',
            'dates.venue',
            'upcoming_dates.venue',
            'previous_dates.venue'
        ])
        ->where('id', '=', $id)
        ->first();

        if (!$course){
            return response()->json(['error' => 'Not found.'], 404);
        }

        return response()->json($course);
    }
}
