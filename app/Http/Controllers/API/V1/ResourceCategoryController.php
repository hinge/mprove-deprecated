<?php

namespace App\Http\Controllers\API\V1;

use App\ResourceType;
use App\ResourceCategory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;

class ResourceCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        $categories = ResourceCategory::with(['type']);

        if (isset($request->type_slug)){
            $type = ResourceType::where('slug', '=', $request->type_slug)->first();
            if (!$type){
                return response()->json(['error' => 'Resource type not found'], 404);
            }
            $categories->where([
                ['resource_type_id', '=', $type->id]
            ]);
        }

        return response()->json($categories->get());
    }
}
