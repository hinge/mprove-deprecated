<?php

namespace App\Http\Controllers\API\V1\Questions;

use App\Http\Controllers\Controller;
use App\Http\Resources\QuestionResource;
use App\Repositories\Contracts\QuestionRepositoryInterface;
use App\Repositories\Contracts\TrainingCourseRepositoryInterface;
use App\Repositories\QuestionRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class QuestionController extends Controller
{

    protected TrainingCourseRepositoryInterface $trainingCourseRepository;
    protected QuestionRepositoryInterface $questionRepository;

    /**
     * QuestionController constructor.
     *
     * @param TrainingCourseRepositoryInterface $trainingCourseRepository
     * @param QuestionRepositoryInterface $questionRepository
     */
    public function __construct(
        TrainingCourseRepositoryInterface $trainingCourseRepository,
        QuestionRepositoryInterface $questionRepository
    ) {
        $this->trainingCourseRepository = $trainingCourseRepository;
        $this->questionRepository = $questionRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param int $trainingCourseId
     * @return JsonResponse
     */
    public function index(int $trainingCourseId): JsonResponse
    {
        $trainingCourse = $this->trainingCourseRepository->find($trainingCourseId);
        abort_if(!$trainingCourse, 404);

        return response()->json(QuestionResource::collection($this->questionRepository->getByTrainingCourseId($trainingCourse->id, [], [])));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $trainingCourseId
     * @param  int  $trainingCourseQuestionId
     *
     * @return JsonResponse
     */
    public function show($trainingCourseId, $trainingCourseQuestionId): JsonResponse
    {
        return response()->json(QuestionResource::make($this->questionRepository->find($trainingCourseQuestionId, ['answers', 'userAnswers'], ['answers' => ['order', 'asc']])));
    }
}
