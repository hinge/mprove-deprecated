<?php

namespace App\Http\Controllers\API\V1;

use App\Resource;
use App\ResourceType;
use App\ResourceCategory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;

class ResourceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        $resources = Resource::with(['categories']);

        if (isset($request->type_slug)){
            $type = ResourceType::where('slug', '=', $request->type_slug)->first();
            if (!$type){
                return response()->json(['error' => 'Resource type not found'], 404);
            }
            $resources->where([
                ['type_id', '=', $type->id]
            ]);
        }

        if (isset($request->category_slug)){
            $category = ResourceCategory::where('slug', '=', $request->category_slug)->first();
            if (!$category){
                return response()->json(['error' => 'Resource category not found'], 404);
            }
            $resources->whereHas('categories', function($query) use ($category) {
                $query->where('slug', '=', $category->slug);
            });
        }

        return response()->json($resources->get());
    }
}
