<?php

namespace App\Http\Controllers\API\V1\TrainingCourseMaterials;
use App\Actions\StoreUserTrainingCourseMaterialSeenAction;
use App\Http\Controllers\Controller;

use App\Repositories\Contracts\TrainingCourseMaterialRepositoryInterface;
use App\Repositories\Contracts\TrainingCourseRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class TrainingCourseMaterialSeenController extends Controller
{
    protected TrainingCourseMaterialRepositoryInterface $trainingCourseMaterialRepository;
    protected TrainingCourseRepositoryInterface $trainingCourseRepository;

    public function __construct (
        TrainingCourseMaterialRepositoryInterface $trainingCourseMaterialRepository,
        TrainingCourseRepositoryInterface $trainingCourseRepository
    )
    {
        $this->trainingCourseRepository = $trainingCourseRepository;
        $this->trainingCourseMaterialRepository = $trainingCourseMaterialRepository;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param StoreUserTrainingCourseMaterialSeenAction $storeUserTrainingCourseMaterialSeenAction
     * @param int $trainingCourseId
     * @param int $trainingCourseMaterialId
     * @return Response
     */

    public function store(Request $request, StoreUserTrainingCourseMaterialSeenAction $storeUserTrainingCourseMaterialSeenAction, int $trainingCourseId, int $trainingCourseMaterialId): Response
    {
        $userId = Auth::user()->id;

        $relevantRecords = $this->retrieveRelevantRecords($trainingCourseId, $trainingCourseMaterialId);

        abort_if(!$relevantRecords['trainingCourse']->materials->contains($relevantRecords['trainingCourseMaterial']), 404, 'Training course material with ID provided does not belong to this training course');

        abort_if($relevantRecords['trainingCourse']->completedMaterials->pluck('training_course_material_id')->contains($relevantRecords['trainingCourseMaterial']->id), 302, 'Training course material was already seen by this user');

        $action = $storeUserTrainingCourseMaterialSeenAction->execute($userId, $trainingCourseMaterialId);
        return response('UserTrainingCourseMaterialSeen record submitted', 200);
    }

    /**
     * @param int $trainingCourseId
     * @param int $trainingCourseMaterialId
     * @return array
     */

    private function retrieveRelevantRecords(int $trainingCourseId, int $trainingCourseMaterialId): array
    {
        $trainingCourse = $this->trainingCourseRepository->find($trainingCourseId, [], null);
        abort_if(!$trainingCourse, 404, 'Training course not found');

        $trainingCourseMaterial = $this->trainingCourseMaterialRepository->find($trainingCourseMaterialId);
        abort_if(!$trainingCourseMaterial, 404, 'Training course material not found');

        return [
            'trainingCourse' => $trainingCourse,
            'trainingCourseMaterial' => $trainingCourseMaterial
        ];
    }
}
