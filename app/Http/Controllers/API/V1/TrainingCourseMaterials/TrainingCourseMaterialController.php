<?php

namespace App\Http\Controllers\API\V1\TrainingCourseMaterials;

use App\Http\Controllers\Controller;
use App\Http\Resources\TrainingCourseMaterialResource;
use App\Repositories\Contracts\TrainingCourseMaterialRepositoryInterface;
use App\Repositories\Contracts\TrainingCourseRepositoryInterface;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class TrainingCourseMaterialController extends Controller
{
    protected TrainingCourseRepositoryInterface $trainingCourseRepository;
    protected TrainingCourseMaterialRepositoryInterface $trainingCourseMaterialRepository;

    /**
     * TrainingCourseMaterialController constructor.
     *
     * @param TrainingCourseRepositoryInterface $trainingCourseRepository
     * @param TrainingCourseMaterialRepositoryInterface $trainingCourseMaterialRepository
     */
    public function __construct(
        TrainingCourseRepositoryInterface $trainingCourseRepository,
        TrainingCourseMaterialRepositoryInterface $trainingCourseMaterialRepository
    ) {
        $this->trainingCourseRepository = $trainingCourseRepository;
        $this->trainingCourseMaterialRepository = $trainingCourseMaterialRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param int $trainingCourseId
     * @return JsonResponse
     */
    public function index(int $trainingCourseId): JsonResponse
    {
        $trainingCourse = $this->trainingCourseRepository->find($trainingCourseId);
        abort_if(!$trainingCourse, 404);

        return response()->json(TrainingCourseMaterialResource::collection($this->trainingCourseMaterialRepository->getByTrainingCourseId($trainingCourseId, ['references.type'])));
    }
}
