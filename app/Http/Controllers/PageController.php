<?php

namespace App\Http\Controllers;

use App\Course;
use \Carbon\Carbon;
use Illuminate\Http\Request;

class PageController extends Controller
{
    /**
     * Show the application splash screen.
     *
     * @return Response
     */
    public function show()
    {
        return view('pages.index');
    }

    public function showAbout()
    {
        return view('pages.about');
    }

    public function showEducation()
    {
        return view('pages.education');
    }

    public function showMprovement()
    {
        return view('pages.mprovement',[
            'pageHero' => [
                'heading' => 'Quality MPROvEment',
                'intro' => 'This channel provides toolkits and educational care bundles for QI with a view to driving improvement in neonatal care.',
            ],
            'meta_title' => 'Quality MPRoVEMent'
        ]);
    }

    public function showToolkits(Request $request)
    {
        return view('pages.resources',[
            'pageHero' => [
                'heading' => 'Toolkits',
                'intro' => 'This part of the website provides resources that can be used by multidisciplinary teams under creative commons distribution for neonatal education, and quality improvement',
                'back' => [
                    'text' => 'Quality MPROvEment'
                ]
            ],
            'meta_title' => 'Quality MPROvEment - Toolkits',
            'resource_type_slug' => 'toolkit'
        ]);
    }

    public function showScenarioBank(Request $request)
    {
        return view('pages.resources',[
            'pageHero' => [
                'heading' => 'Scenario Bank',
                'intro' => 'These are accounts of neonatal simulation scenarios arranged by system with their respective powerpoint presentations and videos.',
                'back' => [
                    'text' => 'MPROvE Academy'
                ]
            ],
            'meta_title' => 'MPROvE Academy - Scenario Bank',
            'resource_type_slug' => 'scenario'
        ]);
    }

	public function showMoulage(Request $request)
    {
        return view('pages.resources',[
            'pageHero' => [
                'heading' => 'Moulage',
                'intro' => 'Making things more realistic and lifelike',
                'back' => [
                    'text' => 'MPROvE Academy'
                ]
            ],
            'meta_title' => 'MPROvE Academy - Moulage',
            'resource_type_slug' => 'moulage'
        ]);
    }

    public function showAcademy()
    {
        return view('pages.courses', [
            'pageHero' => [
                'heading' => 'MPROvE Academy',
                'intro' => 'The MPROvE Academy was established in 2012 and delivers courses on Neonatal Ethics, The Neonatal Airway and Simulation as well as Instructor training globally. The concept MPROvE relates to use of multidisciplinary education of any kind (i.e. simulation, or TEL) being used to improve the quality of neonatal care and outcomes',
            ],
            'meta_title' => 'MPROvE Academy - Courses'
        ]);
    }

    public function showAcademyCourseDates(Request $request)
    {
        $course = Course::where('slug', '=', $request->course)->firstOrFail();
        return view('pages.course-dates', [
            'course_id' => $course->id,
            'meta_title' => $course->name
        ]);
    }

    public function showAcademyCourseDetails(Request $request, $slug, $venue_slug, $date_slug)
    {
        $course = Course::where('slug', '=', $slug)
        ->with('dates.venue')
        ->whereHas('dates.venue', function($query) use ($date_slug, $venue_slug){
            $query->where([
                ['start_date', '<=', Carbon::parse($date_slug)->format('Y-m-d 23:59:59')],
                ['start_date', '>=', Carbon::parse($date_slug)->format('Y-m-d 00:00:00')],
                ['venues.slug', '=', $venue_slug],
            ]);
        })
        ->firstOrFail();

        $venueName = $course->dates->first()->venue->name;

        return view('pages.course-details', [
            'course_id' => $course->id,
            'course_date' => $date_slug,
            'venue_slug' => $venue_slug,
            'meta_title' => $course->name.' - '.$venueName . ' - ' . Carbon::parse($date_slug)->format('dS F Y')
        ]);
    }

    public function trainginCourses()
    {
        return view('pages.training-courses');
    }

}
