<?php

namespace App\Http\Controllers\TrainingCourses;

use App\Http\Controllers\Controller;
use App\Services\Resolvers\Contracts\UserTrainingCourseCertificateResolverInterface;
use App\TrainingCourse;
use App\User;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Auth;

class TrainingCourseController extends Controller
{
    /**
     * @return View
     */
    public function index(): View
    {
        return view('training-courses.index', [
            'banner' => [
                'title' => 'Online Training Courses',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam ornare tellus sit amet mi consequat viverra. Donec dui dui, suscipit quis egestas quis, placerat quis dolor. Donec non magna bibendum.',
                'back' => [
                    'to' => '/quality-mprovement',
                    'label' => 'Quality MPROvEment'
                ]
            ],
            'intro' => [
                'title' => 'Started',
                'description' => 'Training courses that are currenly in progress and not complete.'
            ],
            'meta_title' => 'Online Training Courses',
        ]);
    }

    /**
     * @param int $trainingCourseId
     * @return View
     */
    public function show(int $trainingCourseId): View
    {
        return view('training-courses.show', [
            'trainingCourseId' => $trainingCourseId,
            'banner' => [
                'back' => [
                    'to' => '/online-training-courses',
                    'label' => 'Online Training Courses'
                ]
            ],
            'intro' => [
                'title' => 'Course material',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam pulvinar felis bibendum lectus rutrum, sit amet dictum neque consectetur. Donec non volutpat purus...'
            ],
            'meta_title' => 'Online Training Courses',
        ]);
    }

    /**
     * @param int $trainingCourseId
     * @return View
     */
    public function questions(int $trainingCourseId): View
    {

        return view('training-courses.questions', [
            'trainingCourseId' => $trainingCourseId,
            'banner' => [
                'back' => [
                    'to' => '/online-training-courses',
                    'label' => 'Online Training Courses'
                ]
            ],
            'intro' => [
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam pulvinar felis bibendum lectus rutrum, sit amet dictum neque consectetur. Donec non volutpat purus...'
            ],
            'meta_title' => 'Online Training Courses',
        ]);
    }

    /**
     * @param int $trainingCourseId
     * @return View
     */
    public function questionAnswers(int $trainingCourseId): View
    {
        return view('training-courses.questionAnswers', [
            'trainingCourseId' => $trainingCourseId,
            'banner' => [
                'back' => [
                    'to' => '/online-training-courses',
                    'label' => 'Online Training Courses'
                ]
            ],
            'intro' => [
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam pulvinar felis bibendum lectus rutrum, sit amet dictum neque consectetur. Donec non volutpat purus...'
            ],
            'meta_title' => 'Online Training Courses',
        ]);
    }

    /**
     * @param int $trainingCourseId
     * @return View
     */
    public function questionsSummary(int $trainingCourseId): View
    {
        return view('training-courses.questionsSummary', [
            'trainingCourseId' => $trainingCourseId,
            'banner' => [
                'back' => [
                    'to' => '/online-training-courses',
                    'label' => 'Online Training Courses'
                ]
            ],
            'intro' => [
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam pulvinar felis bibendum lectus rutrum, sit amet dictum neque consectetur. Donec non volutpat purus...'
            ],
            'meta_title' => 'Online Training Courses',
        ]);
    }

    /**
     * @param int $trainingCourseId
     * @param int $userId
     * @return View|null
     */
    public function certificateGenerate(int $trainingCourseId, int $userId): ?View
    {
        $user = User::find($userId);
        $trainingCourse = TrainingCourse::find($trainingCourseId);

        $certificateData = resolve(UserTrainingCourseCertificateResolverInterface::class)->resolve($trainingCourse, $user);

        app('debugbar')->disable();

        if($certificateData) {
            return view('training-courses.certificate', ['certificateData' => $certificateData]);
        }

        return null;
    }
}
