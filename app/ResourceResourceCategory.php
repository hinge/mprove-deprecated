<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\Pivot;

class ResourceResourceCategory extends Pivot
{
    protected $table = 'resource_resource_category';
}
