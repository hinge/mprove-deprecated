<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\Pivot;

class TrainingCourseQuestion extends Pivot
{
    protected $table = 'training_course_questions';

    public $timestamps = false;
}
