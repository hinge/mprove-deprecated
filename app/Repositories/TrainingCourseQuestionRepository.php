<?php
namespace App\Repositories;

use App\Repositories\Contracts\TrainingCourseQuestionRepositoryInterface;
use App\TrainingCourseQuestion;
use Illuminate\Support\Collection;

class TrainingCourseQuestionRepository implements TrainingCourseQuestionRepositoryInterface {

    /**
     * @param int $trainingCourseQuestionId
     * @return TrainingCourseQuestion|Null
     */
    public function find(int $trainingCourseQuestionId): ?TrainingCourseQuestion
    {
        return TrainingCourseQuestion::find($trainingCourseQuestionId);
    }

    public function all(): Collection
    {
        return TrainingCourseQuestion::all();
    }

    public function getByQuestionAndTrainingCourse(int $questionId, int $trainingCourseId): ?TrainingCourseQuestion
    {
        return TrainingCourseQuestion::where('question_id', '=', $questionId)->where('training_course_id', '=', $trainingCourseId)->first();
    }

}