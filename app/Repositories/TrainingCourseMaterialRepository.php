<?php

namespace App\Repositories;

use App\TrainingCourseMaterial;
use Illuminate\Support\Collection;
use App\Repositories\Contracts\TrainingCourseMaterialRepositoryInterface;
use App\Repositories\Traits\BaseRepositoryTrait;

class TrainingCourseMaterialRepository implements TrainingCourseMaterialRepositoryInterface
{
    use BaseRepositoryTrait;
    /**
     * @inheritDoc
     */
    public function find(int $id): ?TrainingCourseMaterial
    {
        return TrainingCourseMaterial::find($id);
    }

    /**
     * @inheritDoc
     */
    public function all(): Collection
    {
        return TrainingCourseMaterial::all();
    }

    /**
     * @inheritDoc
     */
    public function getByTrainingCourseId(int $trainingCourseId, array $relations = self::WITH_RELATIONS, ?array $orderBy = null): Collection
    {

        $query = TrainingCourseMaterial::query();

        $query = $this->loadWithOrder($query, $relations, $orderBy);

        return $query->where('training_course_id', '=', $trainingCourseId)->orderBy('order', 'asc')->get();
    }
}
