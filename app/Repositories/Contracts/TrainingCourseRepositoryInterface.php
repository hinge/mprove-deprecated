<?php

namespace App\Repositories\Contracts;

use App\TrainingCourse;
use \Illuminate\Support\Collection;

interface TrainingCourseRepositoryInterface
{
    const WITH_RELATIONS = [];

    /**
     * Get's a record by it's ID
     *
     * @param int $id
     * @param array $relations
     * @param array $orderBy
     * @return TrainingCourse|null
     */
    public function find(int $id, array $relations = self::WITH_RELATIONS, ?array $orderBy = null): ?TrainingCourse;

    /**
    * Get's all records.
    *
    * @return Collection
    */
    public function all(): Collection;

    /**
     * Get's all published records
     *
     * @param array $relations
     * @param array|null $orderBy
     * @return Collection
     */
    public function published(array $relations = self::WITH_RELATIONS, ?array $orderBy = null): Collection;


    /**
     * Get's all published records available to the user
     *
     * @param array $relations
     * @param array|null $orderBy
     * @param int $userId
     * @return Collection
     */
    public function publishedAvailable(array $relations = self::WITH_RELATIONS, ?array $orderBy = null, int $userId): Collection;
}
