<?php

namespace App\Repositories\Contracts;

use App\TrainingCourseMaterial;
use \Illuminate\Support\Collection;

interface TrainingCourseMaterialRepositoryInterface
{
    const WITH_RELATIONS = [];

    /**
     * Get's a record by it's ID
     *
     * @param int
     * @return TrainingCourseMaterial|null
     *
     */
    public function find(int $id): ?TrainingCourseMaterial;

    /**
     * Get's all records.
     *
     * @return Collection
     */
    public function all(): Collection;

    /**
     * Get's records by training course ID
     *
     * @param $trainingCourseId
     * @param array $relations
     * @param array|null $orderBy
     * @return Collection
     */
    public function getByTrainingCourseId(int $trainingCourseId, array $relations = self::WITH_RELATIONS, ?array $orderBy = null): Collection;
}
