<?php

namespace App\Repositories\Contracts;

use App\TrainingCourseQuestion;
use \Illuminate\Support\Collection;

interface TrainingCourseQuestionRepositoryInterface
{
    const WITH_RELATIONS = [];

    /**
     * Get's a record by it's ID
     *
     * @param int $trainingCourseQuestionId
     * @return TrainingCourseQuestion|null
     *
     */
    public function find(int $trainingCourseQuestionId): ?TrainingCourseQuestion;

    /**
     * Get's all records.
     *
     * @return Collection
     */
    public function all(): Collection;

    /**
     * @param int $questionId
     * @param int $trainingCourseId
     * @return TrainingCourseQuestion|Null
     */
    public function getByQuestionAndTrainingCourse(int $questionId, int $trainingCourseId): ?TrainingCourseQuestion;
}
