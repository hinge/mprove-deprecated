<?php

namespace App\Repositories\Contracts;

use App\Question;
use \Illuminate\Support\Collection;

interface QuestionRepositoryInterface
{
    const WITH_RELATIONS = [];

    /**
     * Get's a record by it's ID
     *
     * @param int $id
     * @param array $relations
     * @param array $orderBy
     * @return Question|null
     *
     */
    public function find(int $id, array $relations = self::WITH_RELATIONS, ?array $orderBy = null): ?Question;

    /**
     * Get's all records.
     *
     * @return Collection
     */
    public function all(): Collection;

    /**
     * @param $trainingCourseId
     * @param array $relations
     * @param array|null $orderBy
     * @return Collection
     */
    public function getByTrainingCourseId($trainingCourseId, array $relations = self::WITH_RELATIONS, ?array $orderBy = null): Collection;
}
