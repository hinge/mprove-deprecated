<?php

namespace App\Repositories\Contracts;

use App\QuestionAnswer;
use \Illuminate\Support\Collection;

interface QuestionAnswerRepositoryInterface
{
    /**
     * Get's a record by it's ID
     *
     * @param int
     * @return QuestionAnswer|null
     *
     */
    public function find(int $id): ?QuestionAnswer;

    /**
     * Get's all records.
     *
     * @return Collection
     */
    public function all(): Collection;
}