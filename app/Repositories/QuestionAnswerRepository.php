<?php

namespace App\Repositories;

use App\QuestionAnswer;
use Illuminate\Support\Collection;
use App\Repositories\Contracts\QuestionAnswerRepositoryInterface;

class QuestionAnswerRepository implements QuestionAnswerRepositoryInterface
{

    /**
     * @inheritDoc
     */
    public function find(int $id): ?QuestionAnswer
    {
        return QuestionAnswer::find($id);
    }

    /**
     * @inheritDoc
     */
    public function all(): Collection
    {
        return QuestionAnswer::all();
    }
}