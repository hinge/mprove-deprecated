<?php

namespace App\Repositories;

use Illuminate\Support\Collection;
use App\TrainingCourse;
use App\Repositories\Contracts\TrainingCourseRepositoryInterface;
use App\Repositories\Traits\BaseRepositoryTrait;

class TrainingCourseRepository implements TrainingCourseRepositoryInterface
{
    use BaseRepositoryTrait;

    /**
     * @inheritDoc
     */
    public function all(): Collection
    {
        return TrainingCourse::query()
            ->orderBy('name', 'asc')
            ->get();
    }

    /**
     * @inheritDoc
     */
    public function find(int $id, array $relations = self::WITH_RELATIONS, ?array $orderBy = null): ?TrainingCourse
    {
        $query = TrainingCourse::query()->where('draft','!=', '1')->where('published', '=', 1);

        $query = $this->loadWithOrder($query, $relations, $orderBy);

        $trainingCourse = $query->where('id', $id)->first();

        abort_unless($trainingCourse, 404, 'Training Course does not exist, is not published or being drafted');

        return $trainingCourse;
    }

    /**
     * @inheritDoc
     */
    public function published(array $relations = self::WITH_RELATIONS, ?array $orderBy = null): Collection
    {

        $query = TrainingCourse::query();

        $query = $this->loadWithOrder($query, $relations, $orderBy);

        return $query->where('published', 1)
            ->withCount(['questions', 'materials'])
            ->orderBy('name', 'asc')
            ->get();
    }

    /**
     * @inheritDoc
     */
    public function publishedReleased(array $relations = self::WITH_RELATIONS, ?array $orderBy = null, int $userId): Collection
    {
        $query = TrainingCourse::query()
            ->whereHas('completeInformation', function($query) use ($userId) {
                $query->where('user_id', $userId);
            });

        $query = $this->loadWithOrder($query, $relations, $orderBy);


        return $query->where('published', '=', 1)
            ->where('draft', '!=', 1)
            ->withCount(['questions', 'materials'])
            ->orderBy('name', 'asc')
            ->get();
    }

    /**
     * @inheritDoc
     */
    public function publishedAvailable(array $relations = self::WITH_RELATIONS, ?array $orderBy = null, int $userId): Collection
    {
        $query = TrainingCourse::query()
            ->whereDoesntHave('completeInformation', function($query) use ($userId) {
                $query->where('user_id', $userId);
            });

        $query = $this->loadWithOrder($query, $relations, $orderBy);

        return $query->where('published', 1)
            ->withCount(['questions', 'materials'])
            ->orderBy('name', 'asc')
            ->get();
    }
}
