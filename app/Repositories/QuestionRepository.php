<?php

namespace App\Repositories;

use App\Question;
use Illuminate\Support\Collection;
use App\Repositories\Contracts\QuestionRepositoryInterface;
use App\Repositories\Traits\BaseRepositoryTrait;

class QuestionRepository implements QuestionRepositoryInterface
{
    use BaseRepositoryTrait;
    /**
     * @inheritDoc
     */
    public function find(int $id, array $relations = self::WITH_RELATIONS, ?array $orderBy = null): ?Question
    {
        $query = Question::query();

        $query = $this->loadWithOrder($query, $relations, $orderBy);

        return $query->where('id', $id)->first();
    }

    /**
     * @inheritDoc
     */
    public function all(): Collection
    {
        return Question::all();
    }

    /**
     * @inheritDoc
     */
    public function getByTrainingCourseId($trainingCourseId, array $relations = self::WITH_RELATIONS, ?array $orderBy = null): Collection
    {
        $query = Question::query();

        $query = $this->loadWithOrder($query, $relations, $orderBy);

        $query->join('training_course_questions', 'questions.id', '=', 'training_course_questions.question_id')
            ->where('training_course_questions.training_course_id', '=', $trainingCourseId)
            ->orderBy('training_course_questions.order', 'asc');

        return $query->get();
    }
}
