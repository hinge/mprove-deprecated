<?php

namespace App\Repositories\Traits;

use Illuminate\Database\Eloquent\Builder;

trait BaseRepositoryTrait {

    /**
     * @param Builder $query
     * @param array $relations
     * @param array $orderBy
     * @return Builder
     */
    public function loadWithOrder(Builder $query, array $relations, ?array $orderBy): Builder
    {
        if (!$orderBy) {
            return $query->with($relations);
        }

        foreach ($relations as $relation) {
            if (isset($orderBy[$relation])) {
                $query->with([$relation => fn($query) => $query->orderBy($orderBy[$relation][0], $orderBy[$relation][1])]);
                continue;
            }
            $query->with($relation);
        }

        return $query;
    }
}