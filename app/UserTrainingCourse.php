<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\Pivot;

class UserTrainingCourse extends Pivot
{
    protected $table = 'user_training_courses';
}
