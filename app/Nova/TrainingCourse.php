<?php

namespace App\Nova;

use Illuminate\Http\Request;
use Laravel\Nova\Fields\BelongsToMany;
use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Textarea;
use Laravel\Nova\Http\Requests\NovaRequest;
use Techouse\IntlDateTime\IntlDateTime as DateTime;

class TrainingCourse extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\TrainingCourse::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'name';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'name', 'description'
    ];

    /**
     * Provides default dates for timestamps without user needing to populate them manually on create.
     *
     * @return object
     */
    public static function newModel(): object
    {
        $model = new static::$model;
        $model->setAttribute('created_at', now());
        $model->setAttribute('updated_at', now());
        return $model;
    }

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request): array
    {
        return [
            ID::make()->sortable(),

            Text::make('Name')
                ->sortable()
                ->rules('required', 'max:255'),

            Textarea::make('Description')
                ->rules('required')
                ->hideFromIndex(),

            Number::make('Pass rate')
                ->sortable()
                ->rules('required', 'numeric', 'min:1', 'max:100'),

            Boolean::make('Published')
                ->sortable(),

            Boolean::make('Draft')
                ->sortable(),

            BelongsToMany::make('Questions', 'questions', 'App\Nova\Question')->fields(function() {
                return  [
                    Number::make('Order'),
                ];
            }),

            DateTime::make('Created', 'created_at')
                ->dateFormat('DD/MM/YYYY')
                ->sortable()
                ->rules('required')
                ->hideWhenUpdating()
                ->onlyOnDetail(),

            DateTime::make('Updated', 'updated_at')
                ->dateFormat('DD/MM/YYYY')
                ->sortable()
                ->rules('required')
                ->hideWhenUpdating()
                ->onlyOnDetail(),

            HasMany::make('Training Course Materials', 'materials', 'App\Nova\TrainingCourseMaterial'),

        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request): array
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request): array
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request): array
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request): array
    {
        return [];
    }
}
