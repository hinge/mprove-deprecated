<?php

namespace App\Nova;

use Illuminate\Http\Request;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\BelongsToMany;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Markdown;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Textarea;
use Laravel\Nova\Http\Requests\NovaRequest;
use Techouse\IntlDateTime\IntlDateTime as DateTime;

class TrainingCourseMaterial extends Resource
{

    /**
     * Indicates if the resource should be displayed in the sidebar.
     *
     * @var bool
     */
    public static $displayInNavigation = false;

    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\TrainingCourseMaterial::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'title';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'title', 'content'
    ];

    /**
     * Provides default dates for timestamps without user needing to populate them manually on create.
     *
     * @return object
     */
    public static function newModel(): object
    {
        $model = new static::$model;
        $model->setAttribute('created_at', now());
        $model->setAttribute('updated_at', now());
        return $model;
    }
    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request): array
    {
        return [
            ID::make()->sortable(),

            Text::make('Title')
                ->sortable()
                ->rules('required', 'max:255'),

            Markdown::make('Content')
                ->rules('required')
                ->hideFromIndex(),

            Textarea::make('Summary')
                ->rules('required', 'max:191'),

            Number::make('Order')
                ->sortable()
                ->rules('required', 'numeric', 'max:300'),

            DateTime::make('Created', 'created_at')
                ->dateFormat('DD/MM/YYYY')
                ->sortable()
                ->rules('required')
                ->hideWhenUpdating()
                ->onlyOnDetail(),

            DateTime::make('Updated', 'updated_at')
                ->dateFormat('DD/MM/YYYY')
                ->sortable()
                ->rules('required')
                ->hideWhenUpdating()
                ->onlyOnDetail(),

            BelongsToMany::make('Training Course References', 'references', 'App\Nova\TrainingCourseReference')->fields(function(){
                return [
                    Text::make('Unique Reference Number')
                        ->rules('required', 'max:255'),
                ];
            }),

            BelongsTo::make('Training Course', 'course', 'App\Nova\TrainingCourse'),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request): array
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request): array
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request): array
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request): array
    {
        return [];
    }
}
