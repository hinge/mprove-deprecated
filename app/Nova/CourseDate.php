<?php

namespace App\Nova;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\BelongsTo;
use Techouse\IntlDateTime\IntlDateTime as DateTime;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Http\Requests\NovaRequest;

class CourseDate extends Resource
{

    /**
     * Indicates if the resource should be displayed in the sidebar.
     *
     * @var bool
     */
    public static $displayInNavigation = false;

    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\CourseDate::class;


    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
    ];


    /**
     * Custom value for display title.
     *
     * @return string
     */
    public function title(): string
    {
        return $this->start_date->format('d/m/Y');
    }

    /**
     * Provides default dates for timestamps without user needing to populate them manually on create.
     *
     * @return object
     */
    public static function newModel(): object
    {
        $model = new static::$model;
        $model->setAttribute('created_at', now());
        $model->setAttribute('updated_at', now());
        return $model;
    }

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request): array
    {
        return [
            ID::make()->sortable(),

            Text::make('Currency')
                ->sortable()
                ->rules( 'max:3')
                ->hideFromIndex(),

            Text::make('Registration URL')
                ->sortable()
                ->rules( 'required', 'max:255')
                ->hideFromIndex(),

            Text::make('Certificate URL')
                ->sortable()
                ->rules('max:255')
                ->hideFromIndex(),

            Text::make('Resources URL')
                ->sortable()
                ->rules('max:255')
                ->hideFromIndex(),

            Text::make('Code')
                ->sortable()
                ->rules( 'max:255')
            ->hideFromIndex(),

            DateTime::make('Start Date', 'start_date')
                ->dateFormat('DD/MM/YYYY')
                ->sortable(),

            DateTime::make('End Date', 'end_date')
                ->dateFormat('DD/MM/YYYY')
                ->sortable(),

            DateTime::make('Created', 'created_at')
                ->dateFormat('DD/MM/YYYY')
                ->sortable()
                ->rules('required')
                ->hideWhenUpdating()
                ->onlyOnDetail(),

            DateTime::make('Updated', 'updated_at')
                ->dateFormat('DD/MM/YYYY')
                ->sortable()
                ->rules('required')
                ->hideWhenUpdating()
                ->onlyOnDetail(),

            HasMany::make('Venue'),

            HasMany::make('Course Prices', 'prices', 'App\Nova\CoursePrice'),

            BelongsTo::make('Course')
                ->hideWhenUpdating()
                ->hideFromIndex()
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request): array
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request): array
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request): array
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request): array
    {
        return [];
    }
}
