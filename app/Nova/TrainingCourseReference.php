<?php

namespace App\Nova;

use Illuminate\Http\Request;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\BelongsToMany;
use Laravel\Nova\Fields\File;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Http\Requests\NovaRequest;
use Techouse\IntlDateTime\IntlDateTime as DateTime;

class TrainingCourseReference extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\TrainingCourseReference::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'title';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
    ];

    /**
     * Provides a custom resource label for the nova resource menu.
     *
     * @return string
     */
    public static function label(): string {
        return 'References';
    }

    /**
     * Provides default dates for timestamps without user needing to populate them manually on create.
     *
     * @return object
     */
    public static function newModel(): object
    {
        $model = new static::$model;
        $model->setAttribute('created_at', now());
        $model->setAttribute('updated_at', now());
        return $model;
    }
    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request): array
    {
        return array(
            ID::make()->sortable(),

            Text::make('Title')
                ->rules('required', 'max:255')
                ->sortable(),

            DateTime::make('Created', 'created_at')
                ->dateFormat('DD/MM/YYYY')
                ->sortable()
                ->rules('required')
                ->hideWhenUpdating()
                ->onlyOnDetail(),

            DateTime::make('Updated', 'updated_at')
                ->dateFormat('DD/MM/YYYY')
                ->sortable()
                ->rules('required')
                ->hideWhenUpdating()
                ->onlyOnDetail(),

            BelongsTo::make('Training course reference type', 'type', 'App\Nova\TrainingCourseReferenceType'),

            BelongsToMany::make('Training Course Materials', 'materials', 'App\Nova\TrainingCourseMaterial')->fields(function(){
                return [
                    Text::make('Unique Reference Number')
                        ->rules('required', 'max:255'),
                ];
            }),

            Text::make('URL')
                ->rules('required_without:file', 'max:255')
                ->sortable(),

            File::make('File', null, 's3')
                ->disk('s3')
                ->path('training-courses/references')
                ->prunable()
                ->rules('required_without:url'),

            Text::make('File URL', 'file' , function () {
                return '<a class="cursor-pointer dim inline-block text-primary font-bold" href="'.env('S3_PATH').'/'.$this->file.'">'.env('S3_PATH').'/'.$this->file.'</a>';
            })
                ->hideWhenCreating()
                ->hideWhenUpdating()
                ->asHtml(),

        );
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request): array
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request): array
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request): array
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request): array
    {
        return [];
    }
}
