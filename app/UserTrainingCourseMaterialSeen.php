<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\Pivot;

class UserTrainingCourseMaterialSeen extends Pivot
{
    protected $table = 'user_training_course_material_seens';
}
