<?php

namespace App\Services\Resolvers;

use App\Services\Resolvers\Contracts\TextReferenceResolverInterface;
use Illuminate\Support\Collection;

class TextReferenceResolver implements TextReferenceResolverInterface
{
    /**
     * @inheritDoc
     */
    public function resolve(string $text, Collection $references): string
    {
        preg_match_all("/\[(.*?)]/", $text, $matches);

        $validReferences = $references->whereIn('pivot.unique_reference_number', $matches[1])->pluck('url','pivot.unique_reference_number')->toArray();
        $validReferenceIds = array_keys($validReferences);
        $validInTextReferences = [];

        foreach($matches[1] as $key => $matchId) {
            if(in_array($matchId, $validReferenceIds) && ctype_alnum($matches[1][$key])) {
                $validInTextReferences[$matches[0][$key]] = "<a target='_blank' href='{$validReferences[$matchId]}'>{$matches[0][$key]}</a>";
            }
        }

        $find = array_keys($validInTextReferences);
        $replace = array_values($validInTextReferences);

        return str_ireplace($find, $replace, $text);
    }
}
