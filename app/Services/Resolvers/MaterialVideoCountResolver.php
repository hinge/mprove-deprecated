<?php

namespace App\Services\Resolvers;

use App\Services\Resolvers\Contracts\MaterialVideoCountResolverInterface;
use Illuminate\Database\Eloquent\Collection;

class MaterialVideoCountResolver implements MaterialVideoCountResolverInterface {


    /**
     * @inheritDoc
     */
    public function resolve(Collection $materials): int
    {
        $content = $materials->implode('content', ' ');

        preg_match_all("/\<iframe(.*)>/", $content, $matches);

        return count($matches[0]);
    }
}