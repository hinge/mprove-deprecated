<?php

namespace App\Services\Resolvers;

use App\TrainingCourse;

use App\Services\Resolvers\Contracts\UserTrainingCourseCompletionResolverInterface;

class UserTrainingCourseCompletionResolver implements UserTrainingCourseCompletionResolverInterface{

    /**
     * @inheritDoc
     */
    public function resolve(TrainingCourse $trainingCourse): array
    {
        $correctAnswers = $trainingCourse->questions->filter(function($question) {
            return $question->userAnswers ? optional($question->userAnswers->first())->correct : false;
        })->count();

        $questionCount = $trainingCourse->questions->count();

        $score = floor($correctAnswers / $questionCount * 100);
        $passed = $score >= $trainingCourse->pass_rate;
        $completed = true;

        return [
            'score' => $score,
            'passed' => $passed,
            'completed' => $completed
        ];
    }
}