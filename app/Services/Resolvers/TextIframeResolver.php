<?php

namespace App\Services\Resolvers;

use App\Services\Resolvers\Contracts\TextIframeResolverInterface;

class TextIframeResolver implements TextIframeResolverInterface
{
    /**
     * @inheritDoc
     */
    public function resolve(string $text): string
    {
        preg_match_all("/\<iframe(.*)>/", $text, $matches);

        $wrappedMatches = [];

        foreach($matches[0] as $match) {
            $wrappedMatches[] = "<div class='iframeWrapper'>{$match}</div>";
        }

        return str_ireplace($matches[0], $wrappedMatches, $text);
    }
}
