<?php

namespace App\Services\Resolvers;

use App\TrainingCourse;

use App\Services\Resolvers\Contracts\UserTrainingCourseCertificateResolverInterface;
use App\User;
use App\UserTrainingCourse;

class UserTrainingCourseCertificateResolver implements UserTrainingCourseCertificateResolverInterface {

    /**
     * @inheritDoc
     */
    public function resolve(TrainingCourse $trainingCourse, User $user): ?array
    {
        $userTrainingCourse = UserTrainingCourse::where('user_id', $user->id)->where('training_course_id', $trainingCourse->id)->first();

        if($this->canGenerateCertificate($userTrainingCourse)) {
            return [
                'participant_name' => $user->name,
                'course_name' => $trainingCourse->name,
                'course_score' => $userTrainingCourse->score,
                'completion_date' => $userTrainingCourse->updated_at->format('jS F Y'),
            ];
        }

        return null;
    }

    protected function canGenerateCertificate($userTrainingCourse) {
        return $userTrainingCourse && $userTrainingCourse->completed && $userTrainingCourse->passed && !$userTrainingCourse->certificate_url;
    }
}