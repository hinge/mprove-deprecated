<?php

namespace App\Services\Resolvers\Contracts;

use Illuminate\Database\Eloquent\Collection;

interface MaterialVideoCountResolverInterface
{
    /**
     * @param Collection $materials
     * @return int
     */
    public function resolve(Collection $materials): int;
}