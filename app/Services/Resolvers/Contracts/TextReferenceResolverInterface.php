<?php

namespace App\Services\Resolvers\Contracts;

use Illuminate\Support\Collection;

interface TextReferenceResolverInterface
{
    /**
     * @param string $text
     * @param Collection $references
     * @return string
     */
    public function resolve(string $text, Collection $references): string;
}