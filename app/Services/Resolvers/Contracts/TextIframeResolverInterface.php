<?php

namespace App\Services\Resolvers\Contracts;

interface TextIframeResolverInterface
{
    /**
     * @param string $text
     * @return string
     */
    public function resolve(string $text): string;
}
