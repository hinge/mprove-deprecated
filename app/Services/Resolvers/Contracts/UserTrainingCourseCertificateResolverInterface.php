<?php

namespace App\Services\Resolvers\Contracts;

use App\TrainingCourse;
use App\User;

interface UserTrainingCourseCertificateResolverInterface
{
    /**
     * @param TrainingCourse $trainingCourse
     * @param User $user
     * @return array|null
     */
    public function resolve(TrainingCourse $trainingCourse, User $user): ?array;
}