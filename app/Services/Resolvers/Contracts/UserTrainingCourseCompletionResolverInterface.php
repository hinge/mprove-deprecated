<?php

namespace App\Services\Resolvers\Contracts;

use App\TrainingCourse;

interface UserTrainingCourseCompletionResolverInterface
{
    /**
     * @param TrainingCourse $trainingCourse
     * @return array
     */
    public function resolve(TrainingCourse $trainingCourse): array;
}