<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Facades\Auth;

class Question extends Model
{
    /**
     *
     * @return HasMany
     */
    public function answers(): HasMany
    {
        return $this->hasMany(QuestionAnswer::class);
    }

    /**
     *
     * @return HasMany
     */
    public function correctAnswers()
    {
        return $this->hasMany(QuestionAnswer::class)->where('correct', true);
    }

    /**
     *
     * @return HasMany
     */
    public function incorrectAnswers()
    {
        return $this->hasMany(QuestionAnswer::class)->where('correct', false);
    }

    public function userAnswers()
    {
        return $this->belongsToMany(
            QuestionAnswer::class,
            'user_training_course_question_answers',
            'training_course_question_id',
            'question_answer_id'
        )->withPivot('user_id')->wherePivot('user_id', '=', Auth::user()->id);
    }

    /**
     *
     * @return BelongsToMany
     */
    public function training_courses(): BelongsToMany
    {
        return $this->belongsToMany(
            TrainingCourse::class,
            'training_course_questions',
            'question_id',
            'training_course_id'
        )->withPivot('order');
    }

    /**
     * @param $query
     * @return void
     */
    public function scopeComplete($query): void
    {
        $query->join('user_training_course_question_answers', 'user_training_course_question_answers.training_course_question_id', '=', 'training_course_questions.id')
            ->where('user_training_course_question_answers.user_id', '=', Auth::user()->id);
    }
}
