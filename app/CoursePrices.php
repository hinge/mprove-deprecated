<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class CoursePrices extends Model
{

    /**
     *
     * @return BelongsTo
     */
    public function courseDate(): BelongsTo
    {
        return $this->belongsTo(CourseDate::class);
    }
}
