<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\Pivot;

class CourseCourseDetail extends Pivot
{
    protected $table = 'course_course_detail';
}
