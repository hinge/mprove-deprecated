<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class MenuItem extends Model
{

    /**
     *
     * @return HasMany
     */
    public function children(): HasMany
    {
    	return $this->hasMany('App\MenuItem', 'parent_id');
    }
}
