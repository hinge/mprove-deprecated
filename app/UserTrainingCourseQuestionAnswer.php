<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\Pivot;

class UserTrainingCourseQuestionAnswer extends Pivot
{
    protected $table = 'user_training_course_question_answers';

    protected $fillable = [
        'user_id',
        'training_course_question_id',
        'question_answer_id'
    ];
}
