<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\Pivot;

class TrainingCourseMaterialReference extends Pivot
{
    protected $table = 'training_course_material_references';
}
