<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Menu extends Model
{

    /**
     *
     * @return HasMany
     */
    public function items(): HasMany
    {
    	return $this->hasMany('App\MenuItem')
    				->whereNull('parent_id');
    }
}
