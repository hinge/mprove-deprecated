<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class CourseOverview extends Model
{
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
	protected $hidden = ['id', 'course_id'];


    /**
     *
     * @return BelongsTo
     */
    public function course(): BelongsTo
    {
        return $this->belongsTo(Course::class);
    }
}
