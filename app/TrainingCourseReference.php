<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class TrainingCourseReference extends Model
{

    /**
     *
     * @return BelongsTo
     */
    public function type(): BelongsTo
    {
        return $this->belongsTo(TrainingCourseReferenceType::class, 'training_course_reference_type_id');
    }

    /**
     *
     * @return BelongsToMany
     */
    public function materials(): BelongsToMany
    {
        return $this->belongsToMany(
            TrainingCourseMaterial::class,
            'training_course_material_references',
            'training_course_reference_id',
            'training_course_material_id'
        );
    }
}
