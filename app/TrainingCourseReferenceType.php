<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class TrainingCourseReferenceType extends Model
{

    /**
     *
     * @return HasMany
     */
    public function references(): HasMany
    {
        return $this->hasMany(TrainingCourseReference::class);
    }
}
