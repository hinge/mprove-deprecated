<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddResourceTypeToResourceCategories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('resource_types', function (Blueprint $table) {
            $table->integer('id')->unsigned()->change();
            $table->primary('id');
        });
        Schema::table('resource_categories', function (Blueprint $table) {
            $table->integer('resource_type_id')->unsigned()->nullable()->after('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('resource_categories', function (Blueprint $table) {
            $table->dropColumn('resource_type_id');
        });
    }
}
