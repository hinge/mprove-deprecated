<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddUniqueReferenceNumberToTrainingCourseMaterialReferences extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('training_course_material_references', function (Blueprint $table) {
            $table->unsignedInteger('unique_reference_number')->after('training_course_reference_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('training_course_material_references', function (Blueprint $table) {
            $table->dropColumn('unique_reference_number');
        });
    }
}
