<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCourseDateIdToCourseFeedbacksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('course_feedbacks', function (Blueprint $table) {
        	$table->unsignedInteger('course_date_id')->after('venue_id')->nullable();
			$table->foreign('course_date_id')->references('id')->on('course_dates')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('course_feedbacks', function (Blueprint $table) {
            if(DB::getDriverName() != 'sqlite')  {
                $table->dropForeign('course_feedbacks_course_date_id_foreign');
            }

			$table->dropColumn('course_date_id');
        });
    }
}
