<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCourseFeedbacksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('course_feedbacks', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('course_id');
            $table->unsignedInteger('venue_id');
            $table->char('title', 191);
            $table->text('description');
            $table->char('action_text', 191);
            $table->char('action_url', 191);
            $table->nullableTimestamps();

            $table->foreign('venue_id', 'course_feedback_venue_id_foreign')
                ->references('id')
                ->on('venues')->onDelete('CASCADE')
                ->onUpdate('RESTRICT');
            $table->foreign('course_id', 'course_feedback_course_id_foreign')
                ->references('id')
                ->on('courses')->onDelete('CASCADE')
                ->onUpdate('RESTRICT');

        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('course_feedbacks');
    }
}
