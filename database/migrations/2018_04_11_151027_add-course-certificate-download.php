<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCourseCertificateDownload extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::table('course_dates', function(Blueprint $table) {
			$table->string('certificate_url')->after('registration_url')->nullable();
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::table('course_dates', function(Blueprint $table) {
			$table->dropColumn('certificate_url');
		});
    }
}
