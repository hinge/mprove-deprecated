<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateResourceTypesIdColumnToAutoIncrement extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(DB::getDriverName() != 'sqlite') {
            Schema::table('resource_categories', function (Blueprint $table) {
                $table->dropForeign('resource_categories_resource_type_id_foreign');
            });
        }

        Schema::table('resource_types', function (Blueprint $table) {
            $table->increments('id')->change();
        });

        if(DB::getDriverName() != 'sqlite') {
            Schema::table('resource_categories', function (Blueprint $table) {
                $table->foreign('resource_type_id')->references('id')->on('resource_types');
            });
        }

        Schema::table('resources', function (Blueprint $table) {
            $table->foreign('type_id')->references('id')->on('resource_types')->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('resource_types', function (Blueprint $table) {
            Schema::table('resource_types', function (Blueprint $table) {
                $table->dropPrimary('id');
            });

            if(DB::getDriverName() != 'sqlite') {
                Schema::table('resources', function (Blueprint $table) {
                    $table->dropForeign(['type_id']);
                });
            }
        });
    }
}
