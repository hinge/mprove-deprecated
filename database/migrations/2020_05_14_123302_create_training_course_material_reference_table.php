<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTrainingCourseMaterialReferenceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('training_course_material_references', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('training_course_material_id');
            $table->foreign('training_course_material_id', 'training_course_material_id')
                ->references('id')
                ->on('training_course_materials')
                ->onDelete('cascade');

            $table->unsignedBigInteger('training_course_reference_id');
            $table->foreign('training_course_reference_id', 'training_course_reference_id')
                ->references('id')
                ->on('training_course_references')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('training_course_material_references');
    }
}
