<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTrainingCourseReferencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('training_course_references', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('training_course_reference_type_id');
            $table->foreign('training_course_reference_type_id', 'training_course_reference_type_id')
                ->references('id')
                ->on('training_course_reference_types')
                ->onDelete('cascade');
            $table->char('title');
            $table->char('url')->nullable();
            $table->char('file')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('training_course_references');
    }
}
