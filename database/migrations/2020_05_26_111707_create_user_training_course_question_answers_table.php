<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserTrainingCourseQuestionAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_training_course_question_answers', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('user_id');
            $table->foreign('user_id', 'user_training_course_qa_user_id')->references('id')->on('users')->cascadeOnDelete();
            $table->unsignedBigInteger('training_course_question_id',);
            $table->foreign('training_course_question_id', 'user_training_course_qa_question_id')->references('id')->on('training_course_questions')->cascadeOnDelete();
            $table->unsignedBigInteger('question_answer_id')->nullable();
            $table->foreign('question_answer_id', 'user_training_course_qa_answer_id')->references('id')->on('question_answers')->cascadeOnDelete();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_training_course_question_answers');
    }
}
