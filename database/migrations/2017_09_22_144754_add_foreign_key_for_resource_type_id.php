<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeyForResourceTypeId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('resource_categories', function (Blueprint $table) {
           $table->foreign('resource_type_id')
                  ->references('id')->on('resource_types')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('resource_categories', function (Blueprint $table) {
            if(DB::getDriverName() != 'sqlite') {
                $table->dropForeign('resource_categories_resource_type_id_foreign');
            }
        });
    }
}
