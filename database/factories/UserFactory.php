<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;

$factory->define(\App\User::class, function (Faker $faker) {
    return [
        'name' => $faker->realText(30),
        'email' => $faker->email,
        'password' => \Illuminate\Support\Facades\Hash::make($faker->password(6, 8)),
        'created_at' => \Carbon\Carbon::now(),
        'updated_at' => \Carbon\Carbon::now(),
    ];
})->afterCreating(\App\User::class, function($user, Faker $faker){
    factory(\App\ApiToken::class)->create([
        'user_id' => $user->id,
        'name' => $faker->text(30),
        'token' => '123456'
    ]);
});
