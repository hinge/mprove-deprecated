<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;
$factory->define(\App\ResourceType::class, function (Faker $faker) {
    return [
        'name' => $faker->unique()->word,
        'slug' => $faker->unique()->slug,
    ];
})->afterCreating(\App\ResourceType::class, function($resourceType) {

    $resourceCategories = factory(\App\ResourceCategory::class, 2)->create([
        'resource_type_id' => $resourceType->id
    ]);

    foreach($resourceCategories as $resourceCategory) {
        $resources = factory(\App\Resource::class, 5)->create([
            'type_id' => $resourceType->id
        ]);

        foreach($resources as $resource) {
            factory(\App\ResourceResourceCategory::class)->create([
                'resource_id' => $resource->id,
                'resource_category_id' => $resourceCategory->id
            ]);
        }
    }
});

$factory->define(\App\ResourceCategory::class, function (Faker $faker) {
    return [
        'name' => $faker->unique()->text(5),
        'slug' => $faker->slug,
    ];
});

$factory->define(\App\Resource::class, function (Faker $faker) {
    return [
        'title' => $faker->text(20),
        'description' => $faker->text(100),
        'icon' => '',
        'url' => 'mprove.pdf',
        'type_id' => $faker->numberBetween($min = 1, $max = 2)
    ];
});

$factory->define(\App\ResourceResourceCategory::class, function (Faker $faker) {
    return [
    ];
});





