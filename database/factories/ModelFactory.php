<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */

$factory->define(App\UserTrainingCourse::class, function(Faker\Generator $faker) {
    return [
        'user_id' => 1,
        'training_course_id' => factory(App\TrainingCourse::class),
        'score' => $faker->numberBetween(0, 100),
        'passed' => $faker->boolean(50),
        'completed' => $faker->boolean(50)
    ];
});


$factory->define(App\UserTrainingCourseQuestionAnswer::class, function(Faker\Generator $faker) {
    return [
        'user_id' => factory(App\User::class),
        'training_course_question_id' => factory(\App\Question::class),
        'question_answer_id' => factory(\App\QuestionAnswer::class),
        'created_at' => \Carbon\Carbon::now(),
        'updated_at' => \Carbon\Carbon::now(),
    ];
});

$factory->define(App\UserTrainingCourseMaterialSeen::class, function(Faker\Generator $faker) {
    return [
        'user_id' => factory(App\User::class),
        'training_course_material_id' => factory(App\TrainingCourseMaterial::class),
    ];
});
