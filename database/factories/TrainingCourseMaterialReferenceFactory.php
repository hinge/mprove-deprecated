<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(App\TrainingCourseMaterialReference::class, function (Faker $faker) {
    return [
        'unique_reference_number' => $faker->numberBetween(1,100),
    ];
});
