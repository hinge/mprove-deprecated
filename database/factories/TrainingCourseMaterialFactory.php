<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(\App\TrainingCourseMaterial::class, function (Faker $faker) {
    return [
        'title' => $faker->realText(20),
        'summary' => $faker->realText(190),
        'content' => $faker->realText(1000),
        'order' => $faker->randomNumber(2)
    ];
});
