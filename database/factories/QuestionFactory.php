<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(\App\Question::class, function (Faker $faker) {
    return [
        'title' => $faker->realText(20),
    ];
})->afterCreating(\App\Question::class, function($question){
    factory(\App\QuestionAnswer::class, 3)->create([
        'question_id' => $question->id
    ]);
    factory(\App\QuestionAnswer::class)->create([
        'question_id' => $question->id,
        'correct' => 1
    ]);
});
