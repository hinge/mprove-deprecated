<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(\App\TrainingCourseQuestion::class, function (Faker $faker) {
    return [
        'question_id' => factory(\App\Question::class)->create()->id,
        'order' => $faker->randomNumber(2)
    ];
});
