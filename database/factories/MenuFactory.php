<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(OptimistDigital\MenuBuilder\Models\Menu::class, function (Faker $faker) {
    return [
        'name' => $faker->word,
        'slug' => $faker->slug,
        'locale' => 'en',
    ];
});

$factory->define(OptimistDigital\MenuBuilder\Models\MenuItem::class, function (Faker $faker) {
    return [
        'menu_id' => factory(OptimistDigital\MenuBuilder\Models\Menu::class),
        'name' => $faker->word,
        'class' => OptimistDigital\MenuBuilder\Classes\MenuItemStaticURL::class,
        'value' => $faker->url,
        'target' => '_self',
        'order' => $faker->numberBetween(1,20),
        'enabled' => true
    ];
});
