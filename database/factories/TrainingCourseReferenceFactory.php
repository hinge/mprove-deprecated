<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(\App\TrainingCourseReference::class, function (Faker $faker) {
    return [
        'training_course_reference_type_id' => factory(\App\TrainingCourseReferenceType::class)->create()->id,
        'title' => $faker->realText(10),
        'url' => $faker->url
    ];
});
