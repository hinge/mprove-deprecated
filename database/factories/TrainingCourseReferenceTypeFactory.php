<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(\App\TrainingCourseReferenceType::class, function (Faker $faker) {
    return [
        'name' => $faker->realText(15)
    ];
});
