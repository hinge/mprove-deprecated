<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;

$factory->define(\App\ApiToken::class, function (Faker $faker) {
    return [
        'id' => $faker->uuid,
        'name' => $faker->name,
        'token' =>  $faker->regexify('[A-Za-z0-9]{8}'),
        'metadata' => $faker->text(50)
    ];
});
