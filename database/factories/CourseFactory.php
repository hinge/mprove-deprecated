<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(\App\Course::class, function (Faker $faker) {
    return [
        'name' => $faker->text(5),
        'slug' => $faker->slug,
        'description' => $faker->text(50),
        'excerpt' => $faker->text(50),
    ];
})->afterCreating(\App\Course::class, function($course, Faker $faker){
    $icons = [
        'award-svg.svg',
        'checklist-svg.svg',
        'medical-svg.svg',
        'readme.svg'
    ];
    $venues = factory( \App\Venue::class, 4)->create();

    foreach($venues as $key => $venue) {

        $courseDates = factory(\App\CourseDate::class, 3)->create([
            'course_id' => $course->id,
            'venue_id' => $venue->id,
        ]);

        $courseDetails = factory(\App\CourseDetail::class, 1)->create([
            'icon' => '/img/SeedIcons/' . $icons[$key],
        ]);

        foreach($courseDetails as $courseDetail) {

            factory(\App\CourseCourseDetail::class, 1)->create([
                'course_id' => $course->id,
                'course_detail_id' => $courseDetail->id
            ]);
        }

        factory(\App\CourseOverview::class, 1)->create([
            'course_id' => $course->id,
        ]);

        foreach($courseDates as $courseDate) {

            factory(\App\CourseFeedback::class, 2)->create([
                'course_id' => $course->id,
                'venue_id' => $venue->id,
                'course_date_id' => $courseDate->id
            ]);
        }
    }
});

$factory->define(\App\CourseDate::class, function (Faker $faker) {
    return [
        'event_title' => $faker->text(5),
        'event_subtitle' => $faker->text(10),
        'course_id' => null,
        'code' => $faker->word,
        'price' => $faker->numberBetween(100, 200),
        'currency' => $faker->currencyCode,
        'registration_url' => $faker->url,
        'certificate_url' => $faker->url,
        'resources_url' => $faker->url,
        'start_date' => \Carbon\Carbon::now()->addMonths(rand(1, 5))->addMinutes(rand(1,55)),
        'end_date' => \Carbon\Carbon::now()->addMonths(rand(6, 7))->addMinutes(rand(1,55))
    ];
});

$factory->define(\App\CourseDetail::class, function (Faker $faker) {
    return [
        'internal_name' => $faker->slug(5),
        'title' => $faker->text(10),
        'description' => $faker->text(30),
        'url' => $faker->url,
        'icon' => $faker->url,
    ];
});

$factory->define(\App\CourseOverview::class, function (Faker $faker) {
    return [
        'title' => $faker->unique()->text(10),
        'description' => $faker->unique()->text(30),
        'action_text' => $faker->word(),
        'action_url' => $faker->url,
    ];
});

$factory->define(\App\CourseFeedback::class, function (Faker $faker) {
    return [
        'title' => $faker->unique()->text(10),
        'description' => $faker->unique()->text(30),
        'action_text' => $faker->word(),
        'action_url' => $faker->url,
    ];
});

$factory->define(\App\CourseCourseDetail::class, function (Faker $faker) {
    return [];
});
