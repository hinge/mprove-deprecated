<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(\App\QuestionAnswer::class, function (Faker $faker) {
    return [
        'title' => $faker->realText(20),
        'order' => $faker->randomNumber(2),
        'correct' => 0
    ];
});
