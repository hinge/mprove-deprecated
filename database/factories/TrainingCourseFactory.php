<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\Question;
use App\User;
use Faker\Generator as Faker;
use Illuminate\Support\Facades\DB;

$factory->define(\App\TrainingCourse::class, function (Faker $faker) {
    return [
        'name' => $faker->realText(30),
        'description' => $faker->realText(50),
        'pass_rate' => $faker->numberBetween(60, 99),
        'published' => $faker->boolean(50),
        'draft' => $faker->boolean(50),
    ];
})->afterCreating(\App\TrainingCourse::class, function($trainingCourse){

    $trainingCourseMaterials = factory(\App\TrainingCourseMaterial::class, 13)->create([
        'training_course_id' => $trainingCourse->id
    ]);
    $trainingCourseQuestions = factory(\App\TrainingCourseQuestion::class, 11)->create([
        'training_course_id' => $trainingCourse->id
    ]);
    $references = factory(\App\TrainingCourseReference::class, 10)->create([
        'file' => null
    ]);
    $references->random(3)->each(function($reference) use ($trainingCourseMaterials) {
        factory(\App\TrainingCourseMaterialReference::class)->create([
            'training_course_material_id' => $trainingCourseMaterials->random(1)->first()->id,
            'training_course_reference_id' => $reference->id
        ]);
    });

    if(!$trainingCourse->draft && $trainingCourse->published) {

        $courseStates = [
            'started',
            'passed',
            'failed',
            'available'
        ];

        $courseState = $courseStates[rand(0,3)];
        $courseCompleted = true;
        $courseStarted = true;

        if($courseState == 'passed') {
            $numberOfCompletedMaterials = $trainingCourseMaterials->count();
            $numberOfCorrectAnswers = ceil($trainingCourseQuestions->count() * ($trainingCourse->pass_rate / 100));
            $numberOfAnsweredQuestions = $trainingCourseQuestions->count();
        }

        if($courseState == 'failed') {
            $numberOfCompletedMaterials = $trainingCourseMaterials->count();
            $numberOfCorrectAnswers = floor($trainingCourseQuestions->count() * ($trainingCourse->pass_rate / 100));
            $numberOfAnsweredQuestions = $trainingCourseQuestions->count();
        }

        if($courseState == 'started') {
            $courseCompleted = false;
            $chanceForAllMaterialsCompleted = rand(0,1);
            $numberOfCompletedMaterials = !$chanceForAllMaterialsCompleted ? rand(0, $trainingCourseMaterials->count()) : $trainingCourseMaterials->count();
            $numberOfCorrectAnswers = 0;
            $numberOfAnsweredQuestions = 0;

            if($numberOfCompletedMaterials == $trainingCourseMaterials->count()) {
                $numberOfAnsweredQuestions = rand(0, $trainingCourseQuestions->count());
                $numberOfCorrectAnswers = rand(0, $numberOfAnsweredQuestions);
            }
        }

        if($courseState == 'available') {
            $courseCompleted = false;
            $courseStarted = false;
        }

        if($courseStarted) {
            $userId = User::first()->id;

            $userTrainingCourse = factory(\App\UserTrainingCourse::class, 1)->create([
                'training_course_id' => $trainingCourse->id,
                'passed' => $courseState == 'passed',
                'completed' => $courseCompleted,
                'user_id' => $userId,
                'score' => 100 / $trainingCourseQuestions->count() * (int) $numberOfCorrectAnswers,
            ]);

            foreach($trainingCourseMaterials->sortBy('order')->pluck('id') as $key => $trainingCourseMaterialId) {
                if($key + 1 <= $numberOfCompletedMaterials) {
                    $userTrainingCourseMaterialSeen = factory(\App\UserTrainingCourseMaterialSeen::class, 1)->create([
                        'user_id' => $userId,
                        'training_course_material_id' => $trainingCourseMaterialId,
                    ]);
                }
            }

            $questions = Question::whereIn('id', $trainingCourseQuestions->pluck('question_id'))->with('incorrectAnswers', 'correctAnswers')->get();

            foreach($questions as $key => $question) {

                if($key + 1 <= $numberOfAnsweredQuestions) {
                    $correctAnswerId = $question->correctAnswers->first()->id;
                    $incorrectAnswerIds = $question->incorrectAnswers->pluck('id');

                    //pivot records don't return their id as normally you'd fetch the pivot data in a different way so have to get it by loading the table rows manually
                    $trainingCourseQuestion = DB::table('training_course_questions')->where('question_id', $question->id)->first();
                    $trainingCourseQuestionId = isset($trainingCourseQuestion->id) ? $trainingCourseQuestion->id : null;

                    $userTrainingCourseUserAnswer = factory(\App\UserTrainingCourseQuestionAnswer::class, 1)->create([
                        'user_id' => $userId,
                        'training_course_question_id' => $trainingCourseQuestionId,
                        'training_course_question_id' => $trainingCourseQuestionId,
                        'question_answer_id' => $key + 1 < $numberOfCorrectAnswers ? $correctAnswerId : $incorrectAnswerIds[rand(0,2)],
                    ]);
                }
            }
        }
    }
});
