<?php

use App\ApiToken;
use App\User;
use Illuminate\Database\Seeder;
use Faker\Factory;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(User::class)->create([
            'name' => 'Joe Bloggs',
            'email' => 'joe.bloggs@email.com',
            'password' => Hash::make('testUserPassword123')
        ]);
    }
}
