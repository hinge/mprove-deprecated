<?php

use Illuminate\Database\Seeder;

class UserTrainingCourseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        //user training course materials seens
//
//        \DB::table('user_training_course_material_seens')->insert([
//            [
//                'id' => 1,
//                'user_id' => 10000,
//                'training_course_material_id' => 1,
//                'created_at' => \Carbon\Carbon::now(),
//                'updated_at' => \Carbon\Carbon::now(),
//            ],
//            [
//                'id' => 2,
//                'user_id' => 10000,
//                'training_course_material_id' => 2,
//                'created_at' => \Carbon\Carbon::now(),
//                'updated_at' => \Carbon\Carbon::now(),
//            ],
//            [
//                'id' => 3,
//                'user_id' => 10000,
//                'training_course_material_id' => 3,
//                'created_at' => \Carbon\Carbon::now(),
//                'updated_at' => \Carbon\Carbon::now(),
//            ],
//            [
//                'id' => 4,
//                'user_id' => 10000,
//                'training_course_material_id' => 4,
//                'created_at' => \Carbon\Carbon::now(),
//                'updated_at' => \Carbon\Carbon::now(),
//            ],
//            [
//                'id' => 5,
//                'user_id' => 10000,
//                'training_course_material_id' => 5,
//                'created_at' => \Carbon\Carbon::now(),
//                'updated_at' => \Carbon\Carbon::now(),
//            ],
//            [
//                'id' => 6,
//                'user_id' => 10000,
//                'training_course_material_id' => 7,
//                'created_at' => \Carbon\Carbon::now(),
//                'updated_at' => \Carbon\Carbon::now(),
//            ],
//        ]);
//
//        //user training course question answers
//        \DB::table('user_training_course_question_answers')->delete();
//        \DB::table('user_training_course_question_answers')->insert([
//            [
//                //1,2
//                'id' => 1,
//                'user_id' => 10000,
//                'training_course_question_id' => 1,
//                'question_answer_id' => 1,
//                'created_at' => \Carbon\Carbon::now(),
//                'updated_at' => \Carbon\Carbon::now(),
//            ],
//            [
//                //3,4
//                'id' => 2,
//                'user_id' => 10000,
//                'training_course_question_id' => 2,
//                'question_answer_id' => 3,
//                'created_at' => \Carbon\Carbon::now(),
//                'updated_at' => \Carbon\Carbon::now(),
//            ],
//            [
//                //5,6
//                'id' => 3,
//                'user_id' => 10000,
//                'training_course_question_id' => 3,
//                'question_answer_id' => 6,
//                'created_at' => \Carbon\Carbon::now(),
//                'updated_at' => \Carbon\Carbon::now(),
//            ],
//            [
//                //7,8
//                'id' => 4,
//                'user_id' => 10000,
//                'training_course_question_id' => 4,
//                'question_answer_id' => 7,
//                'created_at' => \Carbon\Carbon::now(),
//                'updated_at' => \Carbon\Carbon::now(),
//            ],
//            [
//                //9,10
//                'id' => 5,
//                'user_id' => 10000,
//                'training_course_question_id' => 5,
//                'question_answer_id' => 10,
//                'created_at' => \Carbon\Carbon::now(),
//                'updated_at' => \Carbon\Carbon::now(),
//            ],
//            [
//                //11,12
//                'id' => 6,
//                'user_id' => 10000,
//                'training_course_question_id' => 6,
//                'question_answer_id' => 12,
//                'created_at' => \Carbon\Carbon::now(),
//                'updated_at' => \Carbon\Carbon::now(),
//            ],
//            [
//                //13,14
//                'id' => 7,
//                'user_id' => 10000,
//                'training_course_question_id' => 7,
//                'question_answer_id' => 14,
//                'created_at' => \Carbon\Carbon::now(),
//                'updated_at' => \Carbon\Carbon::now(),
//            ],
//            [
//                //15,16
//                'id' => 8,
//                'user_id' => 10000,
//                'training_course_question_id' => 8,
//                'question_answer_id' => 15,
//                'created_at' => \Carbon\Carbon::now(),
//                'updated_at' => \Carbon\Carbon::now(),
//            ],
//            [
//                //17,18
//                'id' => 9,
//                'user_id' => 10000,
//                'training_course_question_id' => 9,
//                'question_answer_id' => 17,
//                'created_at' => \Carbon\Carbon::now(),
//                'updated_at' => \Carbon\Carbon::now(),
//            ],
//        ]);
//
//        //user training courses
//        \DB::table('user_training_courses')->delete();
//        \DB::table('user_training_courses')->insert([
//            [
//                'id' => 1,
//                'user_id' => 10000,
//                'training_course_id' => 1,
//                'score' => 1,
//                'passed' => 1,
//                'completed' => 1,
//                'created_at' => \Carbon\Carbon::now(),
//                'updated_at' => \Carbon\Carbon::now(),
//            ],
//            [
//                'id' => 2,
//                'user_id' => 10000,
//                'training_course_id' => 2,
//                'score' => 0,
//                'passed' => 0,
//                'completed' => 0,
//                'created_at' => \Carbon\Carbon::now(),
//                'updated_at' => \Carbon\Carbon::now(),
//            ],
//            [
//                'id' => 3,
//                'user_id' => 10000,
//                'training_course_id' => 3,
//                'score' => 0,
//                'passed' => 0,
//                'completed' => 0,
//                'created_at' => \Carbon\Carbon::now(),
//                'updated_at' => \Carbon\Carbon::now(),
//            ],
//            [
//                'id' => 4,
//                'user_id' => 10000,
//                'training_course_id' => 4,
//                'score' => 0,
//                'passed' => 0,
//                'completed' => 0,
//                'created_at' => \Carbon\Carbon::now(),
//                'updated_at' => \Carbon\Carbon::now(),
//            ],
//            [
//                'id' => 5,
//                'user_id' => 10000,
//                'training_course_id' => 5,
//                'score' => 0,
//                'passed' => 0,
//                'completed' => 0,
//                'created_at' => \Carbon\Carbon::now(),
//                'updated_at' => \Carbon\Carbon::now(),
//            ],
//        ]);
//    }
    }
}
