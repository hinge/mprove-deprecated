<?php

use Illuminate\Database\Seeder;

class TrainingCourseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //published
        factory(\App\TrainingCourse::class, 7)->create([
            'published' => true,
            'draft' => false
        ]);

        //drafted
        factory(\App\TrainingCourse::class, 2)->create([
            'published' => true,
            'draft' => true
        ]);

        //not available
        factory(\App\TrainingCourse::class, 2)->create([
            'published' => false,
            'draft' => false
        ]);
    }
}
