<?php

use Illuminate\Database\Seeder;

class ContentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Course::class)->create([
            'id' => 1,
            'name' => 'Neonatal Difficult Airway Course',
            'slug' => 'neonatal-difficult-airway-course',
            'description' => 'This course addresses human factors, human resource management, development of pathways and difficult airway trolleys in management of the difficult neonatal airway.',
            'excerpt' => 'This course addresses human factors, human resource management, development of pathways and difficult airway trolleys in management of the difficult neonatal airway.'
        ]);

        factory(\App\ResourceType::class)->createMany([
            [
                'id' => 1,
                'name' => 'Scenario',
                'slug' => 'scenario'
            ],
            [
                'id' => 2,
                'name' => 'Toolkit',
                'slug' => 'toolkit'
            ],
            [
                'id' => 3,
                'name' => 'Moulage',
                'slug' => 'moulage'
            ]
        ]);
    }
}
