<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserSeeder::class);
        $this->call(TrainingCourseSeeder::class);
        $this->call(UserTrainingCourseSeeder::class);
        $this->call(MenuSeeder::class);
        $this->call(ContentSeeder::class);

    }
}
