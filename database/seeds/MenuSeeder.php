<?php

use Illuminate\Database\Seeder;

class MenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {



        factory(OptimistDigital\MenuBuilder\Models\Menu::class)->createMany([
            [
                'id' => '1',
                'name' => 'Main menu',
                'slug' => 'main-menu',
            ],
            [
                'id' => '2',
                'name' => 'Secondary menu',
                'slug' => 'secondary-menu',
            ],
            [
                'id' => '4',
                'name' => 'Footer: Column 1',
                'slug' => 'footer-column-1',
            ],
            [
                'id' => '5',
                'name' => 'Footer: Column 2',
                'slug' => 'footer-column-2',
            ],
            [
                'id' => '6',
                'name' => 'Footer: Column 3',
                'slug' => 'footer-column-3',
            ]
        ]);

        factory(OptimistDigital\MenuBuilder\Models\MenuItem::class)->createMany([
            [
                'id' => 1,
                'name' => 'MPROvE Academy',
                'value' => '/mprove-academy',
                'class' => OptimistDigital\MenuBuilder\Classes\MenuItemStaticURL::class,
                'menu_id' => 1
            ],
            [
                'id' => 2,
                'name' => 'Neonatal Difficult Airway Course',
                'value' => '/mprove-academy/neonatal-difficult-airway-course',
                'class' => OptimistDigital\MenuBuilder\Classes\MenuItemStaticURL::class,
                'parent_id' => 1,
                'menu_id' => 1
            ],
            [
                'id' => 5,
                'name' => 'Scenario Bank',
                'value' => '/mprove-academy/scenario-bank',
                'class' => OptimistDigital\MenuBuilder\Classes\MenuItemStaticURL::class,
                'parent_id' => 1,
                'menu_id' => 1
            ],
            [
                'id' => 6,
                'name' => 'Moulage',
                'value' => '/mprove-academy/moulage',
                'class' => OptimistDigital\MenuBuilder\Classes\MenuItemStaticURL::class,
                'parent_id' => 1,
                'menu_id' => 1
            ],
            [
                'id' => 7,
                'name' => 'Quality MPROvEment',
                'value' => '/quality-mprovement',
                'class' => OptimistDigital\MenuBuilder\Classes\MenuItemStaticURL::class,
                'menu_id' => 1
            ],
            [
                'id' => 8,
                'name' => 'Community Forum',
                'value' => '/forums',
                'class' => OptimistDigital\MenuBuilder\Classes\MenuItemStaticURL::class,
                'parent_id' => 7,
                'menu_id' => 1
            ],
            [
                'id' => 9,
                'name' => 'Toolkits',
                'value' => '/quality-mprovement/toolkits',
                'class' => OptimistDigital\MenuBuilder\Classes\MenuItemStaticURL::class,
                'parent_id' => 7,
                'menu_id' => 1
            ],
            [
                'id' => 22,
                'name' => 'Online Training Courses',
                'value' => '/online-training-courses',
                'class' => OptimistDigital\MenuBuilder\Classes\MenuItemStaticURL::class,
                'menu_id' => 1
            ],
            [
                'id' => 10,
                'name' => 'About MPROvE',
                'value' => '/about-mprove',
                'class' => OptimistDigital\MenuBuilder\Classes\MenuItemStaticURL::class,
                'menu_id' => 2
            ],
            [
                'id' => 11,
                'name' => 'MPROvE Academy',
                'value' => '',
                'class' => OptimistDigital\MenuBuilder\Classes\MenuItemText::class,
                'menu_id' => 4
            ],
            [
                'id' => 12,
                'name' => 'Moulage',
                'value' => '/mprove-academy/moulage',
                'class' => OptimistDigital\MenuBuilder\Classes\MenuItemStaticURL::class,
                'parent_id' => 11,
                'menu_id' => 4
            ],
            [
                'id' => 13,
                'name' => 'Nenonatal Simulation Instructor Course',
                'value' => '/mprove-academy/nenonatal-simulation-instructor-course',
                'class' => OptimistDigital\MenuBuilder\Classes\MenuItemStaticURL::class,
                'parent_id' => 11,
                'menu_id' => 4
            ],
            [
                'id' => 14,
                'name' => 'Nenonatal Difficult Airway Course',
                'value' => '/mprove-academy/neonatal-difficult-airway-course',
                'class' => OptimistDigital\MenuBuilder\Classes\MenuItemStaticURL::class,
                'parent_id' => 11,
                'menu_id' => 4
            ],
            [
                'id' => 15,
                'name' => 'NEDS Course',
                'value' => '/mprove-academy/nenonatal-emergencies-difficult-situations-course',
                'class' => OptimistDigital\MenuBuilder\Classes\MenuItemStaticURL::class,
                'parent_id' => 11,
                'menu_id' => 4
            ],
            [
                'id' => 16,
                'name' => 'Scenario Bank',
                'value' => '/mprove-academy/scenario-bank',
                'class' => OptimistDigital\MenuBuilder\Classes\MenuItemStaticURL::class,
                'parent_id' => 11,
                'menu_id' => 4
            ],
            [
                'id' => 17,
                'name' => 'Quality MPROvEment',
                'value' => '',
                'class' => OptimistDigital\MenuBuilder\Classes\MenuItemText::class,
                'menu_id' => 5
            ],
            [
                'id' => 18,
                'name' => 'Community Forum',
                'value' => '/forums',
                'class' => OptimistDigital\MenuBuilder\Classes\MenuItemStaticURL::class,
                'parent_id' => 17,
                'menu_id' => 5
            ],
            [
                'id' => 19,
                'name' => 'Toolkits',
                'value' => '/quality-mprovement/toolkits',
                'class' => OptimistDigital\MenuBuilder\Classes\MenuItemStaticURL::class,
                'parent_id' => 17,
                'menu_id' => 5
            ],
            [
                'id' => 20,
                'name' => 'MPROvE',
                'value' => '',
                'class' => OptimistDigital\MenuBuilder\Classes\MenuItemText::class,
                'menu_id' => 6
            ],
            [
                'id' => 21,
                'name' => 'About MPROvE',
                'value' => '/about-mprove',
                'class' => OptimistDigital\MenuBuilder\Classes\MenuItemStaticURL::class,
                'parent_id' => 20,
                'menu_id' => 6
            ]
        ]);
    }
}
