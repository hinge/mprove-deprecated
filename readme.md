# MPROvE

## Getting Started

### Running Docker

Run docker for the first time.
```
docker-compose up -d --build
```

Run docker normally.

```
docker-compose up -d
```
 
### Local Development

- Create `.env` file and copy the contents of the `.env.example` into it.

- Make sure you have `MIX_API_URL` set in the env file matching the correct for the online training courses to call the right routes

- Update your hosts file

Edit your hosts file using `vi /etc/hosts` and add the following:

```
127.0.0.1 local.mproveacademy.com
127.0.0.1 mprove-mysql
``` 

- Connect to the database using DB credentials in the `.env` file

- Run `composer install`

- Run `php artisan key generate`

- Run `php artisan app:refresh`

- Run `npm install`

- Run `npm run watch`

If the online training course certificate download endpoint throws and error which complains about browser revision not being found, run `node node_modules/puppeteer/install.js` within the project root in order to install headless Chrome manually although that should happen on automatically npm install.

The mprove project is running on `local.mproveacademy.com:8080`.

### Stop Docker
```
docker-compose stop
```




